package net.colonize2;

import net.colonize2.game.*;
import net.colonize2.game.trade.TradeNode;
import net.colonize2.ui.*;
import net.stgl.MainFrame;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.state.ResourceManager;
import net.stgl.texture.DynamicTextureAtlas;
import net.stgl.ui.ComponentManager;
import net.stgl.ui.STGLDesktopPanel;
import net.stgl.ui.iframe.AbstractInternalFrame;
import org.joml.Vector4i;

import java.util.ArrayList;

public class UIManager {
    private final MainFrame frame;
    private final ComponentManager componentManager;

    private final HumanPlayer player;
    private final FontManager fontManager;
    private final STGLFont monogram;
    private final STGLFont monogram12;
    private final STGLFont monogram7;

    private final TopInformationPanel topInformationPanel;

    private final ResourceManager primaryResourceManager;
    private final STGLDesktopPanel desktopPanel;

    private final STGLFont monogram15;
    private final DynamicTextureAtlas flagAtlas;

    private ProvinceInfoWindow provinceInfoWindow = null;
    private DiplomacyWindow diplomacyWindow = null;
    private TradeNodeInfoWindow tradeNodeInfoWindow = null;

    private final ArrayList<EventPopupWindow> eventsProcessed = new ArrayList<>();

    private Province selectedProvince = null;

    public UIManager(MainFrame frame, HumanPlayer player, FontManager fontManager,
                     STGLFont monogram, STGLFont monogram12, DynamicTextureAtlas flagAtlas){
        this.flagAtlas = flagAtlas;

        primaryResourceManager = new ResourceManager();
        monogram15 = fontManager.createFont("font/monogram.ttf", 15, false, false);

        this.frame = frame;
        componentManager = new ComponentManager(frame, 4);
        this.player = player;
        this.fontManager = fontManager;
        this.monogram = monogram;
        this.monogram12 = monogram12;
        this.monogram7 = fontManager.createFont("font/monogram.ttf", 7, false, false);
        this.topInformationPanel = new TopInformationPanel(0, 0, monogram,
                monogram12, player, flagAtlas, primaryResourceManager);
        this.desktopPanel = new STGLDesktopPanel(0, topInformationPanel.getHeight(), frame.getWidth(),
                frame.getHeight()-topInformationPanel.getHeight());

        componentManager.enableDirectDrawing();
        componentManager.addComponent(topInformationPanel);
        componentManager.addComponent(desktopPanel);

    }

    public void update(){
        if (!player.getEventManager().getEvents().isEmpty()) {
            ArrayList<PopupEvent> processed = new ArrayList<>();
            for (PopupEvent event : player.getEventManager().getEvents()) {
                EventPopupWindow eventPopupWindow = new EventPopupWindow((desktopPanel.getWidth() - 300) / 2,
                        (desktopPanel.getHeight() - 370) / 2, event, monogram15, monogram12, player);
                desktopPanel.addInternalFrame(eventPopupWindow);
                eventsProcessed.add(eventPopupWindow);
                processed.add(event);
            }
            player.getEventManager().getEvents().removeAll(processed);
        }
        if (!eventsProcessed.isEmpty()){
            ArrayList<EventPopupWindow> rm = new ArrayList<>();
            for (EventPopupWindow eventPopupWindow : eventsProcessed) {
                if (eventPopupWindow.isClosed()) {
                    rm.add(eventPopupWindow);
                }
            }
            eventsProcessed.removeAll(rm);
        }
        componentManager.paint();
    }

    public ArrayList<Vector4i> getAreasCoveredByUIElements(){
        ArrayList<Vector4i> areas = new ArrayList<>();
        areas.add(new Vector4i(topInformationPanel.getX(), topInformationPanel.getY(),
                topInformationPanel.getWidth(), topInformationPanel.getHeight()));
        //desktopPanel.
        for (AbstractInternalFrame abstractInternalFrame: desktopPanel.getInternalFrames()){
            areas.add(new Vector4i(desktopPanel.getX()+abstractInternalFrame.getX(),
                    desktopPanel.getY()+abstractInternalFrame.getY(),
                    abstractInternalFrame.getWidth(),
                    abstractInternalFrame.getHeight()));
        }
        return areas;
    }

    public void spawnProvinceInformationWindow(Country c, Province pr, HumanPlayer humanPlayer, Game g){
        if (provinceInfoWindow == null || provinceInfoWindow.isClosed()){
            provinceInfoWindow = new ProvinceInfoWindow(this, 0, desktopPanel.getHeight()-230,
                    monogram15, c, pr, humanPlayer, g, monogram12, monogram, flagAtlas);
        }
        else{
            desktopPanel.removeInternalFrame(provinceInfoWindow);
            provinceInfoWindow = new ProvinceInfoWindow(this, provinceInfoWindow.getX(), provinceInfoWindow.getY(),
                    monogram15, c, pr, humanPlayer, g, monogram12, monogram, flagAtlas);
        }
        desktopPanel.addInternalFrame(provinceInfoWindow);
        desktopPanel.focusFrame(provinceInfoWindow);
    }

    public void spawnDiplomacyMenu(Country c, Province pr, HumanPlayer humanPlayer, Game g){
        if (diplomacyWindow == null || diplomacyWindow.isClosed()){
            diplomacyWindow = new DiplomacyWindow(this, 0, 0,
                    monogram15, pr, monogram, monogram12,  c, g, flagAtlas);
            desktopPanel.addInternalFrame(diplomacyWindow);
        }
        else{
            desktopPanel.removeInternalFrame(diplomacyWindow);
            diplomacyWindow = new DiplomacyWindow(this, diplomacyWindow.getX(), diplomacyWindow.getY(),
                    monogram15, pr, monogram, monogram12,  c, g, flagAtlas);
            desktopPanel.addInternalFrame(diplomacyWindow);
        }
        desktopPanel.focusFrame(diplomacyWindow);
    }

    public RivalSelectionWindow spawnRivalMenu(){
        RivalSelectionWindow rivalSelectionWindow = new RivalSelectionWindow((desktopPanel.getWidth()-300)/2,
                (desktopPanel.getHeight()-410)/2, monogram15, monogram12,
                flagAtlas, player.getCountry());
        desktopPanel.addInternalFrame(rivalSelectionWindow);
        return rivalSelectionWindow;
    }

    public FabricateClaimWindow spawnClaimWindow(Country c){
        FabricateClaimWindow claimWindow = new FabricateClaimWindow(this,(desktopPanel.getWidth()-150)/2,
                (desktopPanel.getHeight()-160)/2, monogram15, monogram12, player.getCountry(), c);
        desktopPanel.addInternalFrame(claimWindow);
        return claimWindow;
    }

    public BuildMenuWindow spawnBuildMenuWindow(Province pr, Country c){
        BuildMenuWindow buildMenuWindow = new BuildMenuWindow(0,0, this,
                primaryResourceManager, monogram15, monogram12, c, pr);
        desktopPanel.addInternalFrame(buildMenuWindow);
        desktopPanel.focusFrame(buildMenuWindow);
        return buildMenuWindow;
    }

    public StartConstructionWindow spawnConstructionMenu(Province pr, Country c){
        StartConstructionWindow constructionWindow = new StartConstructionWindow(0,0, primaryResourceManager,
                pr, c
                , monogram15, monogram12, monogram7);
        desktopPanel.addInternalFrame(constructionWindow);
        desktopPanel.focusFrame(constructionWindow);
        return constructionWindow;
    }

    public TradeNodeInfoWindow spawnTradeNodeInfoWindow(TradeNode node, Country c){
        if (tradeNodeInfoWindow != null)
            tradeNodeInfoWindow.close();
        tradeNodeInfoWindow = new TradeNodeInfoWindow(0, 0, node, c, flagAtlas, monogram15, monogram12);
        desktopPanel.addInternalFrame(tradeNodeInfoWindow);
        desktopPanel.focusFrame(tradeNodeInfoWindow);
        return tradeNodeInfoWindow;
    }

    public boolean areEventsProcessed(){
        return !eventsProcessed.isEmpty();
    }

    public void setSelectedProvince(Province selectedProvince) {
        this.selectedProvince = selectedProvince;
    }

    public Province getSelectedProvince() {
        return selectedProvince;
    }
}
