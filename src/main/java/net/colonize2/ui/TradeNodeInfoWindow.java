package net.colonize2.ui;

import net.colonize2.game.Country;
import net.colonize2.game.Game;
import net.colonize2.game.Resources;
import net.colonize2.game.trade.TradeNode;
import net.colonize2.ui.texgen.GenerateEnhancedBackground;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.color.STGLColors;
import net.stgl.font.STGLFont;
import net.stgl.texture.DynamicTextureAtlas;
import net.stgl.texture.Texture;
import net.stgl.ui.STGLMultiList;
import org.joml.Vector4f;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TradeNodeInfoWindow extends Cl2InternalFrame {
    private final TradeNode tradeNode;
    private final Country country;
    private final DynamicTextureAtlas flagAtlas;
    private final STGLFont monogram12;

    private Texture bg1;

    private STGLMultiList resList;

    public TradeNodeInfoWindow(int x, int y, TradeNode tradeNode, Country country,
                               DynamicTextureAtlas flagAtlas,
                               STGLFont monogram15, STGLFont monogram12) {
        super(x, y, 250, 300, 20,
                "Trade Node Information: " + tradeNode.getProvince().getName(), monogram15);
        this.tradeNode = tradeNode;
        this.country = country;
        this.flagAtlas = flagAtlas;
        this.monogram12 = monogram12;

        bg1 = new Texture(
                GenerateEnhancedBackground
                        .createButton(new Vector4f(0.5f, 0.5f, 0.5f, 1), 3, 210, 240));

        resList = new STGLMultiList(List.of(new STGLMultiList.DefaultColumn("Available resources", genColumn())),
                30, 40+32-monogram12.getMaxHeight()/2+40+monogram12.getMaxHeight(), 190, 150, 15, 20);
        resList.setForeground(Utils.colorToVec(new Color(198, 130, 77)));

        resList.setForeground(Utils.colorToVec(new Color(255, 255, 255)));
        resList.setBackground(Utils.colorToVec(new Color(107, 107, 107)));
        resList.setSelectedBackground(Utils.colorToVec(new Color(65, 65, 65)));
        resList.setSelectedForeground(Utils.colorToVec(Color.BLACK));
        resList.setFont(monogram12);
    }

    private List<String> genColumn(){
        ArrayList<String> res = new ArrayList<>();
        for (Resources.Resource r: tradeNode.getAvailableResources()){
            res.add(r.name());
        }
        return res;
    }

    @Override
    public void prePaint(STGLGraphics graphics) {
        super.prePaint(graphics);

        graphics.drawTexture(20, 40, bg1);
        GraphicUtils.drawFrameLikeRect(graphics, 20, 40, 210, 240);
        graphics.renderText(monogram12, 20+32+10+10, 40+32-monogram12.getMaxHeight()/2, "Our trade power: " + tradeNode.getTotalNodeTradePower(), STGLColors.WHITE);
        graphics.renderText(monogram12, 20+32+10+10, 40+32-monogram12.getMaxHeight()/2+40,
                String.format ("Trade control: %.1f" , tradeNode.getTradeInformation(country).getTradePower()/tradeNode.getTotalNodeTradePower()*100)+"%", STGLColors.WHITE);

        graphics.drawTextureRegionScaled(20+10, 20+20+10, flagAtlas.getTextureRegion(country.getFlag()), STGLColors.WHITE, 32, 32);

        // update columns

        resList.setColumns(List.of(new STGLMultiList.DefaultColumn("Available resources", genColumn())));

    }

    @Override
    public void initAdd() {
        super.initAdd();
        addComponent(resList);
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
        removeComponent(resList);
        bg1.dispose();
    }
}
