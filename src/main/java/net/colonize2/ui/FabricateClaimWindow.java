package net.colonize2.ui;

import net.colonize2.UIManager;
import net.colonize2.game.Country;
import net.colonize2.game.Province;
import net.colonize2.game.history.ProvinceClaim;
import net.stgl.STGLGraphics;
import net.stgl.color.STGLColors;
import net.stgl.font.STGLFont;
import net.stgl.ui.STGLClassicButton;

public class FabricateClaimWindow extends Cl2InternalFrame{
    private final UIManager manager;
    private final STGLFont monogram12;
    private final Country ourcountry;
    private final Country othercountry;

    private Province selectedClaimProvince = null;

    private final STGLClassicButton fabricateClaim;

    public FabricateClaimWindow(UIManager manager, int x, int y, STGLFont monogram15, STGLFont monogram12,
                                Country ourcountry, Country othercountry) {
        super(x, y, 250, 160, 30, "Select a province on the map", monogram15);
        this.manager = manager;
        this.monogram12 = monogram12;
        this.ourcountry = ourcountry;
        this.othercountry = othercountry;

        fabricateClaim = new STGLClassicButton(100, 35, 75, 160-35-5, "Fabricate a claim");
        fabricateClaim.setActionListener(() -> {
            if (selectedClaimProvince != null) {
                ourcountry.getDiplomacyManager().fabricateClaimOnAStateWithChecks(selectedClaimProvince);
                close();
            }
        });
        StalemateStyle.makeClassicButton(fabricateClaim);
        fabricateClaim.setFont(monogram12);

    }

    @Override
    public void prePaint(STGLGraphics graphics) {
        super.prePaint(graphics);

        if (manager.getSelectedProvince() != null) {
            boolean noClaims = true;
            for (ProvinceClaim claim: manager.getSelectedProvince().getClaims()){
                if (claim.getClaimant() == ourcountry) {
                    noClaims = false;
                    break;
                }
            }

            if (manager.getSelectedProvince().getOwner() == othercountry && noClaims){
                selectedClaimProvince = manager.getSelectedProvince();
            }
        }
        if (selectedClaimProvince == null)
            graphics.renderText(monogram12, (250-monogram12.getFontMetrics().getStringWidth("Province: "))/2,
                55, "Province: ", STGLColors.WHITE);
        else
            graphics.renderText(monogram12,
                    (250-monogram12.getFontMetrics().getStringWidth("Province: "
                            + selectedClaimProvince.getName()))/2,
                    55, "Province: " + selectedClaimProvince.getName(), STGLColors.WHITE);
    }

    @Override
    public void initAdd() {
        super.initAdd();
        addComponent(fabricateClaim);
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
        removeComponent(fabricateClaim);
    }
}
