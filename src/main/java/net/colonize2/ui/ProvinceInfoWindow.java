package net.colonize2.ui;

import net.colonize2.UIManager;
import net.colonize2.game.*;
import net.colonize2.game.history.ProvinceClaim;
import net.colonize2.ui.texgen.GenerateEnhancedBackground;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.color.STGLColors;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import net.stgl.texture.DynamicTextureAtlas;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import net.stgl.ui.STGLIconButton;
import org.joml.Vector2i;
import org.joml.Vector4f;

import java.awt.*;
import java.util.HashMap;

public class ProvinceInfoWindow extends Cl2InternalFrame{
    private final UIManager uiManager;
    private final Country c;
    private final Province pr;
    private final HumanPlayer player;
    private final Game game;
    private final STGLFont monogram12;
    private final STGLFont monogram;
    private final DynamicTextureAtlas flags;

    private final Texture bg1;
    private final Texture bg2;

    private STGLIconButton colonizeBtn;
    private STGLIconButton assimilateBtn;
    private STGLIconButton buildButton;
    private STGLIconButton tradeInformationBtn;

    public ProvinceInfoWindow(UIManager uiManager, int x, int y, STGLFont monogram15, Country c, Province pr, HumanPlayer player, Game g,
                              STGLFont monogram12, STGLFont monogram, DynamicTextureAtlas flags) {
        super(x, y, 500-65, 230, 20, "", monogram15);
        this.uiManager = uiManager;
        this.c = c;
        this.pr = pr;
        this.player = player;
        this.game = g;
        this.monogram12 = monogram12;
        this.monogram = monogram;
        this.flags = flags;

        bg1 = new Texture(
                GenerateEnhancedBackground
                        .createButton(new Vector4f(0.5f, 0.5f, 0.5f, 1), 3, 100+45+15, 105+monogram12.getMaxHeight()*2+20));
        bg2 = new Texture(
                GenerateEnhancedBackground
                        .createButton(new Vector4f(0.5f, 0.5f, 0.5f, 1), 3, 300-65+10-50, 53+10));

        try {
            colonizeBtn = new STGLIconButton(200-25+10+30, getHeight() - 165 + 120 + 15 + 5-15+15-3-25-3-5-100+5-16+4+2,
                    new STGLImage(TextureManager.loadBufferedImage("ui/colonize_btn.png")));
            colonizeBtn.setActionListener(() -> {
                if (c.colonizeWithChecks(pr, c, g)) {
                    for (Province province : pr.getNeighbours()) {
                        player.discoverProvince(province);
                        for (Province _province : province.getNeighbours()) {
                            player.discoverProvince(_province);
                        }
                    }
                    getComponentManager().invokeLater(() -> {
                        assimilateBtn.partiallyEnable();
                        buildButton.partiallyEnable();
                        colonizeBtn.partiallyDisable();
                    });
                }
            });
            StalemateStyle.makeIconButton(colonizeBtn);
            colonizeBtn.setPartiallyDisabledBackgroundColor(new Vector4f(0.6f,0.6f,0.6f,1));
            if (pr.getOwner() != null)
                colonizeBtn.partiallyDisable();

            assimilateBtn = new STGLIconButton(200-25+10+30+32, getHeight() - 165 + 120 + 15 + 5-15+15-3-25-3-5-100+5-16+4+2,
                    new STGLImage(TextureManager.loadBufferedImage("ui/assimilate_btn.png")));
            assimilateBtn.setActionListener(pr::assimilateWithChecks);
            if (pr.getOwner() != c)
                assimilateBtn.partiallyDisable();
            StalemateStyle.makeIconButton(assimilateBtn);
            assimilateBtn.setPartiallyDisabledBackgroundColor(new Vector4f(0.6f,0.6f,0.6f,1));

            buildButton = new STGLIconButton(200-25+10+30+32+32, getHeight() - 165 + 120 + 15 + 5-15+15-3-25-3-5-100+5-16+4+2,
                    new STGLImage(TextureManager.loadBufferedImage("ui/build_btn.png")));
            buildButton.setActionListener(() -> uiManager.spawnBuildMenuWindow(pr, c));
            StalemateStyle.makeIconButton(buildButton);
            if (pr.getOwner() != c)
                assimilateBtn.partiallyDisable();

            tradeInformationBtn = new STGLIconButton(200-25+10+30+32+32+32, getHeight() - 165 + 120 + 15 + 5-15+15-3-25-3-5-100+5-16+4+2,
                    new STGLImage(TextureManager.loadBufferedImage("ui/trade_information_btn.png")));
            StalemateStyle.makeIconButton(tradeInformationBtn);
            tradeInformationBtn.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(131, 99, 79)));
            tradeInformationBtn.setActionListener(() -> {
                uiManager.spawnTradeNodeInfoWindow(pr.getControlledTradeNode(), c);
            });
            if (pr.getControlledTradeNode() == null)
                tradeInformationBtn.partiallyDisable();
        } catch (Exception ignored){

        }

    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
        removeComponent(assimilateBtn);
        removeComponent(colonizeBtn);
        removeComponent(buildButton);
        removeComponent(tradeInformationBtn);
        bg1.dispose();
        bg2.dispose();
    }

    @Override
    public void initAdd() {
        super.initAdd();
        addComponent(assimilateBtn);
        addComponent(colonizeBtn);
        addComponent(buildButton);
        addComponent(tradeInformationBtn);
    }

    @Override
    public void prePaint(STGLGraphics graphics) {
        super.prePaint(graphics);

        graphics = graphics.childGraphics();
        graphics.setTranslationVector(new Vector2i(0, -10));

        graphics.drawTexture(35-10, getHeight()-165-monogram.getMaxHeight()-10, bg1);
        GraphicUtils.drawFrameLikeRect(graphics,35-10, getHeight()-165-monogram.getMaxHeight()-10, 100+45+15, 105+monogram12.getMaxHeight()*2+20);

        graphics.renderText(monogram, 35, getHeight()-165, pr.getName(), STGLColors.WHITE);

        graphics.renderText(monogram12, 35, getHeight()-165+15, "Culture: " + pr.getCulture().getCultureName(), STGLColors.WHITE);

        graphics.renderText(monogram12, 35, getHeight()-165+45-15, "Governor: " + (pr.getGovernor() == null
                ? "No governor" : pr.getGovernor().getName()+ " " + pr.getGovernor().getSurname()), STGLColors.WHITE);
        graphics.renderText(monogram12, 35, getHeight()-165+45+15-15, "Population: " + (pr.getPopulation()), STGLColors.WHITE);

        graphics.renderText(monogram12, 35, getHeight()-165+45+30-15, "Admin Level: " + (pr.getAdminLevel()), STGLColors.WHITE);
        graphics.renderText(monogram12, 35, getHeight()-165+45+45-15, "Production Level: " + (pr.getProductionLevel()), STGLColors.WHITE);
        graphics.renderText(monogram12, 35, getHeight()-165+105-15, "Manpower Level: " + (pr.getManpowerLevel()), STGLColors.WHITE);
        graphics.renderText(monogram12, 35, getHeight()-165+120-15, "Resource: " + (pr.getResource().toString()), STGLColors.WHITE);

        if (pr.getProvinceRank() == Province.ProvinceRank.COLONIAL_PROVINCE) {
            graphics.renderText(monogram12, 35, getHeight() - 165 + 120 + 15-15+10, "Colonization Progress ", STGLColors.WHITE);

            graphics.fillRect(35, getHeight() - 165 + 120 + 15 + 5-15+10, monogram12.getFontMetrics().getStringWidth("Colonization Progress"), 12, Utils.colorToVec(new Color(65, 65, 65)));
            graphics.fillRect(35, getHeight() - 165 + 120 + 15 + 5-15+10, (int) (monogram12.getFontMetrics().getStringWidth("Colonization Progress")*(pr.getColonizationProgress()/100f)),
                    12, Utils.colorToVec(new Color(15, 131, 15)));
        }

        graphics.renderText(monogram12, 35, getHeight() - 165 + 120 + 15 + 5-15 + 15+5+10, "Claims: ", STGLColors.WHITE);
        int clx = monogram12.getFontMetrics().getStringWidth("Claims: ")+35;
        for (ProvinceClaim claim: pr.getClaims()){
            graphics.drawTextureRegionScaled(clx, getHeight() - 165 + 120 + 15 + 5-15 + 15+5+10- monogram.getMaxHeight()+4, flags.getTextureRegion(claim.getClaimant().getFlag()), new Vector4f(1,1,1,1), 16, 16);
            clx+=17;
        }

        // draw cultures

        HashMap<String, Float> cp = new HashMap<>();
        HashMap<String, Vector4f> cp2 = new HashMap<>();
        for (UrbanPoint urbanPoint: pr.getUrbanPoints()){
            if (!cp.containsKey(urbanPoint.getCulture().getCultureName())){
                cp.put(urbanPoint.getCulture().getCultureName(), (float) urbanPoint.getPopulation());
                cp2.put(urbanPoint.getCulture().getCultureName(), urbanPoint.getCulture().getColor());
            }
            else{
                cp.put(urbanPoint.getCulture().getCultureName(), cp.get(urbanPoint.getCulture().getCultureName())+urbanPoint.getPopulation());
            }
        }
        cp.replaceAll((k, v) -> v / pr.getPopulation());

        HashMap<String, Float> fl = new HashMap<>();
        HashMap<String, Vector4f> cl = new HashMap<>();

        float lower_class = 0;
        float middle_class = 0;
        float higher_class = 0;

        for (UrbanPoint urbanPoint: pr.getUrbanPoints()){
            lower_class+=urbanPoint.getLowerClassProportion();
            middle_class+=urbanPoint.getMiddleClassProportion();
            higher_class+=urbanPoint.getHigherClassProportion();
        }

        lower_class/=pr.getUrbanPoints().size();
        middle_class/=pr.getUrbanPoints().size();
        higher_class/=pr.getUrbanPoints().size();

        fl.put("Higher Class", higher_class);
        fl.put("Middle Class", middle_class);
        fl.put("Lower Class", lower_class);

        cl.put("Higher Class", new Vector4f(1,1,0,1));
        cl.put("Middle Class", new Vector4f(1,0.5f,0,1));
        cl.put("Lower Class", new Vector4f(1,0,0,1));

        //
        graphics.drawTexture(200-25+10+30, getHeight() - 165 + 120 + 15 + 5-15+15-3-25-3-5, bg2);
        GraphicUtils.drawFrameLikeRect(graphics, 200-25+10+30, getHeight() - 165 + 120 + 15 + 5-15+15-3-25-3-5, 300-65+10-50, 53+10);
        GraphicUtils.drawCircleGraph(graphics, 200-25+10+30+10, getHeight() - 165 + 120 + 15 + 5-15+15-3-25-3-5+5, cp, cp2,monogram12);

        //
        graphics.drawTexture(200-25+10+30, getHeight() - 165 + 120 + 15 + 5-15+15-3-25-3-5-100+5+32, bg2);
        GraphicUtils.drawFrameLikeRect(graphics, 200-25+10+30, getHeight() - 165 + 120 + 15 + 5-15+15-3-25-3-5-100+5+32, 300-65+10-50, 53+10);
        GraphicUtils.drawCircleGraph(graphics, 200-25+10+30+10, getHeight() - 165 + 120 + 15 + 5-15+15-3-25-3-5-100+5+5+32, fl, cl,monogram12);

        // update trade node button if necessary

        if (tradeInformationBtn.isPartiallyDisabled())
            if (pr.getControlledTradeNode() != null)
                tradeInformationBtn.partiallyEnable();
    }
}
