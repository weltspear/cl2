package net.colonize2.ui;

import net.colonize2.game.Country;
import net.colonize2.game.Province;
import net.stgl.Utils;
import net.stgl.color.STGLColors;
import net.stgl.font.STGLFont;
import net.stgl.texture.DynamicTextureAtlas;
import net.stgl.ui.STGLClassicButton;
import net.stgl.ui.STGLMultiList;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class RivalSelectionWindow extends Cl2InternalFrame{
    private final STGLFont monogram12;
    private final DynamicTextureAtlas flags;
    private final Country playerCountry;

    private final STGLMultiList list;
    private final STGLClassicButton button;

    public RivalSelectionWindow(int x, int y,
                                STGLFont monogram15,
                                STGLFont monogram12, DynamicTextureAtlas flags,
                                Country playerCountry) {
        super(x, y, 300, 410, 30, "Select your rival", monogram15);
        this.monogram12 = monogram12;
        this.flags = flags;
        this.playerCountry = playerCountry;

        // valid rivals
        // fixme: do this in a more intelligent way
        HashSet<Country> neighbours = new HashSet<>();
        for (Province pr: playerCountry.getProvinces()){
            for (Province _pr: pr.getNeighbours()){
                if (_pr.getOwner() != null && _pr.getOwner() != playerCountry){
                    neighbours.add(_pr.getOwner());
                }
            }
        }

        ArrayList<String> country_names = new ArrayList<>();
        ArrayList<BufferedImage> country_flags = new ArrayList<>();
        ArrayList<Country> countries = new ArrayList<>(neighbours);
        for (Country c: neighbours){
            country_flags.add(c.getFlag());
            country_names.add(c.getName());
        }

        ArrayList<Integer> toBeRemoved = new ArrayList<>();
        for (int ci = 0; ci < countries.size(); ci++){
            Country c = countries.get(ci);
            if (playerCountry.getDiplomacyManager().getRivals().contains(c)){
                country_names.remove(ci);
                country_flags.remove(ci);
                toBeRemoved.add(ci);
            }
        }
        for (int i: toBeRemoved){
            countries.remove(i);
        }

        list = new STGLMultiList(List.of(new STGLMultiList.DefaultColumn("Country", country_names, (graphics, font, column, idx, x1, y1, is_selected) -> {
            graphics.drawTextureRegionScaled(x1, y1, flags.getTextureRegion(country_flags.get(idx)), STGLColors.WHITE, 12, 12);
            graphics.renderText(font, x1 +15, y1 +font.getMaxHeight(), column,STGLColors.WHITE);
        })), 25, 45+30, 250, 250, 16, 20);
        list.setForeground(Utils.colorToVec(new Color(198, 130, 77)));

        list.setForeground(Utils.colorToVec(new Color(255, 255, 255)));
        list.setBackground(Utils.colorToVec(new Color(107, 107, 107)));
        list.setSelectedBackground(Utils.colorToVec(new Color(65, 65, 65)));
        list.setSelectedForeground(Utils.colorToVec(Color.BLACK));
        list.setFont(monogram12);

        button = new STGLClassicButton(300-50, 35, 25, 345, "OK");

        StalemateStyle.makeClassicButton(button);
        button.setFont(monogram12);
        button.setActionListener(() -> {
            if (!country_names.isEmpty()){
                playerCountry.getDiplomacyManager().addRival(countries.get(list.getSelectedIdx()));
            }
            close();
        });
    }

    @Override
    public void initAdd() {
        super.initAdd();
        addComponent(list);
        addComponent(button);
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
    }
}
