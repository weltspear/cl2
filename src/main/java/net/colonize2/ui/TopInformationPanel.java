package net.colonize2.ui;

import net.colonize2.game.HumanPlayer;
import net.colonize2.ui.texgen.GenerateEnhancedBackground;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.color.STGLColors;
import net.stgl.font.STGLFont;
import net.stgl.state.ResourceManager;
import net.stgl.texture.DynamicTextureAtlas;
import net.stgl.texture.Texture;
import net.stgl.ui.STGLPanel;
import org.joml.Vector4f;

import java.awt.*;
import java.io.IOException;

public class TopInformationPanel extends STGLPanel {
    private final STGLFont monogram;
    private final STGLFont monogram12;
    private final HumanPlayer player;
    private final DynamicTextureAtlas flagAtlas;

    private final Texture bg;
    private final Texture bg2;
    private final Texture bg3;

    private final Texture frame_lr;
    private final Texture gold_t;
    private final Texture manpower_t;

    public TopInformationPanel(int x, int y,
                               STGLFont monogram, STGLFont monogram12,
                               HumanPlayer player, DynamicTextureAtlas flagAtlas,
                               ResourceManager primaryResourceManager) {
        super(x, y, 832, 64+16);
        this.monogram = monogram;
        this.monogram12 = monogram12;
        this.player = player;
        this.flagAtlas = flagAtlas;

        bg = new Texture(GenerateEnhancedBackground.createButton(Utils.colorToVec(new Color(96, 39, 2)), 1, getWidth(), getHeight()));
        bg2 = new Texture(GenerateEnhancedBackground.createButton(Utils.colorToVec(new Color(51, 39, 31)), 2, 150, 20));
        bg3 = new Texture(GenerateEnhancedBackground.createButton(Utils.colorToVec(new Color(51, 39, 31)), 2, 130, 20));

        try {
            frame_lr = primaryResourceManager.loadTexture("frame/frame_lr.png");
            gold_t = primaryResourceManager.loadTexture("ui/gold.png");
            manpower_t = primaryResourceManager.loadTexture("army/manpower.png");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void prePaint(STGLGraphics graphics) {
        //graphics.fillRect(0, 0, 832, 30, Utils.colorToVec(new Color(96, 39, 2)));
        graphics.drawTexture(0, 0, bg);
        graphics.drawTexture(10+32+10, 5+8+5, bg3);
        graphics.renderText(monogram, 15+32+10, 20+8+5, player.getCountry().getName(), Utils.colorToVec(new Color(198, 130, 77)));
        GraphicUtils.drawFrameLikeRect2(graphics, 10+32+10, 5+8+5, bg3.getWidth(), bg3.getHeight());

        graphics.drawTextureRegionScaled(7, 4+5, flagAtlas.getTextureRegion(player.getCountry().getFlag()), new Vector4f(1,1,1,1), 40, 40);
        graphics.drawRect(7, 4+5, 40, 40, new Vector4f(1,1,1,0.25f));

        graphics.drawTexture(10+180+30-10, 5+8+5-5-5+5, bg2);
        graphics.renderText(monogram12, 15+180+30-10, 18+8+5-5-5+5, "    " + player.getCountry().getGold(), Utils.colorToVec(new Color(198, 130, 77)));
        graphics.drawTexture(15+180+30-10, 18+8+5-gold_t.getHeight()/2-3-5-5+5, gold_t);

        graphics.renderText(monogram12, 15+180+30+monogram.getFontMetrics().getStringWidth("Gold: " + player.getCountry().getGold())-15-10, 18+8+5-5-5+5,
                "  " + player.getCountry().getAvailableManpower(), Utils.colorToVec(new Color(198, 130, 77)));
        graphics.drawTextureScaled(15+180+30+30+60-20-10, 18+8+5-gold_t.getHeight()/2-3-5-5+5, manpower_t, STGLColors.WHITE, 11, 15);
        GraphicUtils.drawFrameLikeRect2(graphics, 10+180+30-10, 5+8+5-5-5+5, bg2.getWidth(), bg2.getHeight());

        graphics.drawTexture(10+180+30-10,5+8+5-5-5+22+5, bg2);
        graphics.renderText(monogram12, 10+180+30-10,5+8+5-5-5+22+monogram12.getMaxHeight()+(bg2.getHeight()-monogram12.getMaxHeight())/2+5,
                " Political Power: " + player.getCountry().getPoliticalPower(), Utils.colorToVec(new Color(198, 130, 77)));
        GraphicUtils.drawFrameLikeRect2(graphics, 10+180+30-10,5+8+5-5-5+22+5, bg2.getWidth(), bg2.getHeight());


        if (!player.hasEndedTurn()){
            graphics.renderText(monogram12, 15+180+550, 18+8+5, "It is your turn", Utils.colorToVec(new Color(198, 130, 77)));
        }

        for (int x = 0; x < Math.ceil(getWidth()/(float)(frame_lr.getWidth())); x++){
            graphics.drawTexture(x*frame_lr.getWidth(), getHeight()-frame_lr.getHeight(), frame_lr);
        }

        GraphicUtils.drawFrameLikeRect(graphics, 1, 1, getWidth()-1, getHeight()-frame_lr.getHeight()-1);
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
        bg.dispose();
        bg2.dispose();
        bg3.dispose();
    }
}
