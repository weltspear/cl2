package net.colonize2.ui.texgen;

import net.stgl.Utils;
import org.joml.Math;
import org.joml.Vector4f;

import java.awt.*;
import java.awt.image.BufferedImage;

import static net.colonize2.Utils.clamp_v2c;
import static net.colonize2.gen.noise.NoiseUtils.*;

public class GenerateEnhancedBackground {
    public static float properFbm(long seed, float x, float y){
        //return noiseFbm(x, y, seed, 0.7f, 6, 0.45f, 1.6f);
        return turbulencePersistance(x, y, seed,0.7f, 6, 0.5f, 1.6f);
    }


    public static float properNoise(long seed, float x, float y, float scale){

        return (float) ((Math.clamp(0.5, 99999, (1.3f+
                properFbm(seed, x/scale, y/scale))/1.7f
        )));
    }

    public static BufferedImage createButton(Vector4f c, long seed, int width, int height){
        BufferedImage button = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        for (int y = 0; y < height; y++){
            for (int x = 0; x < width; x++){
                button.setRGB(x, y, clamp_v2c((new Vector4f(c).mul
                        (properNoise(seed, x, y, 15)))).getRGB());
            }
        }

        for (int x = 0; x < width; x++){
            for (int y = 0; y < height; y++) {
                Vector4f v = Utils.colorToVec(new Color(button.getRGB(x, y))).mul(Math.clamp(0.9f,1.2f,x/((float)width)));
                button.setRGB(x, y, clamp_v2c(v).getRGB());
            }
        }

        /*for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (x < width/(width/3.5f) || y < height/(height/3.5f)  || y > height-height/(height/3.5f) || x > width-width/(width/3.5f)){

                    Vector4f v = Utils.colorToVec(new Color(button.getRGB(x, y))).mul(0.5f);
                    button.setRGB(x, y, clamp_v2c(v).getRGB());

                }
            }
        }*/

        return button;
    }
}
