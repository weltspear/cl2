

package net.colonize2.ui;

import net.stgl.Utils;
import net.stgl.color.STGLColors;
import net.stgl.ui.*;
import org.joml.Vector4f;

import java.awt.*;

public class StalemateStyle {
    public static void makeButton(STGLButton button){
        button.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        button.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverForegroundColor(Utils.colorToVec(new Color(60, 38, 22)));
        button.setHoverBackgroundColor(Utils.colorToVec(new Color(198, 130, 77)));
        button.setPressedBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
        button.setPressedForegroundColor(Utils.colorToVec(Color.BLACK));
        button.setPartiallyDisabledForegroundColor(Utils.colorToVec(new Color(128,128,128)));
        button.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
    }

    public static void makeIconButton(STGLIconButton button){
        button.setHoverBackgroundColor(new Vector4f(0.75f, 0.75f, 0.75f, 1));
        button.setPressedBackgroundColor(new Vector4f(0.4f, 0.4f, 0.4f, 1));
        button.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
    }

    public static void makeCheckbox(STGLCheckbox checkbox){
        checkbox.setForeground(Utils.colorToVec(new Color(198, 130, 77)));
        checkbox.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        checkbox.setPartiallyDisabledForegroundColor(Utils.colorToVec(new Color(128,128,128)));
        checkbox.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
    }

    public static void makeEntry(STGLEntry entry){
        entry.setForeground(Utils.colorToVec(new Color(122, 82, 45)));
        entry.setBackground(Utils.colorToVec(new Color(60, 38, 22)));
        entry.setActiveBackgroundColor(Utils.colorToVec(new Color(60, 38, 22)));
        entry.setActiveForegroundColor(Utils.colorToVec(new Color(198, 130, 77)));
        entry.setPartiallyDisabledForegroundColor(Utils.colorToVec(new Color(128,128,128)));
        entry.setPartiallyDisabledBackgroundColor(Utils.colorToVec(new Color(51, 39, 31)));
    }

    public static void makeClassicButton(STGLClassicButton classicButton){
        classicButton.setForeground(STGLColors.WHITE);
        classicButton.setBackground(new Vector4f(1,1,1,0));
    }
}
