package net.colonize2.ui;

import net.stgl.STGLGraphics;
import net.stgl.font.STGLFont;
import org.joml.Vector4f;

import java.util.*;

public class GraphicUtils {

    public static void drawFrameLikeRect(STGLGraphics graphics, int x, int y, int width, int height){
        graphics.drawLine(x+1,y+1, x+1, y+height-1, new Vector4f(1,1,1,0.25f));
        graphics.drawLine(x,y+1, x+width, y+1, new Vector4f(1,1,1,0.25f));

        graphics.drawLine(x,y, x, y+height, new Vector4f(0,0,0,1));
        graphics.drawLine(x,y, x+width, y, new Vector4f(0,0,0,1));

        graphics.drawLine(x,y+height-1,
                x+width, y+height-1, new Vector4f(0,0,0,1));
        graphics.drawLine(x+width,y, x+width,
                y+height, new Vector4f(0,0,0,1));
    }

    public static void drawFrameLikeRect2(STGLGraphics graphics, int x, int y, int width, int height){
        graphics.drawLine(x,y, x, y+height, new Vector4f(0f,0f,0f,0.55f));
        graphics.drawLine(x,y, x+width, y, new Vector4f(0f,0f,0f,0.55f));

        graphics.drawLine(x,y+height-1,
                x+width, y+height-1, new Vector4f(1,1,1,0.15f));
        graphics.drawLine(x+width,y, x+width,
                y+height, new Vector4f(1,1,1,0.15f));
    }

    // stronger effect
    public static void drawFrameLikeRect3(STGLGraphics graphics, int x, int y, int width, int height){
        graphics.drawLine(x,y, x, y+height, new Vector4f(0f,0f,0f,0.65f));
        graphics.drawLine(x,y, x+width, y, new Vector4f(0f,0f,0f,0.65f));

        graphics.drawLine(x,y+height-1,
                x+width, y+height-1, new Vector4f(1,1,1,0.25f));
        graphics.drawLine(x+width,y, x+width,
                y+height, new Vector4f(1,1,1,0.25f));
    }

    public static void drawCircleGraph(STGLGraphics graphics, int x, int y, HashMap<String, Float> data,
                                       HashMap<String, Vector4f> colors, STGLFont font){
        int cur_point = 0;
        int yy = y;
        int i = 0;
        ArrayList<Map.Entry<String, Float>> d = new ArrayList<>(data.entrySet());
        d.sort(Map.Entry.comparingByValue());
        Collections.reverse(d);
        for (Map.Entry<String, Float> c: d) {
            int end = (int) (cur_point + Math.ceil((c.getValue()*35)));
            if (i + 1 != data.entrySet().size()) {
                graphics.partiallyFillCircle(x+25, y+25,
                        cur_point,
                        end < 36 ? end : 35,
                        35, 25, (colors.get(c.getKey())));
                cur_point += (int) Math.ceil((c.getValue()*35));

            } else {
                graphics.partiallyFillCircle(x+25, y+25,
                        cur_point,
                        35,
                        35, 25, colors.get(c.getKey()));
            }

            graphics.renderText(font, x+ 25 + 27 + 10, yy+12, c.getKey() + " ["
                    + (int)(c.getValue()*100) + "%]", (colors.get(c.getKey())));
            yy += font.getMaxHeight() + 2;

            i++;
        }
    }
}
