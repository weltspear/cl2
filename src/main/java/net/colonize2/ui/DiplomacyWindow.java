package net.colonize2.ui;

import net.colonize2.UIManager;
import net.colonize2.game.Country;
import net.colonize2.game.Game;
import net.colonize2.game.Province;
import net.colonize2.game.diplomacy.DiplomacyManager;
import net.colonize2.ui.texgen.GenerateEnhancedBackground;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.color.STGLColors;
import net.stgl.font.STGLFont;
import net.stgl.texture.DynamicTextureAtlas;
import net.stgl.texture.Texture;
import net.stgl.ui.STGLClassicButton;
import org.joml.Vector4f;

import java.awt.*;

public class DiplomacyWindow extends Cl2InternalFrame{

    private final UIManager uiManager;
    private final Province province;
    private final STGLFont monogram;
    private final STGLFont monogram12;
    private final Country ourCountry;
    private final Game g;
    private final DynamicTextureAtlas flagAtlas;

    private final STGLClassicButton improveRelations;
    private final STGLClassicButton sendAnInsult;
    private final STGLClassicButton offerProtection;
    private final STGLClassicButton rivalAddButton;
    private final STGLClassicButton acquireClaim;

    private final Texture bg1;
    private final Texture bg2;

    private RivalSelectionWindow rivalSelectionWindow = null;
    private FabricateClaimWindow claimWindow = null;

    public DiplomacyWindow(UIManager uiManager, int x, int y, STGLFont monogram15,
                           Province province,
                           STGLFont monogram, STGLFont monogram12,
                           Country ourCountry, Game g, DynamicTextureAtlas flagAtlas) {
        super(x, y, 270, 480, 30, "Diplomacy", monogram15);
        this.uiManager = uiManager;
        this.province = province;
        this.monogram = monogram;
        this.monogram12 = monogram12;
        this.ourCountry = ourCountry;
        this.g = g;
        this.flagAtlas = flagAtlas;

        if (ourCountry != province.getOwner()) {
            improveRelations = new STGLClassicButton(
                    getWidth()-30, 20,  15, 209, "Improve relations");
            improveRelations.setActionListener(() -> ourCountry.getDiplomacyManager().improveRelations(province.getOwner()));
            improveRelations.setFont(monogram12);
            StalemateStyle.makeClassicButton(improveRelations);

            sendAnInsult = new STGLClassicButton(getWidth()-30, 20,  15, 229, "Send an insult");
            sendAnInsult.setActionListener(() -> ourCountry.getDiplomacyManager().sendAnInsult(province.getOwner()));
            sendAnInsult.setFont(monogram12);
            StalemateStyle.makeClassicButton(sendAnInsult);

            offerProtection = new STGLClassicButton(getWidth()-30, 20,   15, 249,

                    !ourCountry.isOurPuppet(province.getOwner()) ? "Offer vassalization" :
                            "Integrate");
            offerProtection.setActionListener(() -> {
                if (!ourCountry.isOurPuppet(province.getOwner())) {
                    ourCountry.getDiplomacyManager().issuePuppetRequestToOther(province.getOwner());
                } else{
                    ourCountry.getDiplomacyManager().integrate(province.getOwner(), g);
                }
            });
            StalemateStyle.makeClassicButton(offerProtection);
            offerProtection.setFont(monogram12);

            acquireClaim = new STGLClassicButton(getWidth()-30, 20,  15, 269, "Fabricate a claim");
            StalemateStyle.makeClassicButton(acquireClaim);
            acquireClaim.setFont(monogram12);
            for (DiplomacyManager.DiplomaticProcess process: ourCountry.getDiplomacyManager().getDiplomaticProcesses()){
                if (process instanceof DiplomacyManager.FabricateClaimProcess fabricateClaimProcess){
                    if (fabricateClaimProcess.getProvince().getOwner() == province.getOwner()){
                        acquireClaim.disable();
                    }
                }
            }
            acquireClaim.setActionListener(() -> {
                if (claimWindow == null)
                    claimWindow = uiManager.spawnClaimWindow(province.getOwner());
            });

            rivalAddButton = null;
        }
        else{
            improveRelations = null;
            sendAnInsult = null;
            offerProtection = null;
            acquireClaim = null;

            int xr = 0;
            for (Country ignored : province.getOwner().getDiplomacyManager().getRivals()){
                xr += 18;
            }

            rivalAddButton = new STGLClassicButton(16, 16,
                    30+monogram12.getFontMetrics().getStringWidth("Rivals: ")+xr,
                    65+15-30+getY()+15+15+15+15+15+15+5-monogram12.getMaxHeight()+45, "+");
            rivalAddButton.setFont(monogram12);
            rivalAddButton.setActionListener(() -> {
                if (rivalSelectionWindow == null) {
                    if (ourCountry.getDiplomacyManager().getRivals().size() <
                            ourCountry.getDiplomacyManager().getMaxRivals()) {
                        //isRivalMenuRequested = true;
                    }
                    rivalSelectionWindow = uiManager.spawnRivalMenu();
                }
            });
            StalemateStyle.makeClassicButton(rivalAddButton);
        }

        bg1 = new Texture(GenerateEnhancedBackground.createButton(new Vector4f(0.5f, 0.5f, 0.5f, 1), 2, getWidth()-20, 400));
        bg2 = new Texture(GenerateEnhancedBackground.createButton(new Vector4f(0.3f, 0.3f, 0.3f, 1), 2, getWidth()-20, 260));
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
        bg1.dispose();
        bg2.dispose();
    }

    @Override
    public void initAdd() {
        super.initAdd();
        if (improveRelations != null) {
            addComponent(improveRelations);
            addComponent(sendAnInsult);
            addComponent(offerProtection);
            addComponent(acquireClaim);
        }
        if (rivalAddButton != null)
            addComponent(rivalAddButton);
    }

    @Override
    public void prePaint(STGLGraphics graphics) {
        super.prePaint(graphics);

        //
        if (acquireClaim != null) {
            boolean b = false;
            for (DiplomacyManager.DiplomaticProcess process : ourCountry.getDiplomacyManager().getDiplomaticProcesses()) {
                if (process instanceof DiplomacyManager.FabricateClaimProcess fabricateClaimProcess) {
                    if (fabricateClaimProcess.getProvince().getOwner() == province.getOwner()) {
                        acquireClaim.disable();
                        b = true;
                    }
                }
            }
            if (!b)
                if (!acquireClaim.isEnabled())
                    acquireClaim.enable();
        }
        //

        if (rivalSelectionWindow != null){
            if (rivalSelectionWindow.isClosed())
                rivalSelectionWindow = null;
        }

        if (claimWindow != null){
            if (claimWindow.isClosed())
                claimWindow = null;
        }

        graphics.drawTexture(30-10-10, 65-30+-32+3+45-15, bg1);

        if (province.getOwner().getOverlord() != null){
            graphics.renderText(monogram12, 20, +5+45, "Overlord: " + province.getOwner().getOverlord().getName()
                    , STGLColors.WHITE);
        }

        if (ourCountry.isOurPuppet(province.getOwner())){
            if (offerProtection != null)
                offerProtection.setText("Integrate");
        }

        graphics.drawTextureRegionScaled(30, 65-30+-32+3+45, flagAtlas.getTextureRegion(province.getOwner().getFlag()), STGLColors.WHITE, 32, 32);
        GraphicUtils.drawFrameLikeRect2(graphics, 30-10-10, 65-30+-32+3+45-15, getWidth()-20, 163);

        graphics.renderText(monogram, 30+40, 65-30+-7+45, province.getOwner().getName(), STGLColors.WHITE);
        graphics.renderText(monogram12, 30, 65+15-30+45, "Leader: " + ((province.getOwner().getLeader()) == null
                ? "(Interregnum)" : province.getOwner().getLeader().getName()+ " " + province.getOwner().getLeader().getSurname()
                + (" Age: " + province.getOwner().getLeader().getAge())), STGLColors.WHITE);

        graphics.renderText(monogram12, 30, 65+15*2-30+45, "System: " + province.getOwner().getGovernmentSystem().toString(),
                STGLColors.WHITE);
        graphics.renderText(monogram12, 30, 65+15*3-30+45, "Culture: " + province.getOwner().getCulture().getCultureName(),
                STGLColors.WHITE);
        if (province.getOwner().getRulingDynasty() != null){
            graphics.renderText(monogram12, 30, 65+15*4-30+45, "Dynasty: " + province.getOwner().getRulingDynasty().surname(),
                    STGLColors.WHITE);
        }

        graphics.drawTexture(10,199, bg2);
        GraphicUtils.drawFrameLikeRect2(graphics, 10,199, getWidth()-20, 400-163+20+3);
        if (ourCountry != province.getOwner()){
            graphics.fillRect(15, 65+15-30+15+15+5+15+45, getWidth()-30, 50/2+3*2, Utils.colorToVec(new Color(44, 35, 27)));
            graphics.renderText(monogram12, 30, 65+15-30+15+15+15+15+45, "Our opinion of them: ", new Vector4f(1,1,1,1));
            graphics.renderText(monogram12, 30+
                            monogram12.getFontMetrics().getStringWidth("Our opinion of them: "),
                    65+15-30+15+15+15+15+45, (ourCountry.getDiplomacyManager().getOpinion(province.getOwner()) > 0 ? "+":
                            "")+ourCountry.getDiplomacyManager().getOpinion(province.getOwner()) ,
                    (ourCountry.getDiplomacyManager().getOpinion(province.getOwner()) > 0 ? new Vector4f(0, 1, 0, 1):
                            ourCountry.getDiplomacyManager().getOpinion(province.getOwner()) == 0 ? new Vector4f(1,1,1,1):
                                    new Vector4f(1,0,0,1)));
            graphics.renderText(monogram12, 30, 65+15-30+15+15+15+15+15+45, "Their opinion of us: ", new Vector4f(1,1,1,1));
            graphics.renderText(monogram12, 30+
                            monogram12.getFontMetrics().getStringWidth("Their opinion of us: "),
                    65+15-30+15+15+15+15+15+45, (province.getOwner().getDiplomacyManager().getOpinion(ourCountry) > 0 ? "+":
                            "")+province.getOwner().getDiplomacyManager().getOpinion(ourCountry),
                    (province.getOwner().getDiplomacyManager().getOpinion(ourCountry) > 0 ? new Vector4f(0, 1, 0, 1):
                            province.getOwner().getDiplomacyManager().getOpinion(ourCountry) == 0 ? new Vector4f(1,1,1,1):
                                    new Vector4f(1,0,0,1)));

            //graphics.fillRect(20, 65+15-30+15+15+5+20+50/2+3*2-10-2+15+20+45, getWidth()-30, 50*5/2, Utils.colorToVec(new Color(51, 39, 31)));

        }

        //rivals
        graphics.renderText(monogram12, 30,
                65+15-30+15+15+15+15+15+15+5+45, "Rivals: ",
                STGLColors.WHITE);

        int xr = 0;

        for (Country c: province.getOwner().getDiplomacyManager().getRivals()){
            graphics.drawTextureRegionScaled(30+monogram12.getFontMetrics().getStringWidth("Rivals: ")+xr,
                    65+15-30+15+15+15+15+15+15+5-monogram12.getMaxHeight()+45,
                    flagAtlas.getTextureRegion(c.getFlag()), new Vector4f(1,1,1,1), 16, 16);
            xr+=18;
        }

        // handle rival button
        if (rivalAddButton != null) {
            if (!(ourCountry.getDiplomacyManager().getRivals().size() < ourCountry.getDiplomacyManager().getMaxRivals())){
                rivalAddButton.partiallyDisable();
            }else rivalAddButton.partiallyEnable();

            if ( 30 + monogram12.getFontMetrics().getStringWidth("Rivals: ") + xr >= rivalAddButton.getX()) {
                rivalAddButton.setX( 30 + monogram12.getFontMetrics().getStringWidth("Rivals: ") + xr);
            }
        }


    }


}
