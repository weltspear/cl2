package net.colonize2.ui;

import net.colonize2.UIManager;
import net.colonize2.game.Country;
import net.colonize2.game.Province;
import net.colonize2.ui.texgen.GenerateEnhancedBackground;
import net.stgl.STGLGraphics;
import net.stgl.color.STGLColors;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.STGLFont;
import net.stgl.state.ResourceManager;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import net.stgl.ui.STGLPanel;
import org.joml.Math;
import org.joml.Vector4f;

import java.io.IOException;

public class BuildMenuWindow extends Cl2InternalFrame{
    private final UIManager manager;
    private final ResourceManager resourceManager;
    private final STGLFont monogram12;
    private final Country c;
    private final Province pr;
    private final BuildMenuPanel panel = new BuildMenuPanel();

    private Texture bg1;

    private Texture free_slot;
    private Texture locked_slot;

    private class BuildMenuPanel extends STGLPanel{

        private int iHovered = -1;
        private int iPressed = -1;

        private class BuildMenuPanelListener implements MouseListener {

            public int calcI(double x, double y){
                if (x > 0 && x < BuildMenuPanel.this.getWidth() && y > 0 &&
                        y < BuildMenuPanel.this.getHeight()){
                    x-=15;
                    y-=10;
                    int i = (int) (Math.clamp(0,9,x/32));
                    if (Math.floor(y/32) > 0){
                        i += (int) (Math.floor(y/32f)*10);
                    }
                    return i;
                }
                return -1;
            }

            @Override
            public void mouseMoved(double x, double y) {
                iHovered = calcI(x, y);
            }

            @Override
            public void mousePressed(MousePress mouseButton, int glfw_mods, double x, double y) {
                iPressed = calcI(x, y);
            }

            @Override
            public void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y) {
                if (iPressed < pr.getBuildingSlotCount() && iPressed>=pr.getBuildings().size()) {
                    invokeLater(() -> {
                        if (pr.getOwner() == c)
                            manager.spawnConstructionMenu(pr, c);
                    });
                    iHovered = -1;
                }
                iPressed = -1;
            }

            @Override
            public void mouseWheelMoved(double xoffset, double yoffset) {

            }
        }

        public BuildMenuPanel() {
            super(0, 20, 350, 230);
        }

        private Vector4f hoverColor = new Vector4f(1.5f, 1.5f, 1.5f, 1);
        private Vector4f pressedColor = new Vector4f(0.5f, 0.5f, 0.5f, 1);

        private Vector4f colorBasedOnI(int i){
            return iPressed == i ? pressedColor: iHovered == i ? hoverColor : STGLColors.WHITE;
        }

        @Override
        public void prePaint(STGLGraphics graphics) {
            super.prePaint(graphics);
            graphics.drawTexture(15, 10, bg1);

            int x = 0;
            int y = 0;
            int i = 0;

            for (; i < pr.getBuildings().size(); i++){
                try {
                    graphics.drawTexture(15+x*32, 10+y*32, resourceManager.loadTexture(pr.getBuildings().get(i).buildingIcon()), colorBasedOnI(i));
                    if ((pr.getBuildings().get(i).getConstructionTime() != pr.getBuildings().get(i).getCurrentConstructionTime())){
                        graphics.fillRect(15+x*32, 10+y*32, 32, 6, new Vector4f(0.1f, 0.1f, 0.1f, 1));
                        graphics.fillRect(15+x*32, 10+y*32, (int)(32
                                *((float)pr.getBuildings().get(i).getCurrentConstructionTime())/pr.getBuildings().get(i).getConstructionTime()),
                                6, new Vector4f(0f, 1f, 0f, 1));
                    }
                } catch (IOException e) {
                    graphics.drawTexture(15+x*32, 10+y*32, locked_slot, colorBasedOnI(i));
                }
                x++;
                if (x == 10){
                    x = 0;
                    y++;
                }
            }

            int i_store = i;

            for (; i < i_store+ Math.max(pr.getBuildingSlotCount()-pr.getBuildings().size(), 0); i++){
                graphics.drawTexture(15+x*32, 10+y*32, free_slot,  colorBasedOnI(i));
                x++;
                if (x == 10){
                    x = 0;
                    y++;
                }
            }

            for (; i < 60; i++){
                graphics.drawTexture(15+x*32, 10+y*32, locked_slot,
                        colorBasedOnI(i));
                x++;
                if (x == 10){
                    x = 0;
                    y++;
                }
            }

            GraphicUtils.drawFrameLikeRect2(graphics, 15, 10, 10*32, 6*32);
        }

        private BuildMenuPanelListener listener = new BuildMenuPanelListener();

        @Override
        public void initAdd() {
            super.initAdd();
            this.addMouseListener(listener);
        }

        @Override
        public void deinitRemove() {
            super.deinitRemove();
            this.removeMouseListener(listener);
        }
    }

    public BuildMenuWindow(int x, int y, UIManager manager, ResourceManager resourceManager,
                           STGLFont monogram15, STGLFont monogram12,
                           Country c, Province pr) {
        super(x, y, 350, 250, 20, "Build - " + pr.getName(), monogram15);
        this.manager = manager;
        this.resourceManager = resourceManager;
        this.monogram12 = monogram12;
        this.c = c;
        this.pr = pr;

        bg1 = new Texture(
                GenerateEnhancedBackground
                        .createButton(new Vector4f(0.29f, 0.29f, 0.29f, 1),
                                3, 10*32, 6*32));

        try {
            free_slot = new Texture(TextureManager.loadBufferedImage("buildings/free_slot.png"));
            locked_slot = new Texture(TextureManager.loadBufferedImage("buildings/locked_slot.png"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void initAdd() {
        super.initAdd();
        addComponent(panel);
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
        removeComponent(panel);
    }
}
