package net.colonize2.ui;

import net.colonize2.game.Country;
import net.colonize2.game.Province;
import net.colonize2.game.buildings.BuildingConstructionEntry;
import net.colonize2.ui.texgen.GenerateEnhancedBackground;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.color.STGLColors;
import net.stgl.font.STGLFont;
import net.stgl.state.ResourceManager;
import net.stgl.texture.Texture;
import net.stgl.ui.STGLClassicButton;
import net.stgl.ui.STGLMultiList;
import org.joml.Vector4f;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StartConstructionWindow extends Cl2InternalFrame {
    private final ResourceManager resourceManager;
    private final Province pr;
    private final Country c;
    private final STGLFont monogram12;
    private final STGLFont monogram7;

    private final STGLMultiList list;
    private final STGLClassicButton buildButton;

    private final Texture bg1;

    public StartConstructionWindow(int x, int y, ResourceManager resourceManager,
                                   Province pr, Country c, STGLFont monogram15,
                                   STGLFont monogram12, STGLFont monogram7) {
        super(x, y, 400, 300, 20,
                "Select a building - " + pr.getName(), monogram15);
        this.resourceManager = resourceManager;
        this.pr = pr;
        this.c = c;
        this.monogram12 = monogram12;
        this.monogram7 = monogram7;

        ArrayList<String> buildingEntries = new ArrayList<>();
        for (BuildingConstructionEntry entry: pr.getConstructionEntries()){
            buildingEntries.add(entry.getBuildingName());
        }

        list = new STGLMultiList(List.of(new STGLMultiList.DefaultColumn("Buildings", buildingEntries)), 200, 20, 200, 275, 16, 20);
        list.setForeground(Utils.colorToVec(new Color(198, 130, 77)));

        list.setForeground(Utils.colorToVec(new Color(255, 255, 255)));
        list.setBackground(Utils.colorToVec(new Color(107, 107, 107)));
        list.setSelectedBackground(Utils.colorToVec(new Color(65, 65, 65)));
        list.setSelectedForeground(Utils.colorToVec(Color.BLACK));
        list.setFont(monogram12);

        buildButton = new STGLClassicButton(100, 25, 50, 273, "Build");
        buildButton.setFont(monogram12);
        StalemateStyle.makeClassicButton(buildButton);
        buildButton.setActionListener(() -> {
            if (!pr.getConstructionEntries().isEmpty()){
                BuildingConstructionEntry entry = pr.getConstructionEntries().get(list.getSelectedIdx());
                if (entry.buildWithChecks(pr, c)){
                    close();
                }
            }
        });

        bg1 = new Texture(
                GenerateEnhancedBackground
                        .createButton(new Vector4f(0.5f, 0.5f, 0.5f, 1), 3, 190, 233));
       // StalemateStyle.
    }

    @Override
    public void initAdd() {
        super.initAdd();
        addComponent(list);
        addComponent(buildButton);
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
        removeComponent(list);
        removeComponent(buildButton);
    }

    @Override
    public void prePaint(STGLGraphics graphics) {
        super.prePaint(graphics);

        graphics.drawTexture(5, 30, bg1);

        // update the columns

        ArrayList<String> buildingEntries = new ArrayList<>();
        for (BuildingConstructionEntry entry: pr.getConstructionEntries()){
            buildingEntries.add(entry.getBuildingName());
        }

        list.setColumns(List.of(new STGLMultiList.DefaultColumn("Buildings", buildingEntries)));

        BuildingConstructionEntry entry = getBuildingConstructionEntry();

        GraphicUtils.drawFrameLikeRect(graphics, 5, 30, 190, 233);
        try {
            graphics.drawTexture(7, 32, resourceManager.loadTexture(entry.getBuildingIcon()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (!(5 > 5+(190-getMonogram15().getFontMetrics()
                .getStringWidth(entry.getBuildingName()))/2 ||  5+(190-getMonogram15().getFontMetrics()
                .getStringWidth(entry.getBuildingName()))/2 < 32 + 5)) {
            graphics.renderText(getMonogram15(), 5 + (190 - getMonogram15().getFontMetrics()
                            .getStringWidth(entry.getBuildingName())) / 2,
                    30 + getMonogram15().getMaxHeight() * 2, entry.getBuildingName(), STGLColors.WHITE);
        }
        else{
            // render with smaller font if there is too much text
            graphics.renderText(monogram12, 5 + (190 - monogram12.getFontMetrics()
                            .getStringWidth(entry.getBuildingName())) / 2,
                    30 + monogram12.getMaxHeight() * 2 + 3, entry.getBuildingName(), STGLColors.WHITE);
        }
        graphics.renderText(monogram12, 16, 42*2, "Price: " + entry.getPrice(), STGLColors.WHITE);
        graphics.renderText(monogram12, 16, 42+42+12, "Description: ", STGLColors.WHITE);
        graphics.renderFormattedText(monogram12, monogram12, monogram12, monogram12, entry.getDescription()
                ,16, 42+42+12+12);
    }

    private BuildingConstructionEntry getBuildingConstructionEntry() {
        BuildingConstructionEntry entry;
        if (!pr.getConstructionEntries().isEmpty() && (list.getSelectedIdx() < pr.getConstructionEntries().size())){
            entry = pr.getConstructionEntries().get(list.getSelectedIdx());
        }
        else
            entry = new BuildingConstructionEntry(0, "buildings/free_slot.png", null,
                    "None", "None");
        return entry;
    }
}
