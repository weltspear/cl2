package net.colonize2.ui;

import net.colonize2.game.IPlayer;
import net.colonize2.game.PopupEvent;
import net.stgl.STGLGraphics;
import net.stgl.color.STGLColors;
import net.stgl.font.STGLFont;
import net.stgl.ui.STGLButton;
import net.stgl.ui.STGLClassicButton;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EventPopupWindow extends Cl2InternalFrame{
    private final STGLFont monogram12;

    private boolean isDone = false;

    private final List<String> message;
    private final ArrayList<STGLClassicButton> buttons = new ArrayList<>();

    public EventPopupWindow(int x, int y,
                            PopupEvent event, STGLFont monogram15,
                            STGLFont monogram12, IPlayer player) {
        super(x, y, 300, 370, 45, "Information", monogram15);
        this.monogram12 = monogram12;

        int i = 0;

        Collections.reverse(event.getOptions());

        for (var o: event.getOptions()){

            STGLClassicButton button = new STGLClassicButton(250, 32 + 10, 25, 360 - 45 - i*(42), o.getText());
            button.setForeground(STGLColors.WHITE);
            button.setBackground(new Vector4f(1, 1, 1, 0));
            button.setFont(monogram12);
            button.setActionListener(() -> {
                isDone = true;
                o.executeOption(player);
            });
            this.addComponent(button);
            i++;
        }

        setClosable(false);

        message = List.of(event.getText().split("\n"));
    }

    @Override
    public void initAdd() {
        super.initAdd();
    }

    @Override
    public void deinitRemove() {
        super.deinitRemove();
        for (STGLClassicButton button: buttons)
            removeComponent(button);
    }

    @Override
    public void prePaint(STGLGraphics graphics) {
        super.prePaint(graphics);
        int _y_m = 45 + 25;
        for (String s: message) {
            graphics.renderText(monogram12, 20, _y_m, s, STGLColors.WHITE);
            _y_m += 20;
        }
    }

    @Override
    public boolean isClosed() {
        return isDone || super.isClosed();
    }
}
