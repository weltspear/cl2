package net.colonize2.ui;

import net.colonize2.ui.texgen.GenerateEnhancedBackground;
import net.stgl.STGLGraphics;
import net.stgl.Utils;
import net.stgl.color.STGLColors;
import net.stgl.event.ActionListener;
import net.stgl.font.STGLFont;
import net.stgl.image.STGLImage;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import net.stgl.ui.STGLIconButton;
import net.stgl.ui.iframe.AbstractInternalFrame;
import org.jetbrains.annotations.NotNull;
import org.joml.Vector4f;

import java.awt.*;
import java.io.IOException;

public class Cl2InternalFrame extends AbstractInternalFrame {
    private final Texture bg1;
    private final Texture bg2;

    private final STGLIconButton close;
    private final STGLImage closeImg;
    private String title;
    private final STGLFont monogram15;

    private boolean closed = false;

    public Cl2InternalFrame(int x, int y, int width, int height, int topBarHeight,
                            String title, STGLFont monogram15) {
        super(x, y, width, height);
        this.title = title;
        this.monogram15 = monogram15;
        this.topBarHeight = topBarHeight;

        try {
            closeImg = new STGLImage(TextureManager.loadBufferedImage("ui/close.png"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        close = new STGLIconButton(width-closeImg.getWidth()*2,(topBarHeight-closeImg.getHeight())/2, closeImg);
        close.setActionListener(new ActionListener() {
            @Override
            public void action() {
                close();
            }
        });
        close.setHoverBackgroundColor(new Vector4f(1.25f, 1.25f, 1.25f, 1));
        close.setPressedBackgroundColor(new Vector4f(0.85f, 0.85f, 0.85f, 1));
        bg1 = new Texture(GenerateEnhancedBackground.createButton(Utils.colorToVec(new Color(192, 192, 192)), 1, getWidth(), getHeight()));
        bg2 = new Texture(GenerateEnhancedBackground.createButton(Utils.colorToVec(new Color(133, 133, 133)), 1, getWidth(), topBarHeight));
    }

    @Override
    public void setFocused(boolean b) {

    }

    @Override
    public boolean isClosed() {
        return closed;
    }

    @Override
    public boolean isMinimized() {
        return false;
    }

    @Override
    public void setMinimized(boolean b) {

    }

    @Override
    public @NotNull STGLImage getIcon() {
        return null;
    }

    @Override
    public void setClosable(boolean b) {
        if (!b)
            close.disable();
        else
            close.enable();
    }

    @Override
    public void setMinimizable(boolean b) {
        // ignored
    }

    private final AbstractInternalFrame.InternalFrameMouseListener internalFrameMouseListener = new InternalFrameMouseListener();

    @Override
    public void initAdd() {
        super.initAdd();
        getComponentManager().addMouseListener(internalFrameMouseListener);
        addComponent(close);
    }

    @Override
    public void deinitRemove() {
        getComponentManager().removeMouseListener(internalFrameMouseListener);
        removeComponent(close);
        closeImg.drop();
    }

    @Override
    public void prePaint(STGLGraphics graphics) {
        graphics.drawTexture(0,0,bg1);
        graphics.drawTexture(0,0,bg2);

        graphics.renderText(monogram15, (getWidth()- monogram15.getFontMetrics().getStringWidth(title))/2,(topBarHeight- monogram15.getMaxHeight())/2+ monogram15.getMaxHeight(), title, STGLColors.WHITE);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public STGLFont getMonogram15() {
        return monogram15;
    }

    public void close(){
        closed = true;
    }
}
