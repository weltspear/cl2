package net.colonize2.gen.noise;

public class NoiseUtils {
    public static float turbulence(float x, float y, float seed, int octaves,
                     float lacunarity, float gain)
    {
        float sum = 0;
        float frequency = 1.0f;
        float amplitude = 1.0f;
        for (int i=0; i < octaves; i++)
        {
            float n = OpenSimplex2S.noise2((long) (seed + i), x*frequency,y*frequency);
            sum += n*amplitude;
            frequency *= lacunarity;
            amplitude *= gain;
        }
        return sum;
    }

    public static float turbulenceFreq(float x, float y, float seed, float frequency, int octaves,
                                   float lacunarity, float gain)
    {
        float sum = 0;
        float amplitude = 1.0f;
        for (int i=0; i < octaves; i++)
        {
            float n = OpenSimplex2S.noise2((long) (seed + i), x*frequency,y*frequency);
            sum += n*amplitude;
            frequency *= lacunarity;
            amplitude *= gain;
        }
        return sum;
    }

    public static float turbulenceNormalized(float x, float y, float seed, int octaves,
                                             float lacunarity, float gain){
        float sum = 0;
        float frequency = 1.0f;
        float amplitude = 1.0f;
        float normalization = 0;
        for (int i=0; i < octaves; i++)
        {
            float n = OpenSimplex2S.noise2((long) (seed + i), x*frequency,y*frequency);
            sum += n*amplitude;
            frequency *= lacunarity;
            normalization += amplitude;
            amplitude *= gain;
        }
        return sum/normalization;
    }

    public static float turbulenceFreqNormalized(float x, float y, float seed, float frequency,
                                                 int octaves, float lacunarity, float gain){
        float sum = 0;
        float amplitude = 1.0f;
        float normalization = 0;
        for (int i=0; i < octaves; i++)
        {
            float n = OpenSimplex2S.noise2((long) (seed + i), x*frequency,y*frequency);
            sum += n*amplitude;
            frequency *= lacunarity;
            normalization += amplitude;
            amplitude *= gain;
        }
        return sum/normalization;
    }

    public static float turbulencePersistance(float x, float y, float seed, float persistence, int octaves,
                                             float lacunarity, float exponentiation){
        float gain = (float) Math.pow(2, persistence);
        float frequency = 1.0f;
        float amplitude = 1.0f;
        float normalization = 0;
        float sum = 0;
        for (int i=0; i < octaves; i++)
        {
            float n = OpenSimplex2S.noise2((long) (seed + i), x*frequency,y*frequency)*0.5f + 0.5f;
            sum += n*amplitude;
            frequency *= lacunarity;
            normalization += amplitude;
            amplitude *= gain;
        }
        return (float) Math.pow(sum/normalization, exponentiation);
    }

    public static float turbulenceBillow(float x, float y, float seed, int octaves,
                                   float lacunarity, float gain){
        return Math.abs(turbulence( x,  y,  seed,  octaves,
         lacunarity,  gain));
    }
}
