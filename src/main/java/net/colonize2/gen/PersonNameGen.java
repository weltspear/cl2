package net.colonize2.gen;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class PersonNameGen {

    private final ArrayList<String> firstnames_f;
    private final ArrayList<String> firstnames_m;
    private final ArrayList<String> surnames;

    private final Random random = new Random();

    @SuppressWarnings("all")
    public PersonNameGen(){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            firstnames_f = (ArrayList<String>) objectMapper.readValue(new File("name/firstnames_f.json"), ArrayList.class);
            firstnames_m = (ArrayList<String>)objectMapper.readValue(new File("name/firstnames_m.json"), ArrayList.class);
            surnames = (ArrayList<String>)objectMapper.readValue(new File("name/surnames.json"), ArrayList.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String genName(){
        if (random.nextInt(2) == 1) {
            return firstnames_f.get(random.nextInt(firstnames_f.size()));
        }
        else {
            return firstnames_m.get(random.nextInt(firstnames_m.size()));
        }
    }

    public String genSurname(){
        return surnames.get(random.nextInt(surnames.size()));
    }
}
