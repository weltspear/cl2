package net.colonize2.gen;

import net.colonize2.render.ProvinceSimpleData;
import org.joml.Vector2i;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class ProvinceGen {

    private final Random random = new Random();

    //private final NameGen nameGen = new NameGen();

    private final int province_area;

    private final int width;
    private final int height;

    private int r = 0;
    private int g = 128;
    private int b = 128;

    private final HashMap<Integer, PrData> creeval = new HashMap<>();
    private final HashMap<Integer, Integer> colors = new HashMap<>();

    private int[][] provinces;

    public static class PrData{
        public ArrayList<Vector2i> coords = new ArrayList<>();
        public Vector2i center = new Vector2i(0,0);
        public boolean isSea = false;

        public ArrayList<Vector2i> getCoords() {
            return coords;
        }
    }

    private BufferedImage primage;

    private Color increaseRGB(){
        if (b+1 < 256){
            b++;
            return new Color(r, g, b);
        }
        else{
            if (g+1 < 256){
                g++;
                b=0;
                return new Color(r, g, b);
            }
            else{
                if (r+1 < 256){
                    r++;
                    g = 0;
                    b = 0;
                    return new Color(r, g, b);
                }
            }
        }

        return Color.black;
    }

    int cPr = 0;
    private int increaseProvince(){
        cPr++;
        return cPr;
    }

    public interface SeaProvinceCheckProvince{
        boolean isSea(int x, int y);
    }

    private SeaProvinceCheckProvince seaProvinceCheckProvince = null;

    public ProvinceGen(int province_area, int len, SeaProvinceCheckProvince seaProvinceCheckProvince) {
        this.province_area = province_area;
        this.width = len;
        this.seaProvinceCheckProvince = seaProvinceCheckProvince;
        this.height = width;
        stupid3();
    }

    public void stupid3() {

        int prnum = 1;

        provinces = new int[width][height];

        ggpr(provinces, new Vector2i(0, 0));

        // edit sea colors
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (seaProvinceCheckProvince.isSea(x, y)){
                    provinces[x][y]*=20;
                }

                int _c = provinces[x][y];
                if (!creeval.containsKey(provinces[x][y])) {
                    creeval.put(_c, new PrData());
                    if (seaProvinceCheckProvince.isSea(x, y)){
                        creeval.get(_c).isSea = true;
                    }
                }
                creeval.get(_c).coords.add(new Vector2i(x, y));
                creeval.get(_c).center.add(new Vector2i(x, y));

            }
        }


        for (Map.Entry<Integer, PrData> entry: creeval.entrySet()){
            entry.getValue().center.div(entry.getValue().getCoords().size());
        }

        // generate creval

        // annex small
        boolean[][] closed = new boolean[width][height];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (!closed[y][x]) {
                    if (creeval.get(provinces[x][y]).coords.size() < province_area) {
                        ArrayList<Vector2i> store = creeval.get(provinces[x][y]).coords;
                        int drgb = integrateIfSmall(x, y, provinces, closed, creeval.get(provinces[x][y]).coords);
                        creeval.get(drgb).coords.addAll(store);
                    }
                }
            }
        }

        /*// write provinces
        */

        BufferedImage gen = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++){
                if (!colors.containsKey(provinces[x][y])){
                    colors.put(provinces[x][y], increaseRGB().getRGB());
                }
                gen.setRGB(x, y, colors.get(provinces[x][y]));
            }

        primage = gen;

        //writer.close();
    }

    public void ggpr(int[][] gen, Vector2i _start){
        //ArrayList<Vec>
        ArrayList<Vector2i> to_be_done = new ArrayList<>();
        to_be_done.add(_start);

        int s = to_be_done.size();

        for (int i = 0; i < s; i++) {
            if (gen[to_be_done.get(i).x][ to_be_done.get(i).y] != 0)
                continue;

            int c = increaseRGB().getRGB();
            int arn = province_area;
            ArrayList<Vector2i> open = new ArrayList<>();
            ArrayList<Vector2i> d = new ArrayList<>();
            ArrayList<Vector2i> closed = new ArrayList<>();

            //Vector2i avg = new Vector2i();
            open.add(to_be_done.get(i));
            while (!open.isEmpty()) {
                Vector2i p = open.get(0);
                open.remove(0);
                if (gen[p.x][p.y] != 0) {
                    continue;
                }
                closed.add(p);

                gen[p.x][p.y] = c;
                //avg.add(p);

                arn--;
                if (arn == 0) {
                    break;
                }

                ArrayList<Vector2i> neighbours = new ArrayList<>();
                neighbours.add(new Vector2i(p).add(1, 0));
                neighbours.add(new Vector2i(p).add(-1, 0));
                neighbours.add(new Vector2i(p).add(0, -1));
                neighbours.add(new Vector2i(p).add(0, 1));
                for (Vector2i v : neighbours) {
                    if ((v.x >= width || v.y >= height)) {
                        continue;
                    }
                    if ((v.x < 0 || v.y < 0)) {
                        continue;
                    }
                    if (gen[v.x][v.y] == 0) {
                        if (random.nextBoolean())
                            open.add(v);
                        else d.add(v);
                    }

                }

            }
            //creeval.put(c, new PrData(closed, avg.div(closed.size())));

            for (Vector2i v: open){
                if (gen[v.x][v.y] == 0)
                    to_be_done.add(v);
            }

            for (Vector2i v: d){
                if (gen[v.x][v.y] == 0)
                    to_be_done.add(v);
            }

            s = to_be_done.size();
        }
    }

    private int integrateIfSmall(int x, int y, int[][] provinces, boolean[][] closed, ArrayList<Vector2i> pr_coords) {

        ArrayList<Integer> rgbs = new ArrayList<>();
        ArrayList<Integer> nums = new ArrayList<>();

        for (Vector2i v : pr_coords) {
            closed[v.y][v.x] = true;
        }

        // gud neighbours

        for (Vector2i f : pr_coords) {
            ArrayList<Vector2i> neighbours = new ArrayList<>();
            neighbours.add(new Vector2i(f.x + 1, f.y));
            neighbours.add(new Vector2i(f.x - 1, f.y));
            neighbours.add(new Vector2i(f.x, f.y + 1));
            neighbours.add(new Vector2i(f.x, f.y - 1));

            for (Vector2i p : neighbours) {
                if (!(p.x < width && p.y < height)) {
                    continue;
                }

                if (p.x < 0 || p.y < 0) {
                    continue;
                }

                if (provinces[x][y] != provinces[p.x][p.y]) {
                    if (rgbs.contains(provinces[p.x][p.y])) {
                        nums.set(rgbs.indexOf(provinces[p.x][p.y]), nums.get(rgbs.indexOf(provinces[p.x][p.y])) + 1);
                    } else {
                        rgbs.add(provinces[p.x][p.y]);
                        nums.add(0);
                    }
                }

            }
        }

        int origrgb = provinces[x][y];

        boolean isSea_cr = creeval.get(origrgb).isSea;

        for (int drgb_idx = 0; drgb_idx < rgbs.size(); drgb_idx++){
            if (creeval.get(rgbs.get(drgb_idx)).isSea != isSea_cr){
                rgbs.remove(drgb_idx);
                nums.remove(drgb_idx);
                drgb_idx--;
            }
        }

        int drgb;
        try {
            drgb = rgbs.get(nums.indexOf(Collections.max(nums)));
        } catch (Exception e) {
            drgb = origrgb;
        }

        for (Vector2i v : pr_coords) {
            provinces[v.x][v.y] = drgb;
        }
        return drgb;
    }

    HashMap<Integer, ProvinceSimpleData> pr2color = new HashMap<>();

    public HashMap<Integer, ProvinceSimpleData> getColorToProvince(){
        return pr2color;
    }

    public BufferedImage getProvinceImage() {
        return primage;
    }

    public void writeDefinitions(String path) throws IOException {

        NameGen nameGen = new NameGen();
        int prnum = 0;
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        StringBuilder out = new StringBuilder();
        HashMap<Integer, Boolean> prsdone = new HashMap<>();
        for (int x = 0; x < primage.getWidth(); x++) {
            for (int y = 0; y < primage.getHeight(); y++) {
                if (prsdone.get(provinces[x][y]) == null) {
                    Color color = new Color(primage.getRGB(x, y));
                    prsdone.put(provinces[x][y], true);
                    String prname = nameGen.genRegionName();

                    boolean isSea = creeval.get(provinces[x][y]).isSea;

                    pr2color.put(color.getRGB(), new ProvinceSimpleData(color.getRGB(), prnum, prname, isSea));

                    out.append(prnum).append(";")
                            .append(color.getRed()).append(";")
                            .append(color.getGreen()).append(";")
                            .append(color.getBlue()).append(";")
                            .append(isSea ? "Sea of " : "").append(prname).append(";")
                            .append(isSea).append('\n');
                    prnum++;
                }
            }
        }
        writer.write(String.valueOf(out));
        writer.flush();
        writer.close();

    }
}
