package net.colonize2.gen;

import net.colonize2.gen.noise.NoiseUtils;
import org.joml.Math;
import org.joml.Vector3f;

import java.awt.*;
import java.awt.image.BufferedImage;


public class GenerateTerrain {

    public static int WATER = new Color(21, 21, 136).getRGB();
    public static int DEEP_WATER = new Color(0, 0, 128).getRGB();
    private static final int SAND = new Color(220, 220, 0).getRGB();
    private static final int GRASS = new Color(6, 189, 6).getRGB();
    private static final int FOREST = new Color(4, 105, 4).getRGB();
    public static int MOUNTAIN = new Color(70, 70, 70).getRGB();
    private static final int SNOW = Color.WHITE.getRGB();

    private static final int DRYEST = new Color(255, 115, 0).getRGB();
    private static final int DRYER = new Color(255, 196, 0).getRGB();
    private static final int DRY = new Color(132, 255, 0).getRGB();
    private static final int WET = new Color(0, 255, 255).getRGB();
    private static final int WETTER = new Color(0, 128, 255).getRGB();
    private static final int WETTEST = new Color(0, 60, 255).getRGB();

    private static final int HOTTEST = Color.RED.getRGB();
    private static final int HOTTER = new Color(255, 89,0).getRGB();
    private static final int HOT = new Color(255, 213,0).getRGB();
    private static final int COLD = new Color(55, 255,0).getRGB();
    private static final int COLDER = new Color(154, 220, 220).getRGB();
    private static final int COLDEST = new Color(37, 112, 255).getRGB();

    public static int ICE = Color.WHITE.getRGB();
    public static int DESERT = new Color(238, 218, 130).getRGB();
    public static int SAVANNA = new Color(177, 209, 110).getRGB();
    public static int TROPICAL_RAINFOREST = new Color(66, 123, 25).getRGB();
    public static int TUNDRA = new Color(96, 131, 112).getRGB();
    public static int TEMPERATE_RAINFOREST = new Color(29, 73,40).getRGB();
    public static int GRASSLAND = new Color(164, 225, 99).getRGB();
    public static int SEASONAL_FOREST = new Color(73, 100, 35).getRGB();
    public static int BOREAL_FOREST = new Color(95, 115, 62).getRGB();
    public static int WOODLAND = new Color(139, 175, 90).getRGB();

    private float[][] heightmap;
    private BufferedImage _heightmap;

    public GenerateTerrain(){

    }

    public static float properFbm2(float x, float y, long seed){
        return NoiseUtils.turbulenceFreq(x, y, seed, 1.25f,12, 2, 0.5f);
    }

    public static float properFbm3(float x, float y, long seed){
        return NoiseUtils.turbulenceFreq(x, y, seed, 1.25f,12, 2f, 0.5f);
    }

    public static float moistureNoise(float x, float y, long seed){
        return ((1+ NoiseUtils.turbulenceFreqNormalized(x/2f, y/2f, seed, 3f,4, 2.2f, 0.5f)))/2f;
    }

    public BufferedImage generateMap(float width, float height, long seed){
        heightmap = new float[(int) width][(int) height];
        float[][] heightmap2 = new float[(int) width][(int) height];
        float max_value = -999999999;
        float min_value = 999999999;

        float max_value2 = -999999999;
        float min_value2 = 999999999;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                heightmap[x][y] = properFbm2( x/1500f, y/1500f, seed);
                if (heightmap[x][y] < min_value){
                    min_value = heightmap[x][y];
                }
                if (heightmap[x][y] > max_value){
                    max_value = heightmap[x][y];
                }

                heightmap2[x][y] = properFbm3( x/300f, y/300f, seed);
                if (heightmap2[x][y] < min_value2){
                    min_value2 = heightmap2[x][y];
                }
                if (heightmap2[x][y] > max_value2){
                    max_value2 = heightmap2[x][y];
                }
            }
        }

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                heightmap[x][y] = (heightmap[x][y]-min_value)/(max_value-min_value);
                heightmap2[x][y] = (heightmap2[x][y]-min_value2)/(max_value2-min_value2);
                if (heightmap[x][y] > 0.56){
                    heightmap[x][y] = Math.clamp(0, heightmap[x][y]+0.1f,heightmap2[x][y]*0.44f+0.56f);
                }
            }
        }

        _heightmap = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                float h = (extendHeight(heightmap[x][y])+1)/2;
                int _h = (int) (255*h);
                _heightmap.setRGB(x, y, new Color(_h, _h, _h).getRGB());
            }
        }

        BufferedImage terrain = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++){
            for (int x = 0; x < width; x++){
                if (heightmap[x][y] > 0.56f) {
                    if (heightmap[x][y] < 0.60f) {
                        terrain.setRGB(x, y, SAND);
                    }
                    else if (heightmap[x][y] < 0.72f){
                        terrain.setRGB(x, y, GRASS);
                    }
                    else if (heightmap[x][y] < 0.80f){
                        terrain.setRGB(x, y, FOREST);
                    }
                    else if (heightmap[x][y] < 0.91f){
                        terrain.setRGB(x, y, MOUNTAIN);
                    }
                    else
                    terrain.setRGB(x, y, SNOW);
                }
                else if (heightmap[x][y] > 0.5f) terrain.setRGB(x, y, WATER);
                else terrain.setRGB(x, y, DEEP_WATER);
            }
        }

        BufferedImage heatmap = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++){
            for (int x = 0; x < width; x++){
                float coeff = Math.abs((height/2-y)/(height/2))*moistureNoise(x/256f, y/256f, seed+16);
                coeff+=((heightmap[x][y]))/2;

                if (coeff < 0.2f){
                    heatmap.setRGB(x, y, HOTTEST);
                }
                else if (coeff < 0.3f){
                    heatmap.setRGB(x, y, HOTTER);
                }
                else if (coeff < 0.5f){
                    heatmap.setRGB(x, y, HOT);
                }
                else if (coeff < 0.7f){
                    heatmap.setRGB(x, y, COLD);
                }
                else if (coeff < 0.9f){
                    heatmap.setRGB(x, y, COLDER);
                }
                else{
                    heatmap.setRGB(x, y, COLDEST);
                }
            }
        }


        BufferedImage moisture = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                float coeff = moistureNoise(x/800f, y/800f, seed+16);

                if (terrain.getRGB(x,y) == DEEP_WATER){
                    coeff/=10;
                }
                if (terrain.getRGB(x,y) == WATER){
                    coeff/=5;
                }
                if (terrain.getRGB(x,y) == SAND){
                    coeff*=1.2f;
                }

                if (coeff < 0.4f){
                    moisture.setRGB(x, y, WETTEST);
                }
                else if (coeff < 0.45f){
                    moisture.setRGB(x, y, WETTER);
                }
                else if (coeff < 0.5f){
                    moisture.setRGB(x, y, WET);
                }
                else if (coeff < 0.6f){
                    moisture.setRGB(x, y, DRY);
                }
                else if (coeff < 0.7f){
                    moisture.setRGB(x, y, DRYER);
                }
                else {
                    moisture.setRGB(x, y, DRYEST);
                }

            }
        }

        BufferedImage terrain2 = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_RGB);

        for (int y = 0; y < height; y++){
            for (int x = 0; x < width; x++){
                if (terrain.getRGB(x, y) != WATER && terrain.getRGB(x, y) != DEEP_WATER && terrain.getRGB(x, y) != SNOW
                        && terrain.getRGB(x, y) != MOUNTAIN) {
                    terrain2.setRGB(x, y, biomeFromMoistureHeatmap(moisture, heatmap, x, y));
                }
                else terrain2.setRGB(x, y, terrain.getRGB(x, y));
            }
        }

        return terrain2;
    }

    private static int biomeFromMoistureHeatmap(BufferedImage moisture, BufferedImage heatmap, int x, int y){
        if (heatmap.getRGB(x, y) == COLDEST){
            return ICE;
        }
        else if (heatmap.getRGB(x, y) == COLDER){
            return TUNDRA;
        }
        //
        else if (heatmap.getRGB(x, y) == COLD && (moisture.getRGB(x, y) == DRYER
                || moisture.getRGB(x, y) == DRYEST)){
            return GRASSLAND;
        }
        else if (heatmap.getRGB(x, y) == COLD && (moisture.getRGB(x, y) == DRY)){
            return WOODLAND;
        }
        else if (heatmap.getRGB(x, y) == COLD && (moisture.getRGB(x, y) == WET
                || moisture.getRGB(x, y) == WETTER || moisture.getRGB(x, y) == WETTEST)){
            return BOREAL_FOREST;
        }
        //
        else if (heatmap.getRGB(x, y) == HOT && (moisture.getRGB(x, y) == DRYER
                || moisture.getRGB(x, y) == DRYEST)){
            return DESERT;
        }
        else if (heatmap.getRGB(x, y) == HOT && (moisture.getRGB(x, y) == DRY || moisture.getRGB(x, y) == WET)){
            return WOODLAND;
        }
        else if (heatmap.getRGB(x, y) == HOT && (moisture.getRGB(x, y) == WETTER)){
            return SEASONAL_FOREST;
        }
        else if (heatmap.getRGB(x, y) == HOT && (moisture.getRGB(x, y) == WETTEST)){
            return TEMPERATE_RAINFOREST;
        }
        //
        else if (heatmap.getRGB(x, y) == HOTTER && (moisture.getRGB(x, y) == DRYER
                || moisture.getRGB(x, y) == DRYEST)){
            return DESERT;
        }
        else if (heatmap.getRGB(x, y) == HOTTER && (moisture.getRGB(x, y) == DRY || moisture.getRGB(x, y) == WET)){
            return SAVANNA;
        }
        else if (heatmap.getRGB(x, y) == HOTTER && (moisture.getRGB(x, y) == WETTER || moisture.getRGB(x, y) == WETTEST)){
            return TROPICAL_RAINFOREST;
        }
        //
        else if (heatmap.getRGB(x, y) == HOTTEST && (moisture.getRGB(x, y) == DRYER
                || moisture.getRGB(x, y) == DRYEST)){
            return DESERT;
        }
        else if (heatmap.getRGB(x, y) == HOTTEST && (moisture.getRGB(x, y) == DRY || moisture.getRGB(x, y) == WET)){
            return SAVANNA;
        }
        else {
            return TROPICAL_RAINFOREST;
        }
    }

    public float[][] getHeightmap() {
        return heightmap;
    }

    private static float safeGetImage(int x, int y, BufferedImage img){
        return (new Color(img.getRGB(Math.clamp(0, img.getWidth()-1, x),
                Math.clamp(0, img.getHeight()-1, y))).getRed()/255f)*2-1;
    }

    public static BufferedImage generateNormal(BufferedImage heightmap){
        BufferedImage normals = new BufferedImage(heightmap.getWidth(),
                heightmap.getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < heightmap.getHeight(); y++){
            for (int x = 0; x < heightmap.getWidth(); x++){

                int offset = 1;

                float left = safeGetImage(x-offset, y, heightmap);
                float right = safeGetImage(x+offset, y, heightmap);
                float up = safeGetImage(x, y-offset, heightmap);
                float down = safeGetImage(x, y+offset, heightmap);
                Vector3f va = new Vector3f(-1,0,right-left).normalize();
                Vector3f vb = new Vector3f(0,-1,down-up).normalize();

                Vector3f normal = va.cross(vb).normalize();

                int r = (int) ((1+normal.x)*255f/2);
                int g = (int) ((1+normal.y)*255f/2);
                int b = (int) ((1+normal.z)*255f/2);

                normals.setRGB(x, y, new Color(r, g, b).getRGB());
            }
        }
        return normals;
    }

    public BufferedImage getHeightmapImg() {
        return _heightmap;
    }
    // [0,1] -> [-1,1]
    public static float extendHeight(float h){
        if (h < 0.56f){
            h*=0.8f;
        }

        return h*2-1;
    }
}