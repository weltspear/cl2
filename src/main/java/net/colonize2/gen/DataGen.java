package net.colonize2.gen;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.colonize2.game.Resources;
import net.colonize2.game.UrbanPointType;
import net.colonize2.render.ProvinceSimpleData;
import org.joml.Vector2i;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.stream.IntStream;

public class DataGen {

    public static final String[] const_positions = new String[]{"politician",
            "scientist",
            "peasant_leader",
            "factory_owner",
            "military_officer"};

    public record GeneratedProvinceData(
            String pr_name,
            int id,
            int land_amount,
            int mountain_amount,
            int cold_land_amount,
            int tropical_land_amount,
            int desert_land,
            String resource_id,
            //int population,
            float population_growth,

            int production_level,
            int admin_level,
            int manpower_level,

            int owner_id,

            String culture,

            String minority_culture,

            int avg_x,
            int avg_y,

            ArrayList<Integer> neighbouring_provinces,
            ArrayList<GeneratedUrbanPointData> urban_points


    ) {
    }

    public record GeneratedSeaProvinceData(
            String pr_name,
            int id,
            int avg_x,
            int avg_y,

            ArrayList<Integer> neighbouring_provinces
    ){

    }

    public record GeneratedUrbanPointData(
            int population,
            String name,
            UrbanPointType urbanPointType,
            String culture
    ){

    }

    public record GeneratedPersonData(
            String name,
            String surname,
            int admin_skill,
            int production_skill,
            int mil_skill,
            String[] traits,
            int location,
            String position,

            String culture,

            int id,

            int age
    ) {

    }

    public record GeneratedCountryData(int leader_id, int governor_id, int country_id, String country_name){

    }

    private record TribalNode(String culture, Vector2i coord, int power){

    }

    public DataGen(BufferedImage provinces, HashMap<Integer, ProvinceSimpleData> color2pr, BufferedImage terrain) throws IOException {

        PersonNameGen personNameGen = new PersonNameGen();

        HashMap<Integer, Integer> color2land = new HashMap<>();
        HashMap<Integer, Integer> color2mountain = new HashMap<>();
        HashMap<Integer, Integer> color2ice = new HashMap<>();
        HashMap<Integer, Integer> color2desert = new HashMap<>();
        HashMap<Integer, Integer> color2savanna = new HashMap<>();
        HashMap<Integer, Integer> color2tropicalRainforest = new HashMap<>();
        HashMap<Integer, Integer> color2tundra = new HashMap<>();
        HashMap<Integer, Integer> color2temperateRainforest = new HashMap<>();
        HashMap<Integer, Integer> color2grassland = new HashMap<>();
        HashMap<Integer, Integer> color2seasonalForest = new HashMap<>();
        HashMap<Integer, Integer> color2borealForest = new HashMap<>();
        HashMap<Integer, Integer> color2woodland = new HashMap<>();

        HashMap<Integer, List<Vector2i>> color2avgcoord = new HashMap<>();

        ArrayList<GeneratedProvinceData> provinceData = new ArrayList<>();
        ArrayList<GeneratedSeaProvinceData> provinceDataSea = new ArrayList<>();

        HashMap<Integer, ArrayList<Integer>> color2color_neighbours = new HashMap<>();

        NameGen nameGen = new NameGen();

        for (int y = 0; y < provinces.getHeight(); y++){
            for (int x = 0; x < provinces.getWidth(); x++){
                
                increaseResource(provinces.getRGB(x, y), color2land);
                if (terrain.getRGB(x, y) == GenerateTerrain.MOUNTAIN){
                    increaseResource(provinces.getRGB(x, y), color2mountain);
                }
                else if (terrain.getRGB(x, y) == GenerateTerrain.ICE){
                    increaseResource(provinces.getRGB(x, y), color2ice);
                }
                else if (terrain.getRGB(x, y) == GenerateTerrain.DESERT){
                    increaseResource(provinces.getRGB(x, y), color2desert);
                }
                else if (terrain.getRGB(x, y) == GenerateTerrain.SAVANNA){
                    increaseResource(provinces.getRGB(x, y), color2savanna);
                }
                else if (terrain.getRGB(x, y) == GenerateTerrain.TROPICAL_RAINFOREST){
                    increaseResource(provinces.getRGB(x, y), color2tropicalRainforest);
                }
                else if (terrain.getRGB(x, y) == GenerateTerrain.TUNDRA){
                    increaseResource(provinces.getRGB(x, y), color2tundra);
                }
                else if (terrain.getRGB(x, y) == GenerateTerrain.TEMPERATE_RAINFOREST){
                    increaseResource(provinces.getRGB(x, y), color2temperateRainforest);
                }
                else if (terrain.getRGB(x, y) == GenerateTerrain.GRASSLAND){
                    increaseResource(provinces.getRGB(x, y), color2grassland);
                }
                else if (terrain.getRGB(x, y) == GenerateTerrain.SEASONAL_FOREST){
                    increaseResource(provinces.getRGB(x, y), color2seasonalForest);
                }
                else if (terrain.getRGB(x, y) == GenerateTerrain.BOREAL_FOREST){
                    increaseResource(provinces.getRGB(x, y), color2borealForest);
                }
                else if (terrain.getRGB(x, y) == GenerateTerrain.WOODLAND){
                    increaseResource(provinces.getRGB(x, y), color2woodland);
                }

                if (!color2avgcoord.containsKey(provinces.getRGB(x,y))){
                    color2avgcoord.put(provinces.getRGB(x,y), new ArrayList<>(List.of(new Vector2i(x, y))));
                }
                else {
                    color2avgcoord.get(provinces.getRGB(x, y)).add( new Vector2i(x, y));
                }

                if (!color2color_neighbours.containsKey(provinces.getRGB(x,y))){
                    color2color_neighbours.put(provinces.getRGB(x,y), new ArrayList<>());
                }
                try {
                    if (!color2color_neighbours.get(provinces.getRGB(x, y)).contains(provinces.getRGB(x - 1, y))) {
                        color2color_neighbours.get(provinces.getRGB(x, y)).add(provinces.getRGB(x - 1, y));
                    }
                }
                catch (Exception ignored){

                }

                try {
                    if (!color2color_neighbours.get(provinces.getRGB(x, y)).contains(provinces.getRGB(x - 1, y))) {
                        color2color_neighbours.get(provinces.getRGB(x, y)).add(provinces.getRGB(x + 1, y));
                    }
                }
                catch (Exception ignored){

                }

                try {
                    if (!color2color_neighbours.get(provinces.getRGB(x, y)).contains(provinces.getRGB(x - 1, y))) {
                        color2color_neighbours.get(provinces.getRGB(x, y)).add(provinces.getRGB(x, y - 1));
                    }
                }
                catch (Exception ignored){

                }

                try {
                    if (!color2color_neighbours.get(provinces.getRGB(x, y)).contains(provinces.getRGB(x - 1, y))) {
                        color2color_neighbours.get(provinces.getRGB(x, y)).add(provinces.getRGB(x, y + 1));
                    }
                }
                catch (Exception ignored){

                }

                try {
                    if (!color2color_neighbours.get(provinces.getRGB(x, y)).contains(provinces.getRGB(x - 1, y + 1))) {
                        color2color_neighbours.get(provinces.getRGB(x, y)).add(provinces.getRGB(x - 1, y + 1));
                    }
                }
                catch (Exception ignored){

                }

                try {
                    if (!color2color_neighbours.get(provinces.getRGB(x, y)).contains(provinces.getRGB(x + 1, y + 1))) {
                        color2color_neighbours.get(provinces.getRGB(x, y)).add(provinces.getRGB(x + 1, y + 1));
                    }
                }
                catch (Exception ignored){

                }

                try {
                    if (!color2color_neighbours.get(provinces.getRGB(x, y)).contains(provinces.getRGB(x - 1, y - 1))) {
                        color2color_neighbours.get(provinces.getRGB(x, y)).add(provinces.getRGB(x - 1, y - 1));
                    }
                }
                catch (Exception ignored){

                }

                try {
                    if (!color2color_neighbours.get(provinces.getRGB(x, y)).contains(provinces.getRGB(x + 1, y - 1))) {
                        color2color_neighbours.get(provinces.getRGB(x, y)).add(provinces.getRGB(x + 1, y - 1));
                    }
                }
                catch (Exception ignored){

                }
            }
        }

        HashMap<Integer, Vector2i> color2avgcoord_calculated = new HashMap<>();
        for (Map.Entry<Integer, List<Vector2i>> entry: color2avgcoord.entrySet()){
            int avg_x = 0;
            int avg_y = 0;

            for (Vector2i v: entry.getValue()){
                avg_x+=v.x;
                avg_y+=v.y;
            }

            color2avgcoord_calculated.put(entry.getKey(), new Vector2i(avg_x/entry.getValue().size(),
                    avg_y/entry.getValue().size()));
        }

        int country_gen = 0;
        int current_country_id = 1;

        // gen tribal node
        ArrayList<TribalNode> tribalNodes = new ArrayList<>();
        HashSet<String> tribalCultures = new HashSet<>();

        Random random = new Random();
        for (int i = 0; i < 80; i++){
            String cname = nameGen.genCultureName(nameGen.genRegionName());
            tribalNodes.add(new TribalNode(cname,
                    new Vector2i(random.nextInt(provinces.getWidth()),
                            random.nextInt(provinces.getHeight())),
                    random.nextInt(500)));
            tribalCultures.add(cname);
        }

        ArrayList<GeneratedProvinceData> countries = new ArrayList<>();
        HashMap<Integer, GeneratedProvinceData> color2prcomplex = new HashMap<>();
        HashMap<Integer, GeneratedSeaProvinceData> color2prcomplexSea = new HashMap<>();

        HashMap<String, Integer> culture_colors = new HashMap<>();

        // gen provinces

        for (Map.Entry<Integer, ProvinceSimpleData> entry: color2pr.entrySet()){
            if (entry.getValue().isSea()) {
                GeneratedSeaProvinceData province = new GeneratedSeaProvinceData(
                        entry.getValue().getName(),
                        entry.getValue().getProvinceID(),
                        color2avgcoord_calculated.get(entry.getKey()).x,
                        color2avgcoord_calculated.get(entry.getKey()).y,
                        new ArrayList<>()
                );
                provinceDataSea.add(province);

                color2prcomplexSea.put(entry.getKey(), province);
            }
            else {
                int wet_forest_land = color2borealForest.getOrDefault(entry.getKey(), 0) +
                        color2seasonalForest.getOrDefault(entry.getKey(), 0) +
                        color2tropicalRainforest.getOrDefault(entry.getKey(), 0) +
                        color2temperateRainforest.getOrDefault(entry.getKey(), 0);
                int dry_forest_land = color2woodland.getOrDefault(entry.getKey(), 0) +
                        color2savanna.getOrDefault(entry.getKey(), 0) +
                        color2grassland.getOrDefault(entry.getKey(), 0);
                int very_dry_land = color2ice.getOrDefault(entry.getKey(), 0) +
                        color2tundra.getOrDefault(entry.getKey(), 0) +
                        color2desert.getOrDefault(entry.getKey(), 0) +
                        color2mountain.getOrDefault(entry.getKey(), 0);

                int population = calculatePopulation(very_dry_land, dry_forest_land, wet_forest_land);

                // gen minority
                String tribal_culture = null;

                ArrayList<Double> power = new ArrayList<>();

                for (TribalNode node : tribalNodes) {
                    double dist = node.coord.distance(color2avgcoord_calculated.get(entry.getKey()));
                    power.add(node.power / dist);
                }

                tribal_culture = tribalNodes.get(IntStream.range(0, power.size()).boxed()
                        .max(Comparator.comparing(power::get)).orElse(-1)).culture;
                //

                ArrayList<GeneratedUrbanPointData> urbanPointData = new ArrayList<>();


                GeneratedProvinceData province = new GeneratedProvinceData(
                        entry.getValue().getName(),
                        entry.getValue().getProvinceID(),
                        (color2land.getOrDefault(entry.getKey(), 0)),
                        color2mountain.getOrDefault(entry.getKey(), 0),
                        color2ice.getOrDefault(entry.getKey(), 0) +
                                color2tundra.getOrDefault(entry.getKey(), 0),
                        color2seasonalForest.getOrDefault(entry.getKey(), 0) +
                                color2tropicalRainforest.getOrDefault(entry.getKey(), 0) +
                                color2temperateRainforest.getOrDefault(entry.getKey(), 0),
                        color2desert.getOrDefault(entry.getKey(), 0),
                        country_gen != 49 ? Resources.Resource.UNGENERATED.toID() :
                                Resources.generateResource(color2mountain.getOrDefault(entry.getKey(), 0),
                                        color2land.getOrDefault(entry.getKey(), 0), color2ice.getOrDefault(entry.getKey(), 0) +
                                                color2tundra.getOrDefault(entry.getKey(), 0),
                                        color2seasonalForest.getOrDefault(entry.getKey(), 0) +
                                                color2tropicalRainforest.getOrDefault(entry.getKey(), 0) +
                                                color2temperateRainforest.getOrDefault(entry.getKey(), 0),
                                        color2desert.getOrDefault(entry.getKey(), 0)).toID(),
                        //population,                   population
                        0.001f,
                        (int) Math.ceil(population / 10000f),
                        (int) Math.ceil(population / 10000f),
                        (int) Math.ceil(population / 10000f),
                        country_gen != 49 ? 0 : current_country_id,
                        country_gen != 49 ? tribal_culture : nameGen.genCultureName(entry.getValue().getName()),
                        country_gen != 49 ? null : tribal_culture,
                        color2avgcoord_calculated.get(entry.getKey()).x,
                        color2avgcoord_calculated.get(entry.getKey()).y,
                        new ArrayList<>(),
                        urbanPointData
                );

                if (population > 2500 && country_gen == 50) {
                    int gpop = random.nextInt(2500, population);
                    GeneratedUrbanPointData upoint = new GeneratedUrbanPointData(
                            gpop,
                            nameGen.genRegionName(),
                            gpop > 2500 ? UrbanPointType.CITY : UrbanPointType.VILLAGE_COMMUNITY,
                            province.minority_culture != null ? (random.nextInt(1, 10) > 7 ? province.culture : province.minority_culture) : province.culture
                    );
                    urbanPointData.add(upoint);
                    population -= gpop;
                }

                while (population > 1) {
                    if (population < 200) {
                        GeneratedUrbanPointData upoint = new GeneratedUrbanPointData(
                                population,
                                nameGen.genRegionName(),
                                UrbanPointType.VILLAGE_COMMUNITY,
                                province.culture
                        );
                        urbanPointData.add(upoint);
                        break;
                    }

                    int gpop = random.nextBoolean() ? random.nextInt(Math.min(2500, population)) : random.nextInt(Math.min(3500, population));
                    if (gpop == 0) {
                        continue;
                    }
                    GeneratedUrbanPointData upoint = new GeneratedUrbanPointData(
                            gpop,
                            nameGen.genRegionName(),
                            gpop > 2500 ? UrbanPointType.CITY : UrbanPointType.VILLAGE_COMMUNITY,
                            province.minority_culture != null ? (random.nextInt(1, 10) < 7 ? province.culture : province.minority_culture) : province.culture
                    );
                    urbanPointData.add(upoint);
                    population -= gpop;
                }

                provinceData.add(province);
                color2prcomplex.put(entry.getKey(), province);

                if (country_gen == 49) {
                    countries.add(province);
                    country_gen = 0;
                    current_country_id++;
                }
                country_gen++;

                if (!culture_colors.containsKey(province.culture) && province.culture != null) {
                    culture_colors.put(province.culture, new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256), 255).getRGB());
                }
                if (!culture_colors.containsKey(province.minority_culture) && province.minority_culture != null) {
                    culture_colors.put(province.minority_culture, new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256), 255).getRGB());
                }
            }
        }

        // set neighbouring provinces
        for (Map.Entry<Integer, ArrayList<Integer>> entry: color2color_neighbours.entrySet()){
            if (!color2prcomplexSea.containsKey(entry.getKey())) {
                GeneratedProvinceData origin = color2prcomplex.get(entry.getKey());
                for (Integer color : entry.getValue()) {
                    if (color2prcomplex.containsKey(color))
                        origin.neighbouring_provinces().add(color2prcomplex.get(color).id);
                    if (color2prcomplexSea.containsKey(color))
                        origin.neighbouring_provinces().add(color2prcomplexSea.get(color).id);
                }
            }
            else{
                GeneratedSeaProvinceData origin = color2prcomplexSea.get(entry.getKey());
                for (Integer color : entry.getValue()) {
                    if (color2prcomplex.containsKey(color))
                        origin.neighbouring_provinces().add(color2prcomplex.get(color).id);
                    if (color2prcomplexSea.containsKey(color))
                        origin.neighbouring_provinces().add(color2prcomplexSea.get(color).id);
                }
            }
        }

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new File("world/pr_definition.json"), provinceData);
        objectMapper.writeValue(new File("world/pr_definition_sea.json"), provinceDataSea);
        objectMapper.writeValue(new File("world/culture_colors.json"), culture_colors);

        ArrayList<GeneratedPersonData> personData = new ArrayList<>();

        int person_id = 1;

        //
        ArrayList<GeneratedCountryData> _countries = new ArrayList<>();

        for (GeneratedProvinceData country: countries){
            // leader
            GeneratedPersonData leader = new GeneratedPersonData(personNameGen.genName(),
                    personNameGen.genSurname(),
                    random.nextInt(6),
                    random.nextInt(6),
                    random.nextInt(6),
                    new String[]{},
                    country.id,
                    "politician",
                    country.culture,
                    person_id,
                    27+random.nextInt(23));
            personData.add(leader);
            person_id++;

            // governor
            GeneratedPersonData governor = new GeneratedPersonData(personNameGen.genName(),
                    personNameGen.genSurname(),
                    random.nextInt(6),
                    random.nextInt(6),
                    random.nextInt(6),
                    new String[]{},
                    country.id,
                    "politician",
                    country.culture,
                    person_id,
                    27+random.nextInt(23));
            personData.add(governor);
            person_id++;

            for (int i = 0; i < 12; i++){
                String position = const_positions[random.nextInt(const_positions.length)];
                String culture = random.nextInt(6) == 1 ? country.minority_culture: country.culture;

                if (position.equals("military_officer")){
                    personData.add(new GeneratedPersonData(personNameGen.genName(),
                            personNameGen.genSurname(),
                            random.nextInt(4),
                            random.nextInt(4),
                            random.nextInt(8),
                            new String[]{},
                            country.id,
                            "military_officer",
                            culture, person_id,
                            27+random.nextInt(23)));
                }
                else{
                    personData.add(new GeneratedPersonData(personNameGen.genName(),
                            personNameGen.genSurname(),
                            random.nextInt(5),
                            random.nextInt(5),
                            random.nextInt(5),
                            new String[]{},
                            country.id,
                            position,
                            culture, person_id,
                            27+random.nextInt(23)));
                }
                person_id++;
            }

            BufferedImage flag = FlagGen.genFlag();

            ImageIO.write(flag, "png", new File("world/flags/"+country.owner_id+".png"));

            _countries.add(new GeneratedCountryData(leader.id, governor.id, country.owner_id, country.pr_name + " State"));

        }

        objectMapper.writeValue(new File("world/person_definition.json"), personData);
        objectMapper.writeValue(new File("world/country_definitions.json"), _countries);
        objectMapper.writeValue(new File("world/tribal_cultures.json"), tribalCultures);

    }

    private void increaseResource(int rgb, HashMap<Integer, Integer> color2smth){
        if (!color2smth.containsKey(rgb)){
            color2smth.put(rgb, 0);
        }
        color2smth.put(rgb, color2smth.get(rgb)+1);
    }

    public int calculatePopulation(int very_dry_land,
                                   int dry_forest_land, int wet_forest_land){

        return (int) ((very_dry_land*5*0.1)+(dry_forest_land*5*0.3)+(wet_forest_land*5*0.5));
    }

}
