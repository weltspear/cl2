package net.colonize2.gen;

import java.util.Random;

public class NameGen {

    private static final Random random = new Random();

    public<T> T rnd_choice(T[] arn){
        return arn[random.nextInt(arn.length)];
    }

    private static String capitalize(String a){
        StringBuilder b = new StringBuilder(a);
        b.setCharAt(0, Character.toUpperCase(a.charAt(0)));
        return b.toString();
    }

    public String genRegionName(){
        String a = "ayuioe";
        String b1 = "rdgjkzxbnm";
        String b2 = "wtpsfhlcv";

        String[] c = {"st", "zh", "rz", "ch", "sch", "sha", "br", "or", "dd", "kl", "io", "qx", "gh", "ck",
        "schsch", "rzsz", "dz", "ea", "ai", "ay", "ee", "ey", "ie"};

        StringBuilder name = new StringBuilder();

        boolean option = false;

        for (int i = 0; i < random.nextInt(3)+5; i++){
            String b = rnd_choice(new String[]{b1, b2});

            boolean weird_thing = false;

            if (!option){
                name.append(b.charAt(random.nextInt(b.length())));

                option = true;

                if (random.nextInt(51) == 1 && name.length() > 5){
                    name.append("-").append(capitalize(genRegionName()));
                }
            }
            else{
                if (random.nextInt(26) == 1){
                    name.append("q");
                }
                else if (random.nextInt(26) == 1) {
                    name.append(rnd_choice(c));
                    weird_thing = true;
                }
                else {
                    name.append(a.charAt(random.nextInt(a.length())));
                }

                option = random.nextInt(61) == 1 && !weird_thing;
            }
        }

        return capitalize(name.toString());
    }

    public String genCultureName(String region_name){
        StringBuilder a = new StringBuilder(region_name);
        while (!a.isEmpty()){
            if (!"wtpsfhlcvrdgjkzxbnm".contains(String.valueOf(a.charAt(a.length() - 1)))){
                a.deleteCharAt(a.length()-1);
            }
            else {
                break;
            }
        }

        if (a.isEmpty()){
            return region_name + rnd_choice(new String[]{"ic", "ish"});
        }
        else {
            a.append(rnd_choice(new String[]{"ic", "ish"}));
            return a.toString();
        }
    }

}
