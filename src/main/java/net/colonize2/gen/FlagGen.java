package net.colonize2.gen;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.stgl.texture.TextureManager;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/***
 * All emblems and the color palette was extracted from flags from here:
 * <a href="https://github.com/emcrisostomo/flags">https://github.com/emcrisostomo/flags</a>
 */
public class FlagGen {

    private static Random random = new Random();

    private static BufferedImage shield = null;

    // that french looking thing
    private static BufferedImage symbol5 = null;
    private static BufferedImage symbol4 = null;
    private static BufferedImage symbol2 = null;
    private static BufferedImage symbol2_scaled = null;

    private static ArrayList<Integer> palette = null;

    private static Color palette_store = null;

    private record PrimitiveFlagData (ArrayList<Color> bg, BufferedImage flag){
    }

    public static BufferedImage genFlag(){
        int R = random.nextInt(100);
        palette_store = null;
        if (R < 50){
            PrimitiveFlagData tricolor = random.nextBoolean() ? genTricolor1() : genTricolor2();
            if (random.nextBoolean()){
                putAShieldOnFlag(tricolor.flag, genShield(tricolor.bg));
            }
            else if (random.nextBoolean()){
                genCircle(tricolor.flag);
            }
            return tricolor.flag;
        }
        else if (R < 75){
            PrimitiveFlagData flag = genSolidColor();
            if (random.nextBoolean()){
                genCross(flag.flag);
            }
            else if (random.nextBoolean()){
                putAShieldOnFlag(flag.flag, genShield(flag.bg));
            }
            else if (random.nextBoolean()){
                genCircle(flag.flag);
            }
            return flag.flag;
        }
        else{
            return genTricolor1().flag;//genFourCross();
        }
        //return genFourCross();
    }

    private static PrimitiveFlagData genTricolor1(){
        ArrayList<Color> bgs = new ArrayList<>();
        BufferedImage bufferedImage = new BufferedImage(128, 128, BufferedImage.TYPE_INT_ARGB);
        Graphics g = bufferedImage.createGraphics();
        for (int i = 0; i < 3; i++){
            Color bg = genColorStorePalette();
            bgs.add(bg);
            g.setColor(bg);
            g.fillRect(0, 128/3*i, 128, 128/3);
        }
        g.drawLine(0, 127, 128, 127);
        g.drawLine(0, 126, 128, 126);
        g.dispose();
        return new PrimitiveFlagData(bgs, bufferedImage);
    }

    private static PrimitiveFlagData genTricolor2(){
        ArrayList<Color> bgs = new ArrayList<>();
        BufferedImage bufferedImage = new BufferedImage(128, 128, BufferedImage.TYPE_INT_ARGB);
        Graphics g = bufferedImage.createGraphics();
        for (int i = 0; i < 3; i++){
            Color bg = genColorStorePalette();
            bgs.add(bg);
            g.setColor(bg);
            g.fillRect(128/3*i, 0, 128/3, 128);
        }
        g.drawLine(126, 0, 126, 128);
        g.drawLine(127, 0, 127, 128);
        g.dispose();
        return new PrimitiveFlagData(bgs, bufferedImage);
    }

    public static void genCross(BufferedImage flag){
        Graphics g = flag.createGraphics();
        int thickness = random.nextInt(10, 15);
        g.setColor(genColorStorePalette());
        g.fillRect((128-thickness)/2, 0, thickness, 128);
        g.fillRect(0, (128-thickness)/2, 128, thickness);
        g.dispose();
    }

    public static void genCircle(BufferedImage flag){
        Graphics g = flag.createGraphics();
        int r = random.nextInt(13, 20);
        g.setColor(genColorStorePalette());
        g.fillOval((128-2*r)/2, (128-2*r)/2, 2*r, 2*r);
        g.dispose();
    }

    public static BufferedImage genFourCross(){
        BufferedImage bufferedImage = new BufferedImage(128, 128, BufferedImage.TYPE_INT_ARGB);
        Graphics g = bufferedImage.createGraphics();
        g.setColor(genColorStorePalette());
        g.fillRect(0, 0, 128, 128);

        Image cross = new BufferedImage(128, 128, BufferedImage.TYPE_INT_ARGB);
        genCross((BufferedImage) cross);
        cross = cross.getScaledInstance(64, 64, Image.SCALE_FAST);

        g.drawImage(cross, 0, 0, null);
        g.drawImage(cross, 64, 0, null);
        g.drawImage(cross, 64, 64, null);
        g.drawImage(cross, 0, 64, null);

        g.dispose();
        return bufferedImage;
    }

    public static PrimitiveFlagData genSolidColor(){
        BufferedImage bufferedImage = new BufferedImage(128, 128, BufferedImage.TYPE_INT_ARGB);
        Graphics g = bufferedImage.createGraphics();
        Color bg = genColorStorePalette();
        g.setColor(genColorStorePalette());
        g.fillRect(0, 0, 128, 128);
        g.dispose();
        return new PrimitiveFlagData(new ArrayList<>(List.of(bg)), bufferedImage);
    }

    public static Color genColor(){
        if (palette == null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                palette = objectMapper.readValue(new File("data/flag_palette2.json"),
                        new TypeReference<>() {
                        });
            } catch (IOException ignored){

            }
        }

        Color c = new Color(palette.get(random.nextInt(palette.size())));
        /*while (c.getRed() > 155 && c.getGreen() > 155 && c.getAlpha() > 155){
            c = new Color(palette.get(random.nextInt(palette.size())));
        }*/

        return c;
    }

    private static void loadStuff(){
        if (shield == null){
            try {
                shield = TextureManager.loadBufferedImage("emblems/shield.png");

                BufferedImage argb = new BufferedImage(shield.getWidth(), shield.getHeight(), BufferedImage.TYPE_INT_ARGB);
                argb.getGraphics().drawImage(shield, 0,0,null);
                for (int x = 0; x < shield.getWidth(); x++){
                    for (int y = 0; y < shield.getHeight(); y++){
                        if (argb.getRGB(x, y) == Color.RED.getRGB())
                            argb.setRGB(x, y, new Color(0,0,0,0).getRGB());
                    }
                }
                shield = argb;

                symbol5 = TextureManager.loadBufferedImage("emblems/symbol_5.png");
                for (int x = 0; x < symbol5.getWidth(); x++) {
                    for (int y = 0; y < symbol5.getHeight(); y++) {
                        if (symbol5.getRGB(x, y) == new Color(0,36,136).getRGB()){
                            symbol5.setRGB(x, y, new Color(0,0,0,0).getRGB());
                        }
                    }
                }

                symbol4 = TextureManager.loadBufferedImage("emblems/symbol_4.png");
                for (int x = 0; x < symbol4.getWidth(); x++) {
                    for (int y = 0; y < symbol4.getHeight(); y++) {
                        if (symbol4.getRGB(x, y) == new Color(0,0,0).getRGB()){
                            symbol4.setRGB(x, y, new Color(0,0,0,0).getRGB());
                        }
                    }
                }

                symbol2 = TextureManager.loadBufferedImage("emblems/symbol_2.png");
                for (int x = 0; x < symbol2.getWidth(); x++) {
                    for (int y = 0; y < symbol2.getHeight(); y++) {
                        if (symbol2.getRGB(x, y) == new Color(255,199,38).getRGB()){
                            symbol2.setRGB(x, y, new Color(0,0,0,0).getRGB());
                        }
                    }
                }

                Image sc =symbol2.getScaledInstance(symbol2.getWidth()/2, symbol2.getHeight()/2, Image.SCALE_FAST);
                symbol2_scaled = new BufferedImage(symbol2.getWidth()/2, symbol2.getHeight()/2, BufferedImage.TYPE_INT_ARGB);
                symbol2_scaled.getGraphics().drawImage(sc, 0, 0, null);


            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static BufferedImage genTwoSquareShield(ArrayList<Color> bg){
        BufferedImage twoSquares = new BufferedImage(52, 64, BufferedImage.TYPE_INT_ARGB);
        Graphics g = twoSquares.createGraphics();
        g.setColor(genDiffColor(bg));
        g.fillRect(0,0,twoSquares.getWidth(),twoSquares.getHeight());
        g.setColor(genDiffColor(bg));
        g.fillRect(0, 0, twoSquares.getWidth()/2, twoSquares.getHeight()/2);
        g.fillRect(twoSquares.getWidth()/2, twoSquares.getHeight()/2, twoSquares.getWidth()/2, twoSquares.getHeight()/2);
        g.drawImage(shield, 0,0, null);
        g.dispose();
        return twoSquares;
    }

    private static BufferedImage genSolidColorShield(ArrayList<Color> bg){
        BufferedImage solid = new BufferedImage(52, 64, BufferedImage.TYPE_INT_ARGB);
        Graphics g = solid.createGraphics();
        g.setColor(genDiffColor(bg));
        g.fillRect(0,0,solid.getWidth(),solid.getHeight());
        g.drawImage(shield, 0,0, null);
        g.dispose();
        return solid;
    }

    private static void fillWithSymbolShield(BufferedImage img, BufferedImage symbol, ArrayList<Color> bg){
        BufferedImage symbol_delta= new BufferedImage(symbol.getWidth(), symbol.getHeight(), BufferedImage.TYPE_INT_ARGB);
        symbol_delta.getGraphics().drawImage(symbol, 0,0,null);
        Color c = genDiffColor(bg);
        for (int x = 0; x < symbol_delta.getWidth(); x++) {
            for (int y = 0; y < symbol_delta.getHeight(); y++) {
                if (symbol_delta.getRGB(x, y) == Color.RED.getRGB()){
                    symbol_delta.setRGB(x, y, c.getRGB());
                }
            }
        }

        img.getGraphics().drawImage(symbol_delta, (img.getWidth()-symbol_delta.getWidth())/2, (img.getHeight()-symbol_delta.getHeight())/2, null);
    }

    private static BufferedImage chooseSymbol(){
        int rnd = random.nextInt(0, 3);
        return switch (rnd) {
            case 0 -> symbol4;
            case 1 -> symbol5;
            default -> symbol2_scaled;
        };
    }

    // fix black color
    // make dif than bg
    public static BufferedImage genShield(ArrayList<Color> bg){
        loadStuff();
        //return shield;
        // two squares style

        boolean stype = random.nextBoolean();
        BufferedImage shield = stype ? genTwoSquareShield(bg) : genSolidColorShield(bg);
        if (!stype || random.nextBoolean())
            fillWithSymbolShield(shield, chooseSymbol(), bg);

        for (int x = 0; x < shield.getWidth(); x++) {
            for (int y = 0; y < shield.getHeight(); y++) {
                if (shield.getRGB(x, y) == new Color(0,0,0).getRGB()){
                    shield.setRGB(x, y, new Color(0,0,0,0).getRGB());
                }
            }
        }

        return shield;

    }

    public static void putAShieldOnFlag(BufferedImage flag, BufferedImage shield){
        int rnd = random.nextInt(1,3);

        if (rnd == 1) //pos1
            flag.getGraphics().drawImage(shield, (flag.getWidth()-shield.getWidth())/2,(flag.getHeight()-shield.getHeight())/2, null);
        else if (rnd == 2)
            flag.getGraphics().drawImage(shield, flag.getWidth()-shield.getWidth()-5,(flag.getHeight()-shield.getHeight())/3-20, null);
    }

    public static Color genDiffColor(Color from){
        boolean notDiff = true;
        Color c = genColor();
        while (notDiff){
            c = genColor();
            if (isDiff(c, from)){
                notDiff = false;
            }
        }
        return c;
    }

    // stupid but ok
    public static Color genDiffColor(ArrayList<Color> colors){
        Color d = null;
        for (Color c: colors){
            if (d != null)
            {
                if (!isDiff(d, c)){
                    d = genDiffColor(c);
                }
            }
            else d = genDiffColor(c);
        }
        return d;
    }

    public static boolean isDiff(Color c, Color from){
        return Math.abs(c.getRed()-from.getRed()) > 130 || Math.abs(c.getBlue()-from.getBlue()) > 130 || Math.abs(c.getGreen()-from.getGreen()) > 130;
    }

    public static Color genColorStorePalette(){
        if (palette_store != null) {
            Color c = genDiffColor(palette_store);
            palette_store = c;
            return c;
        }
        else{
            palette_store = genColor();
            return palette_store;
        }
    }
}
