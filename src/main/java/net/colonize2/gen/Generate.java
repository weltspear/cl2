package net.colonize2.gen;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class Generate {
    public static void generate() throws IOException {
        GenerateTerrain terrainGen = new GenerateTerrain();
        Random random = new Random();
        long seed = random.nextLong();
        BufferedImage terrain=terrainGen.generateMap(2500, 2500, seed);

        ProvinceGen provinceGen = new ProvinceGen(1275, 2500, (x, y)
                -> !(terrainGen.getHeightmap()[x][y] > 0.56f));
        BufferedImage provinces =provinceGen.getProvinceImage();

        ImageIO.write(terrain, "bmp", new File("world/terrain.bmp"));
        ImageIO.write(provinces, "bmp", new File("world/provinces.bmp"));
        ImageIO.write(GenerateTerrain.generateNormal(terrainGen.getHeightmapImg()), "bmp", new File("world/normals.bmp"));
        ImageIO.write(terrainGen.getHeightmapImg(), "png", new File("world/heightmap.png"));

        provinceGen.writeDefinitions("world/definitions.csv");

        DataGen dataGen = new DataGen(provinceGen.getProvinceImage(),
                provinceGen.getColorToProvince(),
                terrain);
    }
}
