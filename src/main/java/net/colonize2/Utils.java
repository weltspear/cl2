package net.colonize2;

import org.joml.Math;
import org.joml.Vector4f;

import java.awt.*;

public class Utils {
    public static Color v2c(Vector4f v){
        return new Color((int)(255*v.x), (int)(255*v.y), (int)(255*v.z), 255);
    }

    public static Color clamp_v2c(Vector4f v){
        return v2c(new Vector4f(Math.clamp(0, 1, v.x),
                Math.clamp(0, 1, v.y),
                Math.clamp(0, 1, v.z),
                Math.clamp(0, 1, v.w)));
    }
}
