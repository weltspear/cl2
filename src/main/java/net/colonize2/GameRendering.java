package net.colonize2;

import net.colonize2.buffers.*;
import net.colonize2.game.Game;
import net.colonize2.game.HumanPlayer;
import net.colonize2.game.Province;
import net.colonize2.game.SeaProvince;
import net.colonize2.game.army.Army;
import net.colonize2.game.diplomacy.DiplomaticRequest;
import net.colonize2.game.diplomacy.DummyDiploRequest;
import net.colonize2.load.ExtFileLoader;
import net.colonize2.render.ProvinceSimpleData;
import net.colonize2.render.ProvincesSimpleData;
import net.stgl.MainFrame;
import net.stgl.STGLGraphics;
import net.stgl.debug.ErrorCheck;
import net.stgl.event.KeyPress;
import net.stgl.event.KeyboardListener;
import net.stgl.event.MouseListener;
import net.stgl.event.MousePress;
import net.stgl.font.FontManager;
import net.stgl.font.STGLFont;
import net.stgl.shader.Shader;
import net.stgl.shader.ShaderManager;
import net.stgl.shader.ShaderProgram;
import net.stgl.state.GlobalTextureSlotManager;
import net.stgl.texture.DynamicTextureAtlas;
import net.stgl.texture.Texture;
import net.stgl.texture.TextureManager;
import org.joml.Vector4f;
import org.joml.Vector4i;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBShaderStorageBufferObject;
import org.lwjgl.opengl.GL32;
import org.lwjgl.system.MemoryUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import static org.joml.Math.clamp;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

public class GameRendering {

    private final Game g;
    private final HumanPlayer player;

    private boolean isLegacyCard = false;

    private final int width = 832;
    private final int height = 576;

    private Army selectedArmy = null;

    private boolean noTerrain = false;

    private int noTerrain_uniform;
    private int lightPos_uniform;

    public GameRendering(Game g, HumanPlayer player){
        this.g = g;
        this.player = player;
    }

    public ArrayList<String> getSupportedExtensions(){
        int[] n = new int[1];
        glGetIntegerv(GL_NUM_EXTENSIONS, n);
        ArrayList<String> ext = new ArrayList<>();
        for (int i = 0; i < n[0]; i++){
            ext.add(glGetStringi(GL_EXTENSIONS, i));
        }
        return ext;
    }

    private int cam_x = 0;
    private int cam_y = 0;

    private final Color[] pressed_hover = new Color[3];
    private UIManager uiManager;

    private IDiscoveredProvinceManager discoveredProvinces = null;
    
    //private int 

    public void run(){

        ProvincesSimpleData prs = new ProvincesSimpleData();

        //DataLoader dataLoader = new DataLoader(prs);
        //Game g = new Game(dataLoader.getCountries(),dataLoader.getProvinces(), dataLoader.getReprColorsProvince());

        //ErrorCheck.enableDebugging();

        MainFrame frame = new MainFrame(width, height, "MapGame", new MainFrame.FrameConfig());

        if (!getSupportedExtensions().contains("GL_ARB_shader_storage_buffer_object")){
            isLegacyCard = true;
        }

        FontManager fontManager = new FontManager();
        STGLFont monogram = fontManager.createFont("font/monogram.ttf", 20, false, false);

        STGLFont monogram12 = fontManager.createFont("font/monogram.ttf", 12, false, false);

        DynamicTextureAtlas flagAtlas = new DynamicTextureAtlas(50, 128, 128,
                new Texture.TextureOptions().setMagFiltering(Texture.Filtering.NEAREST).setMinFiltering(Texture.Filtering.NEAREST), -0);

        uiManager = new UIManager(frame, player, fontManager, monogram, monogram12, flagAtlas);

        BufferedImage terrain = null;
        BufferedImage normals = null;
        BufferedImage heightmap = null;

        try{
            terrain = ExtFileLoader.loadExternalImage("world/terrain.bmp");
            normals = ExtFileLoader.loadExternalImage("world/normals.bmp");
            heightmap = ExtFileLoader.loadExternalImage("world/heightmap.png");
        }
        catch (Exception ignored){

        }

        frame.show();

        //componentManager.enableDirectDrawing();

        //
        ErrorCheck.checkErrorGL();

        STGLGraphics graphics = frame.getGraphics();

        float[] pr_colors = new float[(prs.getProvinces().size())*3];

        HashMap<Integer, Integer> colors = new HashMap<>(); // for rendering
        HashMap<Integer, ProvinceSimpleData> pr2color = new HashMap<>(); // for data

        // assign colors
        for (ProvinceSimpleData pr : prs.getProvinces()){
            if (pr.isSea()){
                colors.put(pr.getRgb(), pr.getProvinceID());
                continue;
            }

            int i;
            if (pr.getProvinceID() == 1){
                i = 0;
            }
            else i = ((pr.getProvinceID()-1)*3);

            if (g.getProvince(pr.getRgb()).getOwner() != null){
                Color color = new Color(pr.getRgb());
                pr_colors[i] = color.getRed()/255f;
                pr_colors[i+1] = color.getGreen()/255f;
                pr_colors[i+2] = color.getBlue()/255f;
            }

            else{
                pr_colors[i] = 1f;
                pr_colors[i+1] = 1f;
                pr_colors[i+2] = 1f;
            }

            colors.put(pr.getRgb(), pr.getProvinceID());
            pr2color.put(pr.getRgb(), pr);
        }

        // (x, y) -> id
        /*int[] pr_ids_pixels = new int[prs.getProvincesImage().getHeight()*prs.getProvincesImage().getWidth()];
        for (int y = 0; y < prs.getProvincesImage().getHeight(); y++)
            for (int x = 0; x < prs.getProvincesImage().getWidth(); x++){
                //System.out.println(prs.getProvincesImage().getRGB(x, y));
                pr_ids_pixels[y*prs.getProvincesImage().getWidth() + x] = colors.get(prs.getProvincesImage().getRGB(x, y));
            }*/

        int[] pixels = new int[prs.getProvincesImage().getWidth() * prs.getProvincesImage().getHeight()];
        prs.getProvincesImage().getRGB(0, 0, prs.getProvincesImage().getWidth(), prs.getProvincesImage().getHeight(), pixels, 0, prs.getProvincesImage().getWidth());
        IntBuffer buffer = BufferUtils.createIntBuffer(prs.getProvincesImage().getWidth()*prs.getProvincesImage().getHeight());

        for(int h = 0; h < prs.getProvincesImage().getHeight(); h++) {
            for(int w = 0; w < prs.getProvincesImage().getWidth(); w++) {
                int pixel = colors.get(pixels[h * prs.getProvincesImage().getWidth() + w]);

                buffer.put(pixel);
            }
        }

        buffer.flip();

        int slot_reserved = GlobalTextureSlotManager.reserveSlot();
        Texture t = new Texture(prs.getProvincesImage());
        glActiveTexture(GL_TEXTURE0 + slot_reserved);
        glBindTexture(GL_TEXTURE_2D, t.getId());

        int slot_reserved2 = GlobalTextureSlotManager.reserveSlot();
        Texture t2 = new Texture(terrain, new Texture.TextureOptions().setBorderColor(0, 0, 0,1));
        glActiveTexture(GL_TEXTURE0 + slot_reserved2);
        glBindTexture(GL_TEXTURE_2D, t2.getId());

        //ErrorCheck.enableDebugging();

        int slot_reserved3 = GlobalTextureSlotManager.reserveSlot();
        Texture t3 = new Texture(prs.getProvincesImage().getWidth(), prs.getProvincesImage().getHeight(), MemoryUtil.memByteBuffer(buffer));

        int slot_reserved4 = GlobalTextureSlotManager.reserveSlot();
        Texture t4 = new Texture(normals);
        glActiveTexture(GL_TEXTURE0 + slot_reserved4);
        glBindTexture(GL_TEXTURE_2D, t4.getId());

        int slot_reserved5 = GlobalTextureSlotManager.reserveSlot();
        Texture t5 = new Texture(heightmap);
        glActiveTexture(GL_TEXTURE0 + slot_reserved5);
        glBindTexture(GL_TEXTURE_2D, t5.getId());

        //
        glActiveTexture(GL_TEXTURE0 + slot_reserved3);
        glBindTexture(GL_TEXTURE_2D, t3.getId());

        int[] pr_discovered = new int[prs.getProvinces().size()];

        for (Province p: player.getCountry().getProvinces()){
            pr_discovered[p.getId()-1] = 1;
            for (Province neighbour: p.getNeighbours()){
                pr_discovered[neighbour.getId()-1] = 1;

                for (Province neighbour2: neighbour.getNeighbours()){
                    pr_discovered[neighbour2.getId()-1] = 1;
                }
            }
        }

        //

        IProvinceColorManager prColorsB;

        //
        ErrorCheck.checkErrorGL();

        ShaderProgram shaderProgram = new ShaderProgram();

        Shader vshader = ShaderManager.loadShader("/shaders/vmapdraw.vert", GL_VERTEX_SHADER);
        Shader fshader;

        if (!isLegacyCard) {
            fshader = ShaderManager.loadShader("/shaders/fmapdraw.frag", GL_FRAGMENT_SHADER, "#version 430 core" +
                    "\n#define PR_AMOUNT " + prs.getProvinces().size() +
                    "\n#define TEX_WIDTH " + prs.getProvincesImage().getWidth() +
                    "\n#define TEX_HEIGHT " + prs.getProvincesImage().getHeight() +
                    "\n");
        }
        else{
            //System.out.println(GL32.GL_MAX_UNIFORM_BLOCK_SIZE);
            fshader = ShaderManager.loadShader("/shaders/fmapdraw.frag", GL_FRAGMENT_SHADER, "#version 150 core" +
                    "\n#define PR_AMOUNT " + prs.getProvinces().size() +
                    "\n#define TEX_WIDTH " + prs.getProvincesImage().getWidth() +
                    "\n#define TEX_HEIGHT " + prs.getProvincesImage().getHeight() +
                    "\n#define LEGACY" +
                    "\n#define PRDATA_UBO_SIZE " + pr_colors.length +
                    "\n#define PRDATA_UBO_SIZE_3 " + pr_discovered.length);
        }
        shaderProgram.attachShader(vshader);
        shaderProgram.attachShader(fshader);
        shaderProgram.link();
        shaderProgram.useThis();
        ErrorCheck.checkErrorGL();
        //

        if (!isLegacyCard) {

            // setup province data ssbo

            int province_data_ssbo = glGenBuffers();
            glBindBuffer(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, province_data_ssbo);
            glBufferData(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, (prs.getProvinces().size()*(12L)), GL_DYNAMIC_COPY);
            glBindBufferBase(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, 0, province_data_ssbo);

            // discover buffer
            int province_data_ssbo3 = glGenBuffers();
            glBindBuffer(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, province_data_ssbo3);
            glBufferData(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, (prs.getProvinces().size()*(4L)), GL_DYNAMIC_COPY);
            glBindBufferBase(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, 2, province_data_ssbo3);

            discoveredProvinces = new BufferDiscoveredProvinces(province_data_ssbo3, pr_discovered);
            //discoveredProvinces.discoverAll(); //fixme: remove
            prColorsB = new BufferProvinceColors(province_data_ssbo);
        }
        else{
            int province_data_ubo = glGenBuffers();
            glBindBuffer(GL32.GL_UNIFORM_BUFFER, province_data_ubo);
            glBufferData(GL32.GL_UNIFORM_BUFFER, (prs.getProvinces().size()*(12L)), GL_DYNAMIC_COPY);
            int province_data_ubo_idx = GL32.glGetUniformBlockIndex(shaderProgram.getId(), "province_data_buffer");
            GL32.glUniformBlockBinding(shaderProgram.getId(), province_data_ubo_idx, 1);
            glBindBufferBase(GL32.GL_UNIFORM_BUFFER, 1, province_data_ubo);

            // discover buffer
            int province_data_ubo3 = glGenBuffers();
            glBindBuffer(GL32.GL_UNIFORM_BUFFER,province_data_ubo3);
            glBufferData(GL32.GL_UNIFORM_BUFFER, (prs.getProvinces().size()*(4L)), GL_DYNAMIC_COPY);
            int province_data_ubo_idx_2 = GL32.glGetUniformBlockIndex(shaderProgram.getId(), "province_data_buffer3");
            GL32.glUniformBlockBinding(shaderProgram.getId(), province_data_ubo_idx_2, 2);
            GL32.glBindBufferBase(GL32.GL_UNIFORM_BUFFER, 2, province_data_ubo3);

            // data

            discoveredProvinces = new UBODiscoveredProvinces(province_data_ubo3, pr_discovered);
            prColorsB = new UBOProvinceColors(province_data_ubo);
        }

        prColorsB.writeFloats(pr_colors);
        discoveredProvinces.writeInts(pr_discovered);

        ErrorCheck.checkErrorGL();

        int vbo = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        // vao
        int vao = glGenVertexArrays();
        glBindVertexArray(vao);
        int pos = glGetAttribLocation(shaderProgram.getId(), "pos");
        glVertexAttribPointer(pos, 2, GL_FLOAT, false, 4 * 4, 0);
        glEnableVertexAttribArray(pos);

        int texpos = glGetAttribLocation(shaderProgram.getId(), "texpos");
        glVertexAttribPointer(texpos, 2, GL_FLOAT, false, 4 * 4, 2 * 4);
        glEnableVertexAttribArray(texpos);
        // sampler2d

        int sampler2d_tex_uniform = glGetUniformLocation(shaderProgram.getId(), "tex");
        GlobalTextureSlotManager.useCorrectTextureUniform(sampler2d_tex_uniform, slot_reserved);

        int sampler2d_terrain_uniform = glGetUniformLocation(shaderProgram.getId(), "terrain");
        GlobalTextureSlotManager.useCorrectTextureUniform(sampler2d_terrain_uniform, slot_reserved2);

        int sampler2d_tex2_uniform = glGetUniformLocation(shaderProgram.getId(), "tex2");
        GlobalTextureSlotManager.useCorrectTextureUniform(sampler2d_tex2_uniform, slot_reserved3);

        int sampler2d_normals_uniform = glGetUniformLocation(shaderProgram.getId(), "normals");
        GlobalTextureSlotManager.useCorrectTextureUniform(sampler2d_normals_uniform, slot_reserved4);

        int sampler2d_heightmap_uniform = glGetUniformLocation(shaderProgram.getId(), "heightmap");
        GlobalTextureSlotManager.useCorrectTextureUniform(sampler2d_heightmap_uniform, slot_reserved5);

        int border_pressed_uniform = glGetUniformLocation(shaderProgram.getId(), "border_pressed");

        int hover_uniform = glGetUniformLocation(shaderProgram.getId(), "hovered");

        lightPos_uniform = glGetUniformLocation(shaderProgram.getId(), "lightPos");

        noTerrain_uniform = glGetUniformLocation(shaderProgram.getId(), "noTerrain");

        //ArrayList<Vector4i> rects = new ArrayList<>();

        // set xpos, ypos to province center
        cam_x = player.getCountry().getProvinces().get(0).getAvgX()-832/2;
        cam_y = player.getCountry().getProvinces().get(0).getAvgY()-576/2;

        //

        //ARBShaderStorageBufferObject.glShaderStorageBlockBinding();

        float tex_width = prs.getProvincesImage().getWidth();
        float tex_height = prs.getProvincesImage().getHeight();

        g.setProvinceColors(prColorsB);
        player.setDiscoveredProvinces(discoveredProvinces);

        frame.addMouseListener(new MouseListener() {
            private boolean moveCam = false;
            private int lmouse_x = -1;
            private int lmouse_y = -1;

            @Override
            public void mouseMoved(double x, double y) {
                for (Vector4i rect: uiManager.getAreasCoveredByUIElements()){
                    if ((rect.x<x && rect.y<y && x<rect.x+rect.z && y<rect.y+rect.w)){
                        return;
                    }
                }

                if (cam_x+x < prs.getProvincesImage().getWidth() && cam_x+x >= 0){
                    if (cam_y+y < prs.getProvincesImage().getHeight() && cam_y+y >= 0){
                        pressed_hover[2] = new Color(prs.getProvincesImage().getRGB((int) (cam_x+x), (int) (cam_y+y)));
                    }
                }

                if (moveCam){
                    int prv_mouse_x = lmouse_x;
                    int prv_mouse_y = lmouse_y;

                    lmouse_x = (int) x;
                    lmouse_y = (int) y;

                    int d_x = lmouse_x-prv_mouse_x;
                    int d_y = lmouse_y-prv_mouse_y;

                    cam_x -= d_x;
                    cam_y -= d_y;
                }
            }

            @Override
            public void mousePressed(MousePress button, int glfw_mods, double x, double y) {
                for (Vector4i rect: uiManager.getAreasCoveredByUIElements()){
                    if ((rect.x<x && rect.y<y && x<rect.x+rect.z && y<rect.y+rect.w)){
                        return;
                    }
                }

                if (MousePress.MOUSE_BUTTON_1 == button){
                    if (cam_x+x < prs.getProvincesImage().getWidth() && cam_x+x >= 0){
                        if (cam_y+y < prs.getProvincesImage().getHeight() && cam_y+y >= 0){

                            pressed_hover[0] = new Color(prs.getProvincesImage().getRGB((int) (cam_x + x), (int) (cam_y + y)));

                            boolean hasArmyBeenSelected = false;
                            Province pr = g.getProvince(pressed_hover[0].getRGB());
                            if (pr != null) {
                                uiManager.setSelectedProvince(pr);

                                if (discoveredProvinces.isDiscovered(pr.getId()) == 1)
                                    uiManager.spawnProvinceInformationWindow(player.getCountry(), pr, player, g);

                            }
                            else{
                                SeaProvince spr = g.getProvinceSea(pressed_hover[0].getRGB());
                            }
                            if (!hasArmyBeenSelected) {
                                selectedArmy = null;
                                pressed_hover[0] = new Color(prs.getProvincesImage().getRGB((int) (cam_x + x), (int) (cam_y + y)));
                            } else {
                                pressed_hover[0] = null;
                            }
                        }
                    }
                }

                if (MousePress.MOUSE_BUTTON_2 == button){
                    if (cam_x+x < prs.getProvincesImage().getWidth() && cam_x+x >= 0){
                        if (cam_y+y < prs.getProvincesImage().getHeight() && cam_y+y >= 0){
                            pressed_hover[1] = new Color(prs.getProvincesImage().getRGB((int) (cam_x+x), (int) (cam_y+y)));

                            Province pr = g.getProvince(pressed_hover[1].getRGB());

                            if (pr != null)
                                if (pr.getOwner() != null)
                                    uiManager.spawnDiplomacyMenu(player.getCountry(), pr, player, g);
                        }
                    }
                }

                if (MousePress.MOUSE_BUTTON_3 == button){
                    moveCam = true;
                    lmouse_x = (int) x;
                    lmouse_y = (int) y;
                }
            }

            @Override
            public void mouseReleased(MousePress mouseButton, int glfw_mods, double x, double y) {
                if (MousePress.MOUSE_BUTTON_3 == mouseButton) {
                    moveCam = false;
                }
            }

            @Override
            public void mouseWheelMoved(double xoffset, double yoffset) {

            }
        });
        frame.addKeyboardListener(new KeyboardListener() {
            @Override
            public void keyPressed(KeyPress key, int glfw_scancode, int glfw_modifiers) {
                if (KeyPress.KEY_SPACE == key){
                    if (player.getEventManager().getEvents().isEmpty()){
                        if (!uiManager.areEventsProcessed()) {
                            player.endTurn();
                            /*for (DiplomaticRequest rq: pendingRequests){
                                rq.decline(g);
                            }*/
                        }
                    }
                }
                if (KeyPress.KEY_F1 == key){
                    noTerrain = !noTerrain;
                    shaderProgram.useThis();
                    glUniform1i(noTerrain_uniform, noTerrain ? 1 : 0);
                }
            }

            @Override
            public void keyReleased(KeyPress key, int glfw_scancode, int glfw_modifiers) {

            }

            @Override
            public void keyPressedRepeat(KeyPress key, int glfw_scancode, int glfw_modifiers) {

            }

            @Override
            public void charTyped(int codepoint) {

            }
        });


        // handle diplorequests

        ArrayList<DiplomaticRequest> pendingRequests = new ArrayList<>();
        pendingRequests.add(new DummyDiploRequest());

        Texture diploRequestTexture = null;
        try {
            diploRequestTexture = new Texture(TextureManager.loadBufferedImage("diplo_request.png"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //dbg
        //for (Province pr: g.getProvinces())
            //discoveredProvinces.makeDiscovered(pr.getId());
        //RivalSelectionMenu rivalMenu = null;

        while (!frame.shouldClose()) {
            // dbg
            //player.endTurn();
            //if (curPopup != null)
            //curPopup.dbgDone();

            //

            graphics.clear();

            graphics.useCorrectRenderingTarget();
            shaderProgram.useThis();

            if (pressed_hover[0] != null)
                glUniform4f(border_pressed_uniform, pressed_hover[0].getRed()/255f, pressed_hover[0].getGreen()/255f,pressed_hover[0].getBlue()/255f, 1);

            if (pressed_hover[2] != null)
                glUniform4f(hover_uniform, pressed_hover[2].getRed()/255f, pressed_hover[2].getGreen()/255f,pressed_hover[2].getBlue()/255f, 1);

            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            glBindVertexArray(vao);
            // 5632 x 2048

            float[] vertices = new float[]{
                    graphics.toHardwareX(0), graphics.toHardwareY(0), cam_x /tex_width, cam_y /tex_height,
                    graphics.toHardwareX(832), graphics.toHardwareY(0), (832+ cam_x)/tex_width, cam_y /tex_height,
                    graphics.toHardwareX(0), graphics.toHardwareY(576), cam_x /tex_width, (576+ cam_y)/tex_height,
                    graphics.toHardwareX(832), graphics.toHardwareY(576), (832+ cam_x)/tex_width, (576+ cam_y)/tex_height,
            };

            glBufferData(GL32.GL_ARRAY_BUFFER, vertices
                    , GL32.GL_STREAM_DRAW);

            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

            ErrorCheck.checkErrorGL();

            //
            if (isLegacyCard)
                graphics.renderText(monogram, 832-monogram.getFontMetrics().getStringWidth("Legacy Graphics Mode"), 576-5, "Legacy Graphics Mode", new Vector4f(1,0,0,1));
            //

            uiManager.update();
            //componentManager.paint();
            frame.update();
            // this is not a good idea
            g.update();
        }

        frame.dispose();
        ErrorCheck.checkErrorGL();
        t.dispose();
        t2.dispose();
        t3.dispose();
        t4.dispose();
        shaderProgram.dispose();


        // fixme buffer disposal

        //glDeleteBuffers(province_data_ssbo);
        //glDeleteBuffers(province_data_ssbo2);
    }
}
