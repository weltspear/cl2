package net.colonize2;

import net.colonize2.game.*;
import net.colonize2.gen.Generate;
import net.colonize2.render.ProvincesSimpleData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Main {

    private static final Random random = new Random();
    public static void main(String[] args) throws IOException {
        /*try {
            Generate.generate();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }*/

        ProvincesSimpleData prs = new ProvincesSimpleData();

        DataLoader dataLoader = new DataLoader(prs);

        HumanPlayer player = null;
        ArrayList<IPlayer> players = new ArrayList<>();
        Country player_country = dataLoader.getCountries().get(random.nextInt(dataLoader.getCountries().size()));

        for (Country country : dataLoader.getCountries()){
            if (country == player_country){
                player = new HumanPlayer(country);
                players.add(player);
            }
            else{
                players.add(new BotPlayer(country));
            }
        }

        /*Game g = new Game(dataLoader.getCountries(),dataLoader.getProvinces(),
                dataLoader.getSeaProvinces(), dataLoader.getReprColorsProvince(), dataLoader.getReprColorsProvinceSea(), players);*/

        Game g = new Game(dataLoader.getCountries(), dataLoader.getProvinces(), dataLoader.getSeaProvinces(), dataLoader.getReprColorsProvince(), dataLoader.getReprColorsProvinceSea(), players);

        GameRendering gameRendering = new GameRendering(g, player);
        gameRendering.run();

    }

}