package net.colonize2.game;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.colonize2.game.army.Army;
import net.colonize2.game.army.InfantryRegiment;
import net.colonize2.game.history.ProvinceClaim;
import net.colonize2.gen.DataGen;
import net.colonize2.load.ExtFileLoader;
import net.colonize2.render.ProvinceSimpleData;
import net.colonize2.render.ProvincesSimpleData;
import net.stgl.Utils;
import org.joml.Vector4f;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.*;

public class DataLoader {

    private final ArrayList<Person> people;
    private final ArrayList<Province> provinces;
    private final ArrayList<SeaProvince> seaProvinces;
    private final ArrayList<Country> countries;

    private final HashMap<Integer, SeaProvince> repr_color_province_sea = new HashMap<>();
    private final HashMap<Integer, Province> repr_color_province = new HashMap<>();

    public DataLoader(ProvincesSimpleData provincesSimpleData) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ArrayList<DataGen.GeneratedProvinceData> prs
                = objectMapper.readValue(new File("world/pr_definition.json"),
                new TypeReference<>() {
                });

        ArrayList<DataGen.GeneratedSeaProvinceData> prs_sea
                = objectMapper.readValue(new File("world/pr_definition_sea.json"),
                new TypeReference<>() {
                });

        ArrayList<DataGen.GeneratedPersonData> person_defs
                = objectMapper.readValue(new File("world/person_definition.json"),
                new TypeReference<>() {
                });

        ArrayList<DataGen.GeneratedCountryData> country_defs
                = objectMapper.readValue(new File("world/country_definitions.json"),
                new TypeReference<>() {
                });

        HashMap<String, Integer> culture_colors
                = objectMapper.readValue(new File("world/culture_colors.json"),
                new TypeReference<>() {});

        people = new ArrayList<>();
        provinces = new ArrayList<>();
        countries = new ArrayList<>();
        seaProvinces = new ArrayList<>();

        HashMap<Integer, Province> country_id2province = new HashMap<>();
        HashMap<Person, Integer> person2location_id = new HashMap<>();
        HashMap<Integer, Person> id2person = new HashMap<>();

        HashMap<Integer, Province> id2province = new HashMap<>();
        HashMap<Integer, SeaProvince> id2seaProvince = new HashMap<>();

        HashMap<String, Culture> sc = new HashMap<>();

        for (DataGen.GeneratedPersonData generatedPersonData: person_defs){
            Person p = new Person(generatedPersonData.name(),
                    generatedPersonData.surname(),
                    generatedPersonData.age(),
                    generatedPersonData.admin_skill(),
                    generatedPersonData.production_skill(),
                    generatedPersonData.mil_skill(),
                    generatedPersonData.traits(),
                    generatedPersonData.position(),
                    getCulture(sc,generatedPersonData.culture(), true),
                    generatedPersonData.id());

            people.add(p);

            id2person.put(generatedPersonData.id(), p);

            person2location_id.put(p, generatedPersonData.location());
        }

        for (DataGen.GeneratedProvinceData generatedProvinceData: prs){
            ArrayList<UrbanPoint> upoints = new ArrayList<>();
            for (DataGen.GeneratedUrbanPointData generatedUrbanPointData: generatedProvinceData.urban_points()){
                upoints.add(new UrbanPoint(generatedUrbanPointData.population(),
                        generatedUrbanPointData.name(),
                        generatedUrbanPointData.urbanPointType(),
                        getCulture(sc,generatedUrbanPointData.culture(), true)));
            }

            Province province = new Province(generatedProvinceData.pr_name(),
                    generatedProvinceData.id(),
                    generatedProvinceData.land_amount(),
                    generatedProvinceData.mountain_amount(),
                    generatedProvinceData.cold_land_amount(),
                    generatedProvinceData.tropical_land_amount(),
                    generatedProvinceData.desert_land(),
                    Resources.fromID(generatedProvinceData.resource_id()),
                    upoints,
                    generatedProvinceData.population_growth(),
                    generatedProvinceData.production_level(),
                    generatedProvinceData.admin_level(),
                    generatedProvinceData.manpower_level(),
                    null, getCulture(sc,generatedProvinceData.culture(), true), getCulture(sc,generatedProvinceData.minority_culture(), true), null,
                    generatedProvinceData.avg_x(), generatedProvinceData.avg_y());
            provinces.add(province);

            for (ProvinceSimpleData provinceSimpleData: provincesSimpleData.getProvinces()){
                if (provinceSimpleData.getProvinceID() == generatedProvinceData.id()){
                    province.setReprColor(provinceSimpleData.getRgb());
                    break;
                }
            }

            country_id2province.put(generatedProvinceData.owner_id(), province);

            repr_color_province.put(province.getReprColor(), province);

            id2province.put(province.getId(), province);
        }

        for (DataGen.GeneratedSeaProvinceData generatedSeaProvinceData: prs_sea){
            SeaProvince province = new SeaProvince(generatedSeaProvinceData.pr_name(),
                    generatedSeaProvinceData.id(),
                    generatedSeaProvinceData.avg_x(),
                    generatedSeaProvinceData.avg_y());
            seaProvinces.add(province);

            for (ProvinceSimpleData provinceSimpleData: provincesSimpleData.getProvinces()){
                if (provinceSimpleData.getProvinceID() == generatedSeaProvinceData.id()){
                    province.setReprColor(provinceSimpleData.getRgb());
                    break;
                }
            }

            id2seaProvince.put(province.getId(), province);
            repr_color_province_sea.put(province.getReprColor(), province);
        }

        for (DataGen.GeneratedProvinceData generatedProvinceData: prs){
            Province origin = id2province.get(generatedProvinceData.id());

            for (Integer id : generatedProvinceData.neighbouring_provinces()){
                if (id2seaProvince.containsKey(id))
                    origin.addSeaNeighbour(id2seaProvince.get(id));
                else
                    origin.addNeighbour(id2province.get(id));
            }
        }

        for (DataGen.GeneratedSeaProvinceData generatedProvinceData: prs_sea){
            SeaProvince origin = id2seaProvince.get(generatedProvinceData.id());

            for (Integer id : generatedProvinceData.neighbouring_provinces()){
                if (id2seaProvince.containsKey(id))
                    origin.addSeaNeighbour(id2seaProvince.get(id));
                else
                    origin.addNeighbour(id2province.get(id));
            }
        }

        for (DataGen.GeneratedCountryData countryData: country_defs){
            Person governor = id2person.get(countryData.governor_id());
            Person leader = id2person.get(countryData.leader_id());

            Province province = country_id2province.get(countryData.country_id());

            for (Person p: people){
                if (p.getId() != governor.getId() && p.getId() != leader.getId() && person2location_id.get(p) == province.getId()){
                    province.addPerson(p);
                }
            }

            province.setGovernor(governor);

            province.getGeneratedCulture().setTribal(false);
            Country country = new Country(leader, new ArrayList<>(List.of(province)), countryData.country_name(),
                    Country.GovernmentSystem.ABSOLUTE_MONARCHY,
                    province.getGeneratedCulture(), Utils.colorToVec(new Color(province.getReprColor())),
                    ExtFileLoader.loadExternalImage("world/flags/"+countryData.country_id()+".png"), countryData.country_id(),
                    true);
            country.setRulingDynasty(new Dynasty(leader.getSurname()));
            province.addClaim(new ProvinceClaim(ProvinceClaim.ClaimType.CORE, country));

            country.getCulture().setTribal(false);

            countries.add(country);
            Army army = new Army(country);
            army.addRegiment(new InfantryRegiment(country));
            province.addUnit(army);
            province.setOwner(country);
        }

        for (Province province: provinces){
            if (province.getOwner() == null){
                for (UrbanPoint urbanPoint: province.getUrbanPoints()){
                    urbanPoint.setLowerClassProportion(1);
                    urbanPoint.setMiddleClassProportion(0);
                    urbanPoint.setHigherClassProportion(0);
                }
            }
        }

        for (Map.Entry<String, Integer> e: culture_colors.entrySet()){
            Color ccc = new Color(e.getValue());
            sc.get(e.getKey()).setColor(new Vector4f(ccc.getRed()/255f, ccc.getGreen()/255f, ccc.getBlue()/255f, 1));
        }
    }

    public ArrayList<Person> getPeople() {
        return people;
    }

    public ArrayList<Province> getProvinces() {
        return provinces;
    }

    public ArrayList<Country> getCountries() {
        return countries;
    }

    public HashMap<Integer, Province> getReprColorsProvince(){
        return repr_color_province;
    }

    public HashMap<Integer, SeaProvince> getReprColorsProvinceSea() {
        return repr_color_province_sea;
    }

    public ArrayList<SeaProvince> getSeaProvinces() {
        return seaProvinces;
    }

    public Culture getCulture(HashMap<String, Culture> sc, String culture, boolean isTribal){
        if (sc.containsKey(culture)){
            return sc.get(culture);
        }
        else {
            Culture _culture = new Culture(culture, isTribal);
            sc.put(culture, _culture);
            return _culture;
        }
    }
}
