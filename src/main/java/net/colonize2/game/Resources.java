package net.colonize2.game;

import java.util.List;
import java.util.Random;

public class Resources {

    private static final Random RANDOM = new Random();

    public enum Resource{
        GRAIN(2),
        LIVESTOCK(2),
        TIMBER(2),

        FUR(2.5f),
        WOOL(2.5f),
        WINE_GRAPES(2.5f),
        FISH(2.5f),

        CLOTH(3),
        SULPHUR(3),
        COPPER(3),
        SALT(3),
        COTTON(3),
        SUGAR(3),
        GLASS(3),
        IRON(3),

        COCOA(4),
        SILK(4),
        DYES(4),
        TUNGSTEN(4),

        COAL(4.5f),

        GEMS(8),

        GOLD(13f),

        // produced

        BOTTLED_WINE(3),
        BREAD(3),
        CANNED_FOOD(3),
        CEMENT(3),
        MACHINE_PARTS(3),
        RUBBER(3),
        FABRIC(3),

        PAPER(3.5f),
        EXPLOSIVES(3.5f),
        AMMUNITION(3.5f),
        SMALL_ARMS(3.5f),
        ELECTRIC_PARTS(3.5f),

        ARTILLERY(4),
        CLOTHES(4),
        STEEL(4),
        FUEL(4),

        TANK(5),
        AEROPLANE(5),

        // tropical
        TEA(2),

        TOBACCO(2.5f),

        CHINAWARE(3),
        SPICES(3),
        COFFEE(3),

        TROPICAL_WOOD(4),
        IVORY(4),

        CLOVES(5),

        UNGENERATED_TROPICAL(0){
            @Override
            public String toString() {
                return "Ungenerated";
            }
        },
        UNGENERATED(0){
            @Override
            public String toString() {
                return "Ungenerated";
            }
        }

        ;
        private final float price;

        Resource(float price){
            this.price = price;
        }

        public float getPrice() {
            return price;
        }

        public String toString(){
            String str = this.name();
            return (str.substring(0,1).toUpperCase()+str.substring(1).toLowerCase()).replace('_', ' ');
        }

        public String toID(){
            return this.getClass().getSimpleName().toLowerCase();
        }
    }

    public static final List<Resources.Resource> agroLivestockResources = List.of(Resource.GRAIN, Resource.LIVESTOCK,
            Resource.WOOL);
    public static final List<Resources.Resource> minedResources = List.of(Resource.IRON, Resource.COAL, Resource.GEMS,
            Resource.GOLD, Resource.TUNGSTEN, Resource.SULPHUR, Resource.COPPER);

    public static Resource fromID(String id){
        for (Resource res : Resource.values()){
            if (res.toID().equals(id))
                return res;
        }
        return null;
    }

    public static Resource generateResource(int mountain_amount,
                                            int land_amount,
                                            int cold_land_amount,
                                            int tropical_land_amount,
                                            int desert_land){
        float tropical_concentration = tropical_land_amount/((float)land_amount);

        boolean isTropical = RANDOM.nextInt(0, 101) <= (10 + (tropical_concentration) * 90);

        if (!isTropical){
            float rock_concentration = ((float) mountain_amount) / land_amount;
            int ore_gen_chance = (int) (10+rock_concentration*90);

            float cold_concentration = ((float) cold_land_amount) / land_amount;
            int cold_gen_chance = (int) (10+cold_concentration*90);

            float desert_concentration = ((float) desert_land) / land_amount;
            int desert_gen_chance = (int) (10+desert_concentration*90);

            if (RANDOM.nextInt(0, 101) <= ore_gen_chance){
                if (RANDOM.nextInt(0, 101) <= 5){
                    return Resource.GOLD;
                }
                else{
                    switch (RANDOM.nextInt(0, 4)){
                        case 0:
                            return Resource.IRON;
                        case 1:
                            return Resource.COAL;
                        case 2:
                            return Resource.COPPER;
                        case 3:
                            return Resource.TUNGSTEN;
                    }
                }
            }
            else if(RANDOM.nextInt(0, 101) <= cold_gen_chance){
                int gen = RANDOM.nextInt(0, 101);

                if (gen <= 40){
                    switch (RANDOM.nextInt(0, 2)){
                        case 0:
                            return Resource.LIVESTOCK;
                        case 1:
                            return Resource.TIMBER;
                    }
                }
                else if (gen <= 70){
                    switch (RANDOM.nextInt(0, 3)){
                        case 0:
                            return Resource.FUR;
                        case 1:
                            return Resource.WOOL;
                        case 2:
                            return Resource.FISH;
                    }
                }
                else if (gen <= 85){
                    switch (RANDOM.nextInt(0, 3)){
                        case 0:
                            return Resource.SULPHUR;
                        case 1:
                            return Resource.SALT;
                        case 2:
                            return Resource.SUGAR;
                    }
                }
                else if (gen <= 97){
                    return Resource.SILK;
                }
                else return Resource.GEMS;
            }
            else if(RANDOM.nextInt(0, 101) <= desert_concentration){
                int gen = RANDOM.nextInt(0, 101);

                if (gen <= 40){
                    return Resource.LIVESTOCK;
                }
                else if (gen <= 70){
                   return Resource.WOOL;
                }
                else if (gen <= 85){
                    switch (RANDOM.nextInt(0, 2)){
                        case 0:
                            return Resource.SULPHUR;
                        case 1:
                            return Resource.SALT;
                    }
                }
                else if (gen <= 97){
                    return Resource.SILK;
                }
                else return Resource.GEMS;
            }
            else{
                int gen = RANDOM.nextInt(0, 101);

                if (gen <= 40){
                    switch (RANDOM.nextInt(0, 3)){
                        case 0:
                            return Resource.GRAIN;
                        case 1:
                            return Resource.LIVESTOCK;
                        case 2:
                            return Resource.TIMBER;
                    }
                }
                else if (gen <= 70){
                    switch (RANDOM.nextInt(0, 4)){
                        case 0:
                            return Resource.FUR;
                        case 1:
                            return Resource.WOOL;
                        case 2:
                            return Resource.WINE_GRAPES;
                        case 3:
                            return Resource.FISH;
                    }
                }
                else if (gen <= 85){
                    switch (RANDOM.nextInt(0, 5)){
                        case 0:
                            return Resource.CLOTH;
                        case 1:
                            return Resource.SULPHUR;
                        case 2:
                            return Resource.SALT;
                        case 3:
                            return Resource.SUGAR;
                        case 4:
                            return Resource.GLASS;
                    }
                }
                else if (gen <= 97){
                    switch (RANDOM.nextInt(0, 3)){
                        case 0:
                            return Resource.COCOA;
                        case 1:
                            return Resource.SILK;
                        case 2:
                            return Resource.DYES;
                    }
                }
                else return Resource.GEMS;
            }
        }
        else{
            int gen = RANDOM.nextInt(0, 101);

            if (gen <= 40){
                switch (RANDOM.nextInt(0, 2)){
                    case 0:
                        return Resource.TEA;
                    case 1:
                        return Resource.TOBACCO;
                }
            }
            else if (gen <= 70){
                switch (RANDOM.nextInt(0, 3)){
                    case 0:
                        return Resource.CHINAWARE;
                    case 1:
                        return Resource.SPICES;
                    case 2:
                        return Resource.COFFEE;
                }
            }
            else if (gen <= 85){
                switch (RANDOM.nextInt(0, 2)){
                    case 0:
                        return Resource.TROPICAL_WOOD;
                    case 1:
                        return Resource.IVORY;
                }
            }
            else if (gen <= 97){
                return Resource.CLOVES;
            }
            else return Resource.GEMS;
        }
        return Resource.GRAIN;
    }

    public static Resource generateResource(Province pr){
        return generateResource(pr.getMountainAmount(), pr.getLandAmount(),
                pr.getColdLandAmount(), pr.getTropicalLandAmount(), pr.getDesertLand());
    }

}
