package net.colonize2.game.buildings;

import net.colonize2.game.Game;
import net.colonize2.game.Province;
import net.colonize2.game.trade.TradeNode;

public class Marketplace extends Building{
    public Marketplace() {
        super(3, "Marketplace", "Increases province's trade outpost level");
    }

    @Override
    public void onFinishConstruction(Game g, Province pr) {
        pr.upgradeTradeOutpost();

        boolean shouldSpawnTradeNode = true;
        for (TradeNode node: g.getTradeNodes()){
            int dist2 = (node.getProvince().getAvgX()-pr.getAvgX())*(node.getProvince().getAvgX()-pr.getAvgX()) +
                    (node.getProvince().getAvgY()-pr.getAvgY())*(node.getProvince().getAvgY()-pr.getAvgY());
            if (dist2 <= 400 * 400) {
                shouldSpawnTradeNode = false;
                break;
            }
        }

        if (shouldSpawnTradeNode){
            TradeNode nTradeNode = new TradeNode(pr);
            pr.setControlledTradeNode(nTradeNode);
            g.addTradeNode(nTradeNode);

            nTradeNode.getTradeInformation(pr.getOwner()).setTradePower(200);

            nTradeNode.searchAndAddCountriesToTheNode(g);
        }
    }

    @Override
    public void onDestroyBuilding(Game g, Province pr) {
        pr.downgradeTradeOutpost();
    }

    @Override
    public String buildingIcon() {
        return "buildings/marketplace.png";
    }
}
