package net.colonize2.game.buildings;

import net.colonize2.game.trade.Stockpile;

public interface IBuildingOwner {
    Stockpile getStockpile();
}
