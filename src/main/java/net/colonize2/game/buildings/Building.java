package net.colonize2.game.buildings;

import net.colonize2.game.Game;
import net.colonize2.game.Province;
import org.jetbrains.annotations.Nullable;

public abstract class Building {

    private final int constructionTime;
    private final String buildingName;
    private final String description;
    private int currentConstructionTime;

    /***
     * null means that it is owned by province owner
     */
    private @Nullable IBuildingOwner buildingOwner = null;

    public @Nullable IBuildingOwner getBuildingOwner() {
        return buildingOwner;
    }

    public void setBuildingOwner(@Nullable IBuildingOwner buildingOwner) {
        this.buildingOwner = buildingOwner;
    }

    public Building(int constructionTime, String buildingName, String description){
        this.buildingName = buildingName;
        this.description = description;
        currentConstructionTime = 0;
        this.constructionTime = constructionTime;
    }

    public abstract void onFinishConstruction(Game g, Province pr);
    public abstract void onDestroyBuilding(Game g, Province pr);

    public void update(Game g, Province pr){
        if (currentConstructionTime < constructionTime) {
            currentConstructionTime++;
            if (currentConstructionTime == constructionTime){
                onFinishConstruction(g, pr);
            }
        }
    }

    public abstract String buildingIcon();

    public int getConstructionTime() {
        return constructionTime;
    }

    public int getCurrentConstructionTime() {
        return currentConstructionTime;
    }

    public void onForeignOwnership(Game g, Province pr){

    }
}
