package net.colonize2.game.buildings;

import net.colonize2.game.Country;
import net.colonize2.game.Province;

import java.lang.reflect.InvocationTargetException;

public class BuildingConstructionEntry {

    private final float price;
    private final String buildingIcon;
    private final Class<?> building;
    private final String buildingName;
    private final String description;

    public BuildingConstructionEntry(float price, String buildingIcon, Class<?> building,
                                     String buildingName, String description) {
        this.price = price;
        this.buildingIcon = buildingIcon;
        this.building = building;
        this.buildingName = buildingName;
        this.description = description;
    }

    public boolean buildWithChecks(Province pr, Country c){
        if (c.getGold() >= price){
            c.setGold(c.getGold()-price);

            try {
                pr.addBuilding((Building) building.getConstructor().newInstance());
                return true;
            } catch (InstantiationException | NoSuchMethodException | InvocationTargetException |
                     IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return false;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public String getDescription() {
        return description;
    }

    public float getPrice() {
        return price;
    }

    public String getBuildingIcon() {
        return buildingIcon;
    }
}
