package net.colonize2.game.buildings.production;

public class Mine extends GenericProductionBuilding{
    public Mine() {
        super(3, "Mine", "Increases production level in provinces with goods which can be extracted from below", "buildings/placeholder.png");
    }
}
