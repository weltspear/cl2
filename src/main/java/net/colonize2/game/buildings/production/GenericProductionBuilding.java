package net.colonize2.game.buildings.production;

import net.colonize2.game.Game;
import net.colonize2.game.Province;
import net.colonize2.game.buildings.Building;

public class GenericProductionBuilding extends Building {
    private final String icon;

    public GenericProductionBuilding(int constructionTime, String name, String description, String icon){
        super(constructionTime, name, description);
        this.icon = icon;
    }

    @Override
    public void onFinishConstruction(Game g, Province pr) {
        pr.setProductionLevel(pr.getProductionLevel()+1);
    }

    @Override
    public void onDestroyBuilding(Game g, Province pr) {
        pr.setProductionLevel(pr.getProductionLevel()-1);
    }

    @Override
    public String buildingIcon() {
        return icon;
    }
}
