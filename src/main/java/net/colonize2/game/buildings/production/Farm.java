package net.colonize2.game.buildings.production;

public class Farm extends GenericProductionBuilding {
    public Farm(){
        super(3, "Farm",
                "Increases production level in provinces with agricultural or livestock related goods",
                "buildings/farm.png");
    }
}
