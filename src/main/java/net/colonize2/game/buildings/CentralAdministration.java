package net.colonize2.game.buildings;

import net.colonize2.game.Game;
import net.colonize2.game.Province;

public class CentralAdministration extends Building{
    public CentralAdministration() {
        super(3, "Central Administration", "Increases province admin level");
    }

    @Override
    public void onFinishConstruction(Game g, Province pr) {
        pr.setAdminLevel(pr.getAdminLevel()+1);
    }

    @Override
    public void onDestroyBuilding(Game g, Province pr) {
        pr.setAdminLevel(pr.getAdminLevel()-1);
    }

    @Override
    public String buildingIcon() {
        return "buildings/placeholder.png";
    }
}
