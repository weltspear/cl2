package net.colonize2.game;

import net.colonize2.buffers.IDiscoveredProvinceManager;

public class HumanPlayer implements IPlayer{

    private final Country country;
    private final PopupEventManager eventManager;
    private boolean hasEndedTurn = false;

    public HumanPlayer(Country country){
        this.country = country;
        this.eventManager = new PopupEventManager(this);
    }

    @Override
    public void update() {

    }

    @Override
    public void startTurn(Game g) {
        hasEndedTurn = false;
    }

    @Override
    public boolean hasEndedTurn() {
        return hasEndedTurn;
    }

    public void endTurn(){
        hasEndedTurn = true;
    }

    public Country getCountry() {
        return country;
    }

    @Override
    public PopupEventManager getEventManager() {
        return eventManager;
    }

    private IDiscoveredProvinceManager discoveredProvinces;

    public void setDiscoveredProvinces(IDiscoveredProvinceManager discoveredProvinces) {
        this.discoveredProvinces = discoveredProvinces;
    }

    @Override
    public void discoverProvince(Province pr) {
        discoveredProvinces.makeDiscovered(pr.getId());
    }

}
