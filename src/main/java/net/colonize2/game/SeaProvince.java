package net.colonize2.game;

import java.util.ArrayList;

public class SeaProvince {

    private final String name;
    private final int id;
    private final int avgX;
    private final int avgY;

    private final ArrayList<Province> neighbours = new ArrayList<>();
    private final ArrayList<SeaProvince> seaNeighbours = new ArrayList<>();

    /***
     * Province color in provinces.bmp
     */
    private /*rgb*/ int province_repr_color;

    public SeaProvince(String name,
                       int id,
                       int avg_x,
                       int avg_y){

        this.name = name;
        this.id = id;
        avgX = avg_x;
        avgY = avg_y;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getAvgX() {
        return avgX;
    }

    public int getAvgY() {
        return avgY;
    }

    public void addNeighbour(Province province) {
        neighbours.add(province);
    }

    public void addSeaNeighbour(SeaProvince province) {
        seaNeighbours.add(province);
    }

    public ArrayList<Province> getNeighbours() {
        return neighbours;
    }

    public ArrayList<SeaProvince> getSeaNeighbours() {
        return seaNeighbours;
    }

    public void setReprColor(int c) {
        province_repr_color = c;
    }

    public int getReprColor() {
        return province_repr_color;
    }
}
