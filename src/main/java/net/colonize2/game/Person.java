package net.colonize2.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Person {

    private final String name;
    private final String surname;
    private float age;
    private final int adminSkill;
    private final int productionSkill;
    private final int milSkill;
    private final ArrayList<String> traits;
    private Province location;
    private final String position;
    private final Culture culture;
    private final int id;

    private boolean isDead = false;

    private static Random random = new Random();

    public Person(String name, String surname, float age,
                  int admin_skill,
                  int production_skill,
                  int mil_skill,
                  String[] traits,
                  String position,
                  Culture culture,
                  int id
                  ){

        this.name = name;
        this.surname = surname;
        this.age = age;
        adminSkill = admin_skill;
        productionSkill = production_skill;
        milSkill = mil_skill;
        this.traits = new ArrayList<>(List.of(traits));
        this.position = position;
        this.culture = culture;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public float getAge() {
        return age;
    }

    public int getAdminSkill() {
        return adminSkill;
    }

    public int getProductionSkill() {
        return productionSkill;
    }

    public int getMilSkill() {
        return milSkill;
    }

    public ArrayList<String> getTraits() {
        return traits;
    }

    public Province getLocation() {
        return location;
    }

    public String getPosition() {
        return position;
    }

    public Culture getCulture() {
        return culture;
    }

    public int getId() {
        return id;
    }

    public void setLocation(Province location) {
        this.location = location;
    }

    public void update(){
        age += 0.3f;
        age = (float) (Math.ceil(age*10)/10);

        if (age > 50 && age < 72){
            if (random.nextInt(1, 101) < 2){
                isDead = true;
            }
        }
        else if (age > 72) {
            if (random.nextInt(1, 101) < Math.ceil((age-64)*(age-64)/9)+5){
                isDead = true;
            }
        }
    }

    public boolean isDead() {
        return isDead;
    }
}
