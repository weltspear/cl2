package net.colonize2.game;

public class UrbanPoint {
    private int population;
    private String name;
    private UrbanPointType pointType;
    private Culture culture;

    private float lowerClassProportion = 0.7f;
    private float middleClassProportion = 0.25f;
    private float higherClassProportion = 0.05f;

    public static final float HIGHER_CLASS_LIMIT = 0.1f;
    public static final float MIDDLE_CLASS_LIMIT = 0.4f;

    public UrbanPoint(int population, String name, UrbanPointType pointType, Culture culture){
        this.population = population;
        this.name = name;
        this.pointType = pointType;
        this.culture = culture;
    }

    public int getPopulation() {
        return population;
    }

    public String getName() {
        return name;
    }

    public UrbanPointType getPointType() {
        return pointType;
    }

    public Culture getCulture() {
        return culture;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPointType(UrbanPointType pointType) {
        this.pointType = pointType;
    }

    public void setCulture(Culture culture) {
        this.culture = culture;
    }

    public float getLowerClassProportion() {
        return lowerClassProportion;
    }

    public float getMiddleClassProportion() {
        return middleClassProportion;
    }

    public float getHigherClassProportion() {
        return higherClassProportion;
    }

    public void setLowerClassProportion(float lowerClassProportion) {
        this.lowerClassProportion = lowerClassProportion;
    }

    public void setMiddleClassProportion(float middleClassProportion) {
        this.middleClassProportion = middleClassProportion;
    }

    public void setHigherClassProportion(float higherClassProportion) {
        this.higherClassProportion = higherClassProportion;
    }

    public void changeSocialClassProportions(float prosperity){
        if (lowerClassProportion > 0){
            if (middleClassProportion < 0.4f) {
                lowerClassProportion -= prosperity / 1000f;
                middleClassProportion += prosperity / 1000f;
            }

            if (higherClassProportion < 0.1f) {
                middleClassProportion -= prosperity / 10000f;
                higherClassProportion += prosperity / 10000f;
            }
        }
    }
}
