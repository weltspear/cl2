package net.colonize2.game.diplomacy;

import net.colonize2.game.Game;

public class DummyDiploRequest implements DiplomaticRequest{
    @Override
    public String description() {
        return "Test";
    }

    @Override
    public void accept(Game g) {

    }

    @Override
    public void decline(Game g) {

    }
}
