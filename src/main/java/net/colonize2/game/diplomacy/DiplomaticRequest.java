package net.colonize2.game.diplomacy;

import net.colonize2.game.Game;

public interface DiplomaticRequest {
    String description();
    void accept(Game g);
    void decline(Game g);
}
