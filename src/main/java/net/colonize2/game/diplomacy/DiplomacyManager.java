package net.colonize2.game.diplomacy;

import net.colonize2.game.*;
import net.colonize2.game.history.ProvinceClaim;

import java.util.ArrayList;
import java.util.HashMap;

public class DiplomacyManager {

    private final Country country;

    private final HashMap<Country, Integer> opinionOfOtherCountries = new HashMap<>();

    private float infamy = 0;

    private final ArrayList<Country> rivals = new ArrayList<>();

    public interface DiplomaticProcess {
        void turnUpdate(Game g);
        boolean isFinished();
    }
    private final ArrayList<DiplomaticProcess> diplomaticProcesses = new ArrayList<>();
    public static final int MAX_DIPLOMATIC_PROCESS_AMOUNT = 3;

    public class FabricateClaimProcess implements DiplomaticProcess {
        private final Province pr;
        private int turns = 0;

        public FabricateClaimProcess(Province pr){
            this.pr = pr;
        }


        @Override
        public void turnUpdate(Game g) {
            if (country.getPoliticalPower() > 1){
                country.setPoliticalPower(country.getPoliticalPower()-1);
                turns+=1;
            }

            if (turns == 7){
                pr.addClaim(new ProvinceClaim(ProvinceClaim.ClaimType.CLAIM, country, 12));
                g.getPlayer(country).getEventManager().issueEvent(new PopupEvent() {
                    @Override
                    public ArrayList<EventOption> getOptions() {
                        return null;
                    }

                    @Override
                    public String getText() {
                        return "Claim on " + pr.getName() + " has been fabricated";
                    }
                });
            }
        }

        @Override
        public boolean isFinished() {
            return turns == 7;
        }

        public Province getProvince() {
            return pr;
        }
    }

    public DiplomacyManager(Country country){

        this.country = country;
    }

    public int getOpinion(Country country){
        return opinionOfOtherCountries.getOrDefault(country, 0);
    }

    public void setOpinion(Country country, int opinion){
        opinionOfOtherCountries.put(country, opinion >= -200 && opinion <= 200? opinion : opinion <= -200 ? -200 :
                200 );
    }

    public float getInfamy() {
        return infamy;
    }

    public void setInfamy(float infamy) {
        this.infamy = infamy;
    }

    public void update(Game g) {
        ArrayList<DiplomaticProcess> toBeRemoved = new ArrayList<>();
        for (DiplomaticProcess process: diplomaticProcesses){
            process.turnUpdate(g);
            if (process.isFinished())
                toBeRemoved.add(process);
        }
        diplomaticProcesses.removeAll(toBeRemoved);
    }

    public void improveRelations(Country other){
        if (!other.getDiplomacyManager().getRivals().contains(country))
            if (country.getPoliticalPower() >= 5) {
                setOpinion(other, getOpinion(other) + 20);
                other.getDiplomacyManager().setOpinion(country, other.getDiplomacyManager().getOpinion(country) + 20);
                country.setPoliticalPower(country.getPoliticalPower()-5);
            }

    }

    public void sendAnInsult(Country other){
        if (country.getPoliticalPower() >= 5) {
            other.getDiplomacyManager().setOpinion(country, other.getDiplomacyManager().getOpinion(country) - 40);
            country.setPoliticalPower(country.getPoliticalPower()-5);
        }

    }

    public void integrate(Country other, Game g) {
        if (country.isOurPuppet(other))
            if (country.getPoliticalPower() >= 125) {
                country.annexCountry(other, g);

                country.setPoliticalPower(country.getPoliticalPower()-125);
            }

    }

    ArrayList<DiplomaticRequest> requests = new ArrayList<>();

    public void issueRequest(DiplomaticRequest request){
        requests.add(request);
    }

    public DiplomaticRequest getRequest(){
        if (!requests.isEmpty()){
            try {
                return requests.get(0);
            } finally {
                requests.remove(0);
            }
        }
        else return null;
    }

    public void issuePuppetRequestToOther(Country other) {
        if (other.getOverlord() == null) {
            if (country.getPoliticalPower() >= 125) {
                other.getDiplomacyManager().issueRequest(new PuppetRequest(country, other));
                country.setPoliticalPower(country.getPoliticalPower()-125);
            }
        }
    }

    public boolean isSurroundedByEnemies(){
        int enemies = 0;

        for (Province province : country.getProvinces()){
            for (Province neighbour: province.getNeighbours()){
                if (neighbour.getOwner() != country && neighbour.getOwner() != null){
                    if (neighbour.getOwner().getDiplomacyManager().getOpinion(country) <= -120
                    && neighbour.getOwner().getProvinces().size() > country.getProvinces().size()*1.5f){
                        enemies++;
                    }
                }
            }
        }

        return enemies > 5;
    }

    public int getMaxRivals(){
        return 2;
    }

    public void addRival(Country rival){
        if (!rivals.contains(rival)) {
            rivals.add(rival);
            setOpinion(rival, -200);
            rival.getDiplomacyManager().setOpinion(country, -200);
        }
    }

    public ArrayList<Country> getRivals() {
        return rivals;
    }

    public void fabricateClaimOnAStateWithChecks(Province province){
        if (diplomaticProcesses.size() < MAX_DIPLOMATIC_PROCESS_AMOUNT){
            if (country.getPoliticalPower() >= 25){
                country.setPoliticalPower(country.getPoliticalPower()-25);
                diplomaticProcesses.add(new FabricateClaimProcess(province));
            }
        }
    }

    public ArrayList<DiplomaticProcess> getDiplomaticProcesses() {
        return diplomaticProcesses;
    }
}
