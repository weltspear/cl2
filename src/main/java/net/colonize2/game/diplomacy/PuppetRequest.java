package net.colonize2.game.diplomacy;

import net.colonize2.game.Country;
import net.colonize2.game.Game;

public class PuppetRequest implements DiplomaticRequest{

    private final Country issuer;
    private final Country receiver;

    public PuppetRequest(Country issuer, Country receiver){
        this.issuer = issuer;
        this.receiver = receiver;
    }

    @Override
    public String description() {
        return String.format("%s offers us protection in a form of subjugation", issuer.getName());
    }

    @Override
    public void accept(Game g) {
        issuer.addPuppet(receiver, g);
    }

    @Override
    public void decline(Game g) {

    }

    public Country getIssuer() {
        return issuer;
    }
}
