package net.colonize2.game;

import net.colonize2.game.diplomacy.DiplomaticRequest;
import net.colonize2.game.diplomacy.PuppetRequest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Random;

public class BotPlayer implements IPlayer{
    private final Country country;
    private final PopupEventManager eventManager;

    private final Random random = new Random();

    public BotPlayer(Country country){
        this.country = country;
        eventManager = new PopupEventManager(this);

    }

    @Override
    public void update() {
        eventManager.ignoreAllOneOptionEvents();
    }

    private float lgold = 50;

    private boolean hasEndedTurn = true;

    private int rivalRearrange = 20;

    @Override
    public void startTurn(Game g) {
        hasEndedTurn = false;

        if (country.getGold() - lgold > 0){
            for (Province pr: country.getProvinces()){
                if (!Objects.equals(pr.getCulture(), country.getCulture())){
                    pr.assimilateWithChecks();
                }
            }
        }

        if (country.getGold() - lgold > 0 && country.getGold() >= Country.colonizationCost
               ) {
            ArrayList<Province> neighbours = new ArrayList<>();
            for (Province pr : country.getProvinces()) {
                for (Province _pr : pr.getNeighbours()) {
                    if (_pr.getOwner() == null) {
                        neighbours.add(_pr);
                    }
                }
            }

            if (!neighbours.isEmpty()) {
                country.colonizeWithChecks(
                        neighbours.get(random.nextInt(neighbours.size())),
                        country, g);
            }

        }

        lgold = country.getGold();

        DiplomaticRequest request = country.getDiplomacyManager().getRequest();
        while (request != null){
            if (request instanceof PuppetRequest puppetRequest){
                if ((puppetRequest.getIssuer().getProvinces().size() > 2.5f*country.getProvinces().size()
                && country.getDiplomacyManager().getOpinion(puppetRequest.getIssuer()) > 160) ||
                        (puppetRequest.getIssuer().getProvinces().size() > 1.5f*country.getProvinces().size()
                                && country.getDiplomacyManager().getOpinion(puppetRequest.getIssuer()) > 160
                                && country.getDiplomacyManager().isSurroundedByEnemies())

                 ){
                    request.accept(g);
                }
            }
            else{
                request.decline(g);
            }
            request = country.getDiplomacyManager().getRequest();
        }

        // rivals
        if (rivalRearrange >= 20)
            if (country.getDiplomacyManager().getRivals().size() < country.getDiplomacyManager().getMaxRivals()){
                // rival neighbours if possible
                HashSet<Country> neighbours = new HashSet<>();
                for (Province pr: country.getProvinces()){
                    for (Province _pr: pr.getNeighbours()){
                        if (_pr.getOwner() != null && _pr.getOwner() != country){
                            neighbours.add(_pr.getOwner());
                        }
                    }
                }

                for (Country c: neighbours){
                    if ((c.getProvinces().size()/2 < country.getProvinces().size() || (random.nextBoolean() || c.getDiplomacyManager().getRivals().contains(country) ||  c.getDiplomacyManager().getOpinion(country) < -100))){
                        country.getDiplomacyManager().addRival(c);
                        if (!(country.getDiplomacyManager().getRivals().size() < country.getDiplomacyManager().getMaxRivals())){
                            break;
                        }
                    }
                }
                rivalRearrange = 0;
            }
        else rivalRearrange++;

        hasEndedTurn = true;
    }

    @Override
    public boolean hasEndedTurn() {
        return hasEndedTurn;
    }

    public Country getCountry() {
        return country;
    }

    @Override
    public PopupEventManager getEventManager() {
        return eventManager;
    }

    @Override
    public void discoverProvince(Province pr) {

    }
}

