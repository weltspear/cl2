package net.colonize2.game;

public class Puppet {
    private final Country c;
    private final PuppetRank rank;

    public enum PuppetRank{
        SATELITE
    }

    public Puppet(Country c, PuppetRank rank){
        this.c = c;
        this.rank = rank;
    }

    public Country getCountry() {
        return c;
    }

    public PuppetRank getRank() {
        return rank;
    }
}
