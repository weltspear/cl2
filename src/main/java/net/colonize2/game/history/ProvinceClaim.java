package net.colonize2.game.history;

import net.colonize2.game.Country;

public class ProvinceClaim {

    private final ClaimType type;
    private final Country claimant;

    private int turnTime = -1;

    public enum ClaimType{
        CORE,
        CLAIM,
    }

    public ProvinceClaim(ClaimType type, Country claimant){
        this.type = type;
        this.claimant = claimant;
    }

    public ProvinceClaim(ClaimType type, Country claimant, int turnTime){
        this.type = type;
        this.claimant = claimant;
        this.turnTime = turnTime;
    }

    public void update(){
        if (turnTime != -1){
            turnTime--;
        }
    }

    public boolean isValid(){
        return turnTime != 0;
    }

    public ClaimType getType() {
        return type;
    }

    public Country getClaimant() {
        return claimant;
    }

    public int getTurnTime() {
        return turnTime;
    }
}
