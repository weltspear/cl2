package net.colonize2.game.trade;

import net.colonize2.game.Province;
import net.colonize2.game.Resources;
import net.colonize2.game.UrbanPoint;

import java.util.HashMap;
import java.util.List;

public class NeedsGenerator {

    public static void generateNeeds(Province province,
                                     HashMap<Resources.Resource, Float> needs){
        for (UrbanPoint urbanPoint: province.getUrbanPoints()){
            float baseNeed = urbanPoint.getPopulation()*0.001f;

            for (Resources.Resource resource: List.of(
                    Resources.Resource.GRAIN,
                    Resources.Resource.LIVESTOCK,
                    Resources.Resource.TIMBER,
                    Resources.Resource.FUR,
                    Resources.Resource.WOOL,
                    Resources.Resource.WINE_GRAPES,
                    Resources.Resource.FISH,
                    Resources.Resource.CLOTH,
                    Resources.Resource.SUGAR
            )){
                if (!needs.containsKey(resource))
                    needs.put(resource, 0f);
                needs.put(resource, needs.get(resource)+baseNeed);
            }

            for (Resources.Resource resource: List.of(
                    Resources.Resource.SULPHUR,
                    Resources.Resource.SALT,
                    Resources.Resource.COPPER,
                    Resources.Resource.IRON,
                    Resources.Resource.TUNGSTEN,
                    Resources.Resource.COCOA,
                    Resources.Resource.SILK,
                    Resources.Resource.DYES
            )){
                if (!needs.containsKey(resource))
                    needs.put(resource, 0f);
                needs.put(resource, needs.get(resource)+baseNeed*(urbanPoint.getMiddleClassProportion()+urbanPoint.getHigherClassProportion()));
            }

            for (Resources.Resource resource: List.of(
                    Resources.Resource.SULPHUR,
                    Resources.Resource.SALT,
                    Resources.Resource.COPPER,
                    Resources.Resource.IRON,
                    Resources.Resource.TUNGSTEN,
                    Resources.Resource.COCOA,
                    Resources.Resource.SILK,
                    Resources.Resource.DYES,
                    Resources.Resource.TEA,
                    Resources.Resource.TOBACCO,
                    Resources.Resource.CHINAWARE,
                    Resources.Resource.SPICES,
                    Resources.Resource.COFFEE,
                    Resources.Resource.TROPICAL_WOOD,
                    Resources.Resource.IVORY,
                    Resources.Resource.CLOVES
            )){
                if (!needs.containsKey(resource))
                    needs.put(resource, 0f);
                needs.put(resource, needs.get(resource)+baseNeed*(urbanPoint.getHigherClassProportion()*2));
            }
        }
    }

}
