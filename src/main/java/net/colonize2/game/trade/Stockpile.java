package net.colonize2.game.trade;

import net.colonize2.game.Resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Stockpile {

    private final HashMap<Resources.Resource, Float> stockpile = new HashMap<>();

    public Stockpile(){

    }

    public float getResource(Resources.Resource resource){
        return stockpile.getOrDefault(resource, 0f);
    }

    public void setResource(Resources.Resource resource, float amount){
        stockpile.put(resource, amount);
    }

    public void addResource(Resources.Resource resource, float amount){
        stockpile.put(resource, getResource(resource)+amount);
    }

    public Set<Resources.Resource> getResources(){
        return stockpile.keySet();
    }

}
