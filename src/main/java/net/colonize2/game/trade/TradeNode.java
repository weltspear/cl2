package net.colonize2.game.trade;

import net.colonize2.game.Country;
import net.colonize2.game.Game;
import net.colonize2.game.Province;
import net.colonize2.game.Resources;

import java.util.*;

public class TradeNode {

    public class TradeInformation {
        private float tradePower = 0;

        public TradeInformation(){

        }

        public float getTradePower() {
            return tradePower;
        }

        public void setTradePower(float tradePower) {
            this.tradePower = tradePower;
        }
    }

    public static class SellOrder{
        private final Resources.Resource resource;
        private float amount;
        private final Country country;
        private final float price;

        private int turnTime = 0;

        public SellOrder(Resources.Resource resource, float amount, Country country,
                         float price){
            this.resource = resource;
            this.amount = amount;
            this.country = country;
            this.price = price;
        }

        public float getPrice() {
            return price;
        }

        public Country getCountry() {
            return country;
        }

        public float getAmount() {
            return amount;
        }

        public Resources.Resource getResource() {
            return resource;
        }

        /***
         * Doesn't check whether buyer has the money
         */
        public void executeTrade(float amount, Country buyer){
            this.amount -= amount;
            buyer.getStockpile().addResource(resource, amount);
            buyer.setGold(buyer.getGold()-amount*price);
        }
    }

    public static class BuyOrder{
        private final Resources.Resource resource;
        private float amount;
        private final Country country;

        private int turnTime = 0;

        private boolean pay = true;

        public BuyOrder(Resources.Resource resource, float amount, Country country) {
            this.resource = resource;
            this.amount = amount;
            this.country = country;
        }

        public Resources.Resource getResource() {
            return resource;
        }

        public float getAmount() {
            return amount;
        }

        public Country getCountry() {
            return country;
        }
    }

    private final Province province;
    private ArrayList<Country> countriesInNode = new ArrayList<>();

    private HashMap<Country, TradeInformation> countryTradeInfo = new HashMap<>();

    private ArrayList<BuyOrder> buyOrders = new ArrayList<>();
    private HashMap<Resources.Resource, HashMap<Float, Float>> availableResources = new HashMap<>();

    public TradeNode(Province province){
        this.province = province;
    }

    public Province getProvince() {
        return province;
    }

    public ArrayList<Country> getCountriesInNode() {
        return countriesInNode;
    }

    public void searchAndAddCountriesToTheNode(Game g){
        if (!countriesInNode.contains(province.getOwner())){
            countriesInNode.add(province.getOwner());
        }

        for (Country c: g.getCountries()){
            for (Province pr: c.getProvinces()){
                int dist2 = (province.getAvgX()-pr.getAvgX())*(province.getAvgX()-pr.getAvgX()) +
                        (province.getAvgY()-pr.getAvgY())*(province.getAvgY()-pr.getAvgY());
                if (dist2 <= 200*200){
                    if (!countriesInNode.contains(c))
                        countriesInNode.add(c);
                    break;
                }
            }
        }
    }

    public TradeInformation getTradeInformation(Country c){
        if (!countryTradeInfo.containsKey(c))
            countryTradeInfo.put(c, new TradeInformation());
        return countryTradeInfo.get(c);
    }

    public void publishSellOrder(Resources.Resource resource, float amount, Country country,
                                float price){

        if (!availableResources.containsKey(resource)){
            availableResources.put(resource, new HashMap<>());
        }

        var price2amount = availableResources.get(resource);

        getTradeInformation(country).tradePower += amount;

        price2amount.put(price, price2amount.getOrDefault(price, 0f)+amount);
    }

    public void publishBuyOrder(Resources.Resource resource, float amount, Country country){
        buyOrders.add(new BuyOrder(resource, amount, country));
    }

    public void publishUnpaidBuyOrder(Resources.Resource resource, float amount, Country country){
        BuyOrder buyOrder = new BuyOrder(resource, amount, country);
        buyOrders.add(buyOrder);
        buyOrder.pay = false;
    }

    public void turnUpdate(){
        buyOrders.sort((o1, o2) -> {
            if (getTradeInformation(o1.country).tradePower < getTradeInformation(o2.country).tradePower) {
                return -1;
            } else if (getTradeInformation(o1.country).tradePower == getTradeInformation(o2.country).tradePower)
                return 0;
            return 1;
        });

        // execute all orders

        ArrayList<SellOrder> alreadyExecutedSellOrders = new ArrayList<>();
        ArrayList<BuyOrder> alreadyExecutedBuyOrders = new ArrayList<>();

        for (BuyOrder buyOrder: buyOrders){

            boolean isOrderExhausted = false;

            /*for (SellOrder sellOrder: sellOrders){
                if (sellOrder.resource == buyOrder.resource){
                    if (sellOrder.amount >= buyOrder.amount){
                        sellOrder.executeTrade(buyOrder.amount, buyOrder.country);
                        buyOrder.amount = 0;
                        isOrderExhausted = true;
                        break;
                    }
                    else {
                        sellOrder.executeTrade(sellOrder.amount, buyOrder.country);
                        buyOrder.amount -= sellOrder.amount;
                    }

                    if (sellOrder.amount <= 0){
                        alreadyExecutedSellOrders.add(sellOrder);
                    }
                }
            }*/

            if (!availableResources.containsKey(buyOrder.resource)){
                availableResources.put(buyOrder.resource, new HashMap<>());
            }
            var price2amount = availableResources.get(buyOrder.resource);
            ArrayList<Map.Entry<Float, Float>> price2amountList = new ArrayList<>(price2amount.entrySet());
            price2amountList.sort(Map.Entry.comparingByKey());

            for (var priceAndAmount: price2amountList){
                if (priceAndAmount.getValue() > 0){
                    var amountBought = priceAndAmount.getValue() < buyOrder.amount ? priceAndAmount.getValue(): buyOrder.amount;
                    buyOrder.country.getStockpile().addResource(buyOrder.resource, amountBought);
                    if (buyOrder.pay)
                        buyOrder.country.setGold(buyOrder.country.getGold()-amountBought*priceAndAmount.getKey());
                    buyOrder.amount -= amountBought;

                    if (buyOrder.pay)
                        getTradeInformation(buyOrder.country).tradePower += amountBought*0.1f;

                    if (priceAndAmount.getValue()-amountBought >= 0)
                        price2amount.put(priceAndAmount.getKey(), priceAndAmount.getValue()-amountBought);
                    else
                        price2amount.remove(priceAndAmount.getKey());

                    float totalTradePower = getTotalNodeTradePower();

                    for (Country country: countriesInNode){
                        float pc = getTradeInformation(country).tradePower/totalTradePower;
                        country.setGold(country.getGold()+pc*amountBought*priceAndAmount.getKey()/50);
                    }

                    if (buyOrder.amount <= 0){
                        isOrderExhausted = true;
                        break;
                    }
                }
            }



            if (isOrderExhausted){
                alreadyExecutedBuyOrders.add(buyOrder);
            }

            buyOrder.turnTime++;
        }

        for (BuyOrder buyOrder: buyOrders){
            if (buyOrder.turnTime > 5){
                // dead order
                alreadyExecutedBuyOrders.add(buyOrder);
            }
        }

        buyOrders.removeAll(alreadyExecutedBuyOrders);
    }

    public float getTotalNodeTradePower(){
        float tradePower = 0;
        for (var entry: countryTradeInfo.entrySet()){
            tradePower+=entry.getValue().tradePower;
        }
        return tradePower;
    }

    public Set<Resources.Resource> getWantedResources(){
        HashSet<Resources.Resource> resources = new HashSet<>();
        for (BuyOrder buyOrder: buyOrders){
            resources.add(buyOrder.resource);
        }
        return resources;
    }

    public Set<Resources.Resource> getAvailableResources(){
        HashSet<Resources.Resource> resources = new HashSet<>();
        for (var res: availableResources.entrySet()){
            if (!res.getValue().isEmpty()){
                resources.add(res.getKey());
            }
        }
        return resources;
    }
}
