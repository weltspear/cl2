package net.colonize2.game.companies;

import net.colonize2.game.Game;
import net.colonize2.game.Person;
import net.colonize2.game.Province;
import net.colonize2.game.buildings.IBuildingOwner;
import net.colonize2.game.trade.Stockpile;
import org.joml.Vector4f;

public interface ICompany extends IBuildingOwner {

    /***
     * Province shall still be responsible for updating company leader
     */
    Person getCompanyLeader();
    void setCompanyLeader(Person p);

    Province getCompanyHeadquarters();
    void setCompanyHeadquarters(Province hq);

    String getName();

    void turnUpdate(Game g);
    Stockpile getStockpile();
    Vector4f getCompanyColor();

    void setGold(float gold);
    float getGold();

    void dissolve();
    boolean isDissolved();

}
