package net.colonize2.game.army;

import net.colonize2.game.Country;

public class Regiment {

    public static final int MAX_MANPOWER_UNIT = 100;

    private int morale;
    private int manpower;
    private int atkFire;

    private int dfFire;

    private int atkShock;

    private int dfShock;

    private Country country;

    public Regiment(int manpower, int morale, int atkFire, int dfFire, int atkShock, int dfShock, Country country){
        this.morale = morale;
        this.atkFire = atkFire;
        this.dfFire = dfFire;
        this.atkShock = atkShock;
        this.dfShock = dfShock;
        this.manpower = manpower;
        this.country = country;
    }

    public int getMorale() {
        return morale;
    }

    public void setMorale(int morale) {
        this.morale = morale;
    }

    public int getManpower() {
        return manpower;
    }

    public void setManpower(int manpower) {
        this.manpower = manpower;
    }

    public int getAtkFire() {
        return atkFire;
    }

    public void setAtkFire(int atkFire) {
        this.atkFire = atkFire;
    }

    public int getDfFire() {
        return dfFire;
    }

    public void setDfFire(int dfFire) {
        this.dfFire = dfFire;
    }

    public int getAtkShock() {
        return atkShock;
    }

    public void setAtkShock(int atkShock) {
        this.atkShock = atkShock;
    }

    public int getDfShock() {
        return dfShock;
    }

    public void setDfShock(int dfShock) {
        this.dfShock = dfShock;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
