package net.colonize2.game.army;

import net.colonize2.game.Country;

import java.util.ArrayList;

public class Army {
    private final ArrayList<Regiment> regiments = new ArrayList<>();
    private final Country country;

    public Army(Country country){
        this.country = country;
    }

    public ArrayList<Regiment> getRegiments() {
        return regiments;
    }

    public void addRegiment(Regiment regiment){
        regiments.add(regiment);
    }

    public int getManpower(){
        int manpower = 0;
        for (Regiment r: regiments){
            manpower+=r.getManpower();
        }
        return manpower;
    }

    public Country getCountry() {
        return country;
    }
}
