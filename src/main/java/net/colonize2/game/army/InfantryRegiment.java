package net.colonize2.game.army;

import net.colonize2.game.Country;

public class InfantryRegiment extends Regiment {
    public InfantryRegiment(Country country) {
        super(100, 100, 1, 1, 1, 1, country);
    }
}
