package net.colonize2.game;

import java.util.ArrayList;

public class PopupEventManager {
    private final IPlayer player;
    private final ArrayList<PopupEvent> events = new ArrayList<>();

    public PopupEventManager(IPlayer player){
        this.player = player;
    }

    public void issueEvent(PopupEvent event){
        events.add(event);
    }

    public void ignoreAllOneOptionEvents(){
        ArrayList<PopupEvent> toRemove = new ArrayList<>();
        for (PopupEvent event: events){
            if (event.getOptions().size() == 1){
                event.getOptions().get(0).executeOption(player);
                toRemove.add(event);
            }
        }

        events.removeAll(toRemove);
    }

    public ArrayList<PopupEvent> getEvents(){
        return events;
    }

}
