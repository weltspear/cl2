package net.colonize2.game;

import net.colonize2.game.army.Army;
import net.colonize2.game.buildings.Building;
import net.colonize2.game.buildings.BuildingConstructionEntry;
import net.colonize2.game.buildings.CentralAdministration;
import net.colonize2.game.buildings.production.Farm;
import net.colonize2.game.buildings.Marketplace;
import net.colonize2.game.buildings.production.Mine;
import net.colonize2.game.history.ProvinceClaim;
import net.colonize2.game.trade.TradeNode;
import net.colonize2.gen.DataGen;
import net.colonize2.gen.FlagGen;
import net.colonize2.gen.NameGen;
import org.joml.Vector4f;

import java.util.*;

import static net.colonize2.game.Resources.agroLivestockResources;
import static net.colonize2.game.Resources.minedResources;

public class Province {

    private String name;
    private final int id;
    private final int landAmount;
    private final int mountainAmount;
    private final int coldLandAmount;
    private final int tropicalLandAmount;
    private final int desertLand;
    private Resources.Resource resource;
    private final ArrayList<UrbanPoint> urbanPoints;
    private final float populationGrowth;

    private int productionLevel;
    private int adminLevel;
    private int manpowerLevel;

    private Country owner;

    /***
     * Trade node capital of which is this province
     */
    private TradeNode controlledTradeNode = null;

    public TradeNode getControlledTradeNode() {
        return controlledTradeNode;
    }

    public void setControlledTradeNode(TradeNode controlledTradeNode) {
        this.controlledTradeNode = controlledTradeNode;
    }

    private final Culture generatedCulture;
    private final Culture generatedMinorityCulture;

    private final NameGen nameGen = new NameGen();
    private Person governor;

    private int colonizationProgress = 0;

    public int getColonizationProgress() {
        return colonizationProgress;
    }

    public void setColonizationProgress(int colonizationProgress) {
        this.colonizationProgress = colonizationProgress;
    }

    private final int avgX;
    private final int avgY;

    private final ArrayList<Province> neighbours = new ArrayList<>();
    private final ArrayList<SeaProvince> seaNeighbours = new ArrayList<>();
    private final ArrayList<ProvinceClaim> claims = new ArrayList<>();

    private static final Random random = new Random();

    private int tradeOutpostLevel = 0;

    private int buildingSlotCount = 0;

    /***
     * in percents
     */
    private float prosperity = 0;
    private float previousIncome = 1;

    /***
     * in percents
     */
    private int localStability = 100;
    private float unrest = 0f;

    private float investorPool = 0f;

    /***
     * Province color in provinces.bmp
     */
    private /*rgb*/ int province_repr_color;

    public enum ProvinceRank {
        PROVINCE,
        COLONIAL_PROVINCE,
    }

    private ProvinceRank provinceRank = ProvinceRank.PROVINCE;

    public ProvinceRank getProvinceRank() {
        return provinceRank;
    }

    public void setProvinceRank(ProvinceRank provinceRank) {
        this.provinceRank = provinceRank;
    }

    private final ArrayList<Person> people = new ArrayList<>();

    public Province(String name,
                    int id,
                    int land_amount,
                    int mountain_amount,
                    int cold_land_amount,
                    int tropical_land_amount,
                    int desert_land,
                    Resources.Resource resource,
                    ArrayList<UrbanPoint> urbanPoints,
                    float population_growth,
                    int production_level,
                    int admin_level,
                    int manpower_level,
                    Country owner, Culture culture, Culture minority_culture, Person governor
            , int avg_x, int avg_y) {

        this.name = name;
        this.id = id;
        landAmount = land_amount;
        mountainAmount = mountain_amount;
        coldLandAmount = cold_land_amount;
        tropicalLandAmount = tropical_land_amount;
        desertLand = desert_land;
        this.resource = resource;
        this.urbanPoints = urbanPoints;
        populationGrowth = population_growth;
        productionLevel = production_level;
        adminLevel = admin_level;
        manpowerLevel = manpower_level;
        this.owner = owner;
        this.generatedCulture = culture;
        generatedMinorityCulture = minority_culture;
        this.governor = governor;
        avgX = avg_x;
        avgY = avg_y;
        buildingSlotCount = (int) Math.ceil(getPopulation()/1350f);
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getLandAmount() {
        return landAmount;
    }

    public int getMountainAmount() {
        return mountainAmount;
    }

    public int getColdLandAmount() {
        return coldLandAmount;
    }

    public int getTropicalLandAmount() {
        return tropicalLandAmount;
    }

    public int getDesertLand() {
        return desertLand;
    }

    public Resources.Resource getResource() {
        return resource;
    }

    public int getPopulation() {
        int pops = 0;
        for (UrbanPoint urbanPoint : urbanPoints){
            pops += urbanPoint.getPopulation();
        }
        return pops;
    }

    public float getPopulationGrowth() {
        return populationGrowth;
    }

    public int getProductionLevel() {
        return productionLevel;
    }

    public int getAdminLevel() {
        return adminLevel;
    }

    public int getManpowerLevel() {
        return manpowerLevel;
    }

    public void setProductionLevel(int productionLevel) {
        this.productionLevel = productionLevel;
    }

    public void setAdminLevel(int adminLevel) {
        this.adminLevel = adminLevel;
    }

    public void setManpowerLevel(int manpowerLevel) {
        this.manpowerLevel = manpowerLevel;
    }

    public float getProsperity() {
        return prosperity;
    }

    public void setProsperity(float prosperity) {
        this.prosperity = prosperity;
    }

    public Country getOwner() {
        return owner;
    }

    public Culture getCulture() {
        HashMap<Culture, Integer> cp = new HashMap<>();
        for (UrbanPoint urbanPoint: urbanPoints){
            if (!cp.containsKey(urbanPoint.getCulture())){
                cp.put(urbanPoint.getCulture(), urbanPoint.getPopulation());
            }
            else{
                cp.put(urbanPoint.getCulture(), cp.get(urbanPoint.getCulture())+urbanPoint.getPopulation());
            }
        }

        if (!cp.entrySet().isEmpty())
            return Collections.max(cp.entrySet(), Map.Entry.comparingByValue()).getKey();
        else return null;
    }

    public ArrayList<Culture> getMinorityCulture() {
        HashMap<Culture, Integer> cp = new HashMap<>();
        ArrayList<Culture> mincultures = new ArrayList<>();
        for (UrbanPoint urbanPoint: urbanPoints){
            if (!cp.containsKey(urbanPoint.getCulture())){
                cp.put(urbanPoint.getCulture(), urbanPoint.getPopulation());
                mincultures.add(urbanPoint.getCulture());
            }
            else{
                cp.put(urbanPoint.getCulture(), cp.get(urbanPoint.getCulture())+urbanPoint.getPopulation());
                mincultures.add(urbanPoint.getCulture());
            }
        }

        mincultures.remove(Collections.max(cp.entrySet(), Map.Entry.comparingByValue()).getKey());
        return mincultures;
    }

    public Person getGovernor() {
        return governor;
    }

    public void addPerson(Person person) {
        person.setLocation(this);
        people.add(person);
    }

    /***
     * Does not check whether `person` exists in province
     */
    public void removePerson(Person person) {
        person.setLocation(null);
        people.remove(person);
    }

    public void setOwner(Country owner) {
        this.owner = owner;
    }

    public void setReprColor(int c) {
        province_repr_color = c;
    }

    public int getReprColor() {
        return province_repr_color;
    }

    public void setGovernor(Person governor) {
        this.governor = governor;
    }

    public void addNeighbour(Province province) {
        neighbours.add(province);
    }

    public void addSeaNeighbour(SeaProvince province) {
        seaNeighbours.add(province);
    }

    public ArrayList<Province> getNeighbours() {
        return neighbours;
    }

    public ArrayList<SeaProvince> getSeaNeighbours() {
        return seaNeighbours;
    }

    public int getAvgX() {
        return avgX;
    }

    public int getAvgY() {
        return avgY;
    }

    public void update(Game g) {
        // update claims
        ArrayList<ProvinceClaim> invalidClaims = new ArrayList<>();
        for (ProvinceClaim claim: claims){
            claim.update();
            if (!claim.isValid()){
                invalidClaims.add(claim);
            }
        }
        claims.removeAll(invalidClaims);

        //population = population + (int) Math.ceil(population * populationGrowth);
        ArrayList<UrbanPoint> pointsToBeRemoved = new ArrayList<>();

        Country.GovernmentSystem govSys = null;
        if (getOwner() != null){
            govSys = getOwner().getGovernmentSystem();
        }

        if (govSys != Country.GovernmentSystem.COLONIAL_GOVERNMENT) {
            for (UrbanPoint urbanPoint : urbanPoints) {
                if (provinceRank == ProvinceRank.COLONIAL_PROVINCE &&
                        getOwner().getCulture().equals(urbanPoint.getCulture())) {
                    urbanPoint.setPopulation((int) (urbanPoint.getPopulation() + urbanPoint.getPopulation() * populationGrowth * 100));
                } else if (provinceRank == ProvinceRank.COLONIAL_PROVINCE) {
                    urbanPoint.setPopulation((int) (urbanPoint.getPopulation() - urbanPoint.getPopulation() * populationGrowth * 70));
                } else {
                    if (getOwner() != null) {
                        if (getOwner().getCulture().equals(urbanPoint.getCulture()) || !urbanPoint.getCulture().isTribal())
                            urbanPoint.setPopulation((int) (urbanPoint.getPopulation() + urbanPoint.getPopulation() * populationGrowth));
                        else
                            urbanPoint.setPopulation((int) (urbanPoint.getPopulation() + urbanPoint.getPopulation() * populationGrowth / 2));
                    } else {
                        urbanPoint.setPopulation((int) (urbanPoint.getPopulation() + urbanPoint.getPopulation() * populationGrowth));
                    }

                }


                if (urbanPoint.getPopulation() <= 0) {
                    pointsToBeRemoved.add(urbanPoint);
                }
            }
        }
        else{
            for (UrbanPoint urbanPoint : urbanPoints) {
                if (provinceRank == ProvinceRank.COLONIAL_PROVINCE &&
                        getOwner().getCulture().equals(urbanPoint.getCulture())) {
                    urbanPoint.setPopulation((int) (urbanPoint.getPopulation() + urbanPoint.getPopulation() * populationGrowth * 100));
                } else if (provinceRank == ProvinceRank.COLONIAL_PROVINCE) {
                    urbanPoint.setPopulation((int) (urbanPoint.getPopulation() - urbanPoint.getPopulation() * populationGrowth * 150));
                } else {
                    if (getOwner().getCulture().equals(urbanPoint.getCulture()) || !urbanPoint.getCulture().isTribal()) {
                        if (!Objects.equals(getCulture(), getOwner().getCulture()))
                            urbanPoint.setPopulation((int) (urbanPoint.getPopulation() + urbanPoint.getPopulation() * populationGrowth * 2));
                        else urbanPoint.setPopulation((int) (urbanPoint.getPopulation() + urbanPoint.getPopulation() * populationGrowth));
                    }
                    else
                        urbanPoint.setPopulation((int) (urbanPoint.getPopulation() + urbanPoint.getPopulation() * populationGrowth / 2));
                }


                if (urbanPoint.getPopulation() <= 0) {
                    pointsToBeRemoved.add(urbanPoint);
                }
            }
        }

        urbanPoints.removeAll(pointsToBeRemoved);

        ArrayList<Person> dead = new ArrayList<>();
        for (Person p : people) {
            p.update();
            if (p.isDead()) {
                dead.add(p);
            }
        }

        if (getOwner() != null) {
            float income = getIncome();
            getOwner().setGold((float) Math.round((getOwner().getGold() + income) * 10) / 10);

            prosperity += (int)Math.ceil(1*(income/previousIncome))/100f;
            previousIncome = income;

            if (prosperity > 0)
                for (UrbanPoint ub: urbanPoints){
                    ub.changeSocialClassProportions(prosperity);
                }

            for (UrbanPoint ub: urbanPoints){
                investorPool += ub.getHigherClassProportion()*ub.getPopulation()*0.01f;
            }
        }

        if (governor != null) {
            governor.update();

            if (governor.isDead()) {
                String surname = g.getPersonNameGen().genSurname();
                if (getOwner() != null) {
                    if (getOwner().getRulingDynasty() != null) {
                        if (random.nextBoolean()) {
                            surname = getOwner().getRulingDynasty().surname();
                        }

                    }
                }

                Person newGovernor = new Person(g.getPersonNameGen().genName(),
                        surname,
                        27 + random.nextInt(23),
                        random.nextInt(6),
                        random.nextInt(6),
                        random.nextInt(6),
                        new String[]{},
                        "politician",
                        getCulture(),
                        random.nextInt());
                if (this.owner != null) {
                    if (g.getPlayer(owner) != null)
                        g.getPlayer(owner).getEventManager().issueEvent(new PopupEvent() {

                            private final Person prvGov = governor;

                            @Override
                            public ArrayList<EventOption> getOptions() {
                                return new ArrayList<>(List.of(new EventOption[]{new EventOption("OK") {
                                    @Override
                                    public int aiWeight(IPlayer player) {
                                        return 100;
                                    }

                                    @Override
                                    public void executeOption(IPlayer player) {

                                    }
                                }}));
                            }

                            @Override
                            public String getText() {
                                return String.format("Governor of %s, %s %s died.\nNew governor %s %s has taken over", name,
                                        prvGov.getName(), prvGov.getSurname(), newGovernor.getName(), newGovernor.getSurname());
                            }
                        });
                }

                dead.add(governor);
                governor = newGovernor;
            }
        }

        people.removeAll(dead);

        if (people.size() < 10) {
            people.add(new Person(g.getPersonNameGen().genName(),
                    g.getPersonNameGen().genSurname(),
                    27 + random.nextInt(23),
                    random.nextInt(6),
                    random.nextInt(6),
                    random.nextInt(6),
                    new String[]{},
                    DataGen.const_positions[random.nextInt(DataGen.const_positions.length)],
                    getCulture(),
                    random.nextInt()));
        }

        if (provinceRank == ProvinceRank.COLONIAL_PROVINCE) {
            colonizationProgress += 5;

            if (colonizationProgress >= 100) {
                provinceRank = ProvinceRank.PROVINCE;
                claims.add(new ProvinceClaim(ProvinceClaim.ClaimType.CORE, getOwner()));
                resource = Resources.generateResource(this);

                /*if (getOwner() != null) {
                    if (!culture.equals(getOwner().getCulture())) {
                        minorityCulture = culture;
                        culture = getOwner().getCulture();
                    }
                }*/
            }
        }

        if (assimilation_turns > 0){
            int assimilated = 0;
            for (UrbanPoint urbanPoint : urbanPoints){
                if (!urbanPoint.getCulture().equals(getOwner().getCulture())){
                    double percent_const = 0.003;
                    if (urbanPoint.getCulture().isTribal()){
                        percent_const+=0.01;
                    }
                    assimilated += (int) Math.ceil(urbanPoint.getPopulation()*percent_const);
                    urbanPoint.setPopulation(urbanPoint.getPopulation()-(int) Math.ceil(urbanPoint.getPopulation()*percent_const));
                }
            }
            assimilate:
            {
                for (UrbanPoint urbanPoint : urbanPoints) {
                    if (urbanPoint.getCulture().equals(getOwner().getCulture())) {
                        urbanPoint.setPopulation(urbanPoint.getPopulation() + assimilated);
                        break assimilate;
                    }
                }

                urbanPoints.add(new UrbanPoint(assimilated, nameGen.genRegionName(), UrbanPointType.VILLAGE_COMMUNITY,
                        getOwner().getCulture()));
            }
            assimilation_turns--;
        }

        if (owner == null && g.getCurrentTurn() < 120 && !urbanPoints.isEmpty()){
            if (g.shouldSpawnTribalState()){
                Country c = new Country(null,
                        new ArrayList<>(List.of(this)),
                        nameGen.genRegionName() + " Tribes",
                        Country.GovernmentSystem.TRIBAL,
                        getCulture(),
                        new Vector4f(random.nextFloat(), random.nextFloat(), random.nextFloat(), 1),
                        FlagGen.genFlag(),
                        Country.genCountryId(),
                        false);
                this.setOwner(c);
                g.addCountry(c);

                this.addClaim(new ProvinceClaim(ProvinceClaim.ClaimType.CORE, c));
                BotPlayer player = new BotPlayer(c);

                g.getProvinceColors().writeColor(id, c.getCountryColor());

                g.addPlayer(player);

                resource = Resources.generateResource(this);
            }
        }

        if (owner != null){
            owner.getStockpile().addResource(resource, 1f*productionLevel);
        }

        for (Building building: buildings){
            building.update(g, this);
        }

        if (owner != null)
            for (TradeNode node: g.getCountryTradeNodes(owner))
                node.getTradeInformation(owner).setTradePower(node.getTradeInformation(owner).getTradePower()
                        +tradeOutpostLevel*20);

        if (owner != null)
            for (Building b: buildings){
                if (b.getBuildingOwner() != null)
                    b.onForeignOwnership(g, this);
            }
    }

    private float getIncome() {
        float income = 0;

        for (UrbanPoint urbanPoint: urbanPoints) {
            float taxes = 0.0001f * (adminLevel / 10f) * urbanPoint.getPopulation();
            if (provinceRank == ProvinceRank.COLONIAL_PROVINCE || !urbanPoint.getCulture().equals(getOwner().getCulture())) {
                taxes /= 2;
            }

            if (urbanPoint.getPointType() == UrbanPointType.CITY){
                taxes*=1.5f;
            }

            if (governor != null) {
                float from_gov_skill = 0.00005f * (adminLevel / 5f) * urbanPoint.getPopulation();
                if (provinceRank == ProvinceRank.COLONIAL_PROVINCE) {
                    from_gov_skill /= 2;
                }
                income += from_gov_skill;
                //getOwner().setGold((float) Math.round((getOwner().getGold() + from_gov_skill) * 10) / 10);
            }
            //getOwner().setGold((float) Math.round((getOwner().getGold() + taxes) * 10) / 10);

            float maintenance = 0.000001f * getPopulation();
            if (provinceRank == ProvinceRank.COLONIAL_PROVINCE) {
                maintenance *= 4;
            }
            income += taxes;
            income -= maintenance;
            // getOwner().setGold((float) Math.round((getOwner().getGold() - maintenance) * 10) / 10);
        }

        /*float production = 0.001f * resource.getPrice() * (productionLevel / 10f) * landAmount / 25;
        if (provinceRank == ProvinceRank.COLONIAL_PROVINCE) {
            production /= 1.5f;
        }
        income+=production;*/
        return income;
    }

    public ArrayList<Person> getPeople() {
        return people;
    }

    public ArrayList<UrbanPoint> getUrbanPoints() {
        return urbanPoints;
    }

    public Culture getGeneratedCulture() {
        return generatedCulture;
    }

    private int assimilation_turns = 0;

    public void assimilateToStateCulture(){
        assimilation_turns+=10;
    }

    public void assimilateWithChecks(){
        if (getOwner().getGold() > Math.ceil(0.001*getPopulation()) && provinceRank != ProvinceRank.COLONIAL_PROVINCE){
            getOwner().setGold(getOwner().getGold()+(float) (-Math.ceil(0.001*getPopulation())));
            assimilateToStateCulture();
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ProvinceClaim> getClaims() {
        return claims;
    }

    public void addClaim(ProvinceClaim claim){
        claims.add(claim);
    }


    // army stuff

    private ArrayList<Army> armiesStationed = new ArrayList<>();

    public void addUnit(Army army){
        armiesStationed.add(army);
    }

    public void removeUnit(Army army){
        armiesStationed.remove(army);
    }

    public ArrayList<Army> getArmiesStationed() {
        return armiesStationed;
    }

    //

    public boolean upgradeTradeOutpostWithChecks(Game g){
        float tradeOutpostCost = (tradeOutpostLevel+1)*75f;

        if (owner.getGold() >= tradeOutpostCost){
            owner.setGold(owner.getGold()-tradeOutpostCost);
            tradeOutpostLevel++;
            return true;
        }
        return false;
    }

    public void upgradeTradeOutpost(){
        tradeOutpostLevel++;
    }

    public void downgradeTradeOutpost() {
        tradeOutpostLevel--;
    }

    // Buildings

    private final ArrayList<Building> buildings = new ArrayList<>();

    public ArrayList<Building> getBuildings() {
        return buildings;
    }

    public int getBuildingSlotCount() {
        return buildingSlotCount;
    }

    public void addBuilding(Building building){
        buildings.add(building);
    }

    public List<BuildingConstructionEntry> getConstructionEntries(){
        List<BuildingConstructionEntry> constructionEntries = new ArrayList<>(List.of(
                new BuildingConstructionEntry(85f, "buildings/marketplace.png", Marketplace.class,
                        "Marketplace", "Increases province's trade outpost\n level")
        ));

        if (agroLivestockResources.contains(resource)){
            constructionEntries.add(new BuildingConstructionEntry(85f, "buildings/farm.png", Farm.class,
                    "Farm", "Increases production level in\n provinces with agricultural or \nlivestock related goods"));
        }

        if (minedResources.contains(resource)){
            constructionEntries.add(new BuildingConstructionEntry(85f, "buildings/placeholder.png", Mine.class,
                    "Mine", "Increases production level in\n provinces with goods which can\n be extracted from below"));
        }

        boolean noCentralAdmin = true;

        for (Building building: buildings){
            if (building instanceof CentralAdministration) {
                noCentralAdmin = false;
                break;
            }
        }

        if (noCentralAdmin){
            constructionEntries.add(new BuildingConstructionEntry(85f, "buildings/placeholder.png",
                    CentralAdministration.class, "Central Administration",
                    "Increases province's admin level"));
        }

        return constructionEntries;
    }
}
