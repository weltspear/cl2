package net.colonize2.game;

public abstract class EventOption {

    private final String text;

    public EventOption(String text){
        this.text = text;
    }

    public abstract int aiWeight(IPlayer player);

    public abstract void executeOption(IPlayer player);

    public String getText() {
        return text;
    }
}
