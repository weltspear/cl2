package net.colonize2.game;

import java.util.ArrayList;

public interface PopupEvent {

    ArrayList<EventOption> getOptions();

    String getText();
}
