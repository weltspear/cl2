package net.colonize2.game;

public interface IPlayer {

    void update();

    void startTurn(Game g);

    boolean hasEndedTurn();

    Country getCountry();

    PopupEventManager getEventManager();

    void discoverProvince(Province pr);
}
