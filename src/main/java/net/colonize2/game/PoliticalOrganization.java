package net.colonize2.game;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Random;

public class PoliticalOrganization {

    public enum PoliticalOrganizationTrait{
        AUTHORITARIAN,
        SOCIALIST,
        BIG_TENT,
        TOTALITARIAN,
        MINORITY_INTERESTS,
    }

    private static Random random = new Random();

    private ArrayList<PoliticalOrganizationTrait> traits = new ArrayList<>();
    private final String abbreviation;
    private final String name;

    private String culture;

    private long id = random.nextLong();

    private Person leader = null;

    public PoliticalOrganization(ArrayList<PoliticalOrganizationTrait> traits, @Nullable String abbreviation,
                                 String name, String culture){
        this.traits = traits;
        this.abbreviation = abbreviation;
        this.name = name;
        this.culture = culture;
    }
}
