package net.colonize2.game;

import net.colonize2.game.decision.Decision;
import net.colonize2.game.decision.DecisionManager;
import net.colonize2.game.diplomacy.DiplomacyManager;
import net.colonize2.game.trade.NeedsGenerator;
import net.colonize2.game.trade.Stockpile;
import net.colonize2.game.trade.TradeNode;
import net.colonize2.gen.NameGen;
import org.joml.Vector4f;

import java.awt.image.BufferedImage;
import java.util.*;

public class Country {

    private static final Random random = new Random();

    private int turn = 0;

    private static final NameGen nameGen = new NameGen();

    public enum GovernmentSystem{
        ABSOLUTE_MONARCHY("Absolute Monarchy"),
        AUTOCRACY("Autocracy"),
        SOCIALISM("Socialism"),
        COLONIAL_GOVERNMENT("Colonial Government"),
        TRIBAL("Tribal"),

        PUPPET("Puppet Government"),
        SUBSTATE("Foreign Subject"),
        ;

        private final String s;

        GovernmentSystem(String s){
            this.s = s;
        }

        @Override
        public String toString() {
            return s;
        }
    }

    private Person leader;
    private final ArrayList<Province> provinces;
    private String name;
    private GovernmentSystem governmentSystem;
    private final Culture culture;
    private Vector4f countryColor;
    private final BufferedImage flag;
    private final int id;

    private boolean isCivilized;
    private Province capital;

    public void setCountryColor(Vector4f countryColor) {
        this.countryColor = countryColor;
    }

    private final DiplomacyManager diplomacyManager;

    private final Vector4f formerColor;

    private float gold = 100f;
    private int availableManpower = 500;
    private float politicalPower = 0;

    private DecisionManager decisionManager = new DecisionManager();

    private Stockpile stockpile = new Stockpile();

    public static int genCountryId(){
        return random.nextInt();
    }

    public Country(Person leader, ArrayList<Province> provinces, String name, GovernmentSystem governmentSystem,
                   Culture culture, Vector4f countryColor, BufferedImage flag, int id, boolean isCivilized){
        this.leader = leader;
        this.provinces = provinces;
        this.name = name;
        this.governmentSystem = governmentSystem;
        this.culture = culture;
        this.countryColor = countryColor;
        this.flag = flag;
        this.id = id;
        this.isCivilized = isCivilized;
        this.diplomacyManager = new DiplomacyManager(this);
        formerColor = countryColor;

        if (!provinces.isEmpty()){
            capital = provinces.get(0);
        }

        decisionManager.getCategories().get(0).addDecision(new Decision(decisionManager.getCategories().get(0), this, "Test") {

            @Override
            public void doDecision(Game g) {

            }
        });
    }

    public Person getLeader() {
        return leader;
    }

    public ArrayList<Province> getProvinces() {
        return provinces;
    }

    public String getName() {
        return name;
    }

    public float getGold() {
        return gold;
    }

    public void setGold(float gold) {
        this.gold = gold;
    }

    public float getPoliticalPower() {
        return politicalPower;
    }

    public void setPoliticalPower(float politicalPower) {
        this.politicalPower = politicalPower;
    }

    public int getAvailableManpower() {
        return availableManpower;
    }

    private int turns_without_leader = 0;
    private Dynasty ruling_dynasty;

    public void setRulingDynasty(Dynasty ruling_dynasty) {
        this.ruling_dynasty = ruling_dynasty;
    }

    public Dynasty getRulingDynasty() {
        return ruling_dynasty;
    }

    public Vector4f getCountryColor() {
        return countryColor;
    }

    public void update(Game g){
        if (leader != null){
            leader.update();
            if (leader.isDead()){
                if (governmentSystem != GovernmentSystem.SUBSTATE)
                    g.getPlayer(this).getEventManager().issueEvent(new PopupEvent() {
                        private final Person prvLeader = leader;

                        @Override
                        public ArrayList<EventOption> getOptions() {
                            return new ArrayList<>(List.of(new EventOption[]{new EventOption("OK") {
                                @Override
                                public int aiWeight(IPlayer player) {
                                    return 100;
                                }

                                @Override
                                public void executeOption(IPlayer player) {

                                }
                            }}));
                        }

                        @Override
                        public String getText() {
                            return String.format("Leader of %s, %s %s died", name, prvLeader.getName(), prvLeader.getSurname());
                        }
                    });


                leader = null;
            }
        }
        else{
            turns_without_leader++;
            if (turns_without_leader >= 5
                    && (governmentSystem == GovernmentSystem.ABSOLUTE_MONARCHY ||
                    (governmentSystem == GovernmentSystem.PUPPET && ruling_dynasty != null) )){
                if (random.nextBoolean()){

                    leader = new Person(g.getPersonNameGen().genName(),
                            ruling_dynasty != null ? ruling_dynasty.surname(): g.getPersonNameGen().genSurname(),
                            27+random.nextInt(23),
                            random.nextInt(6),
                            random.nextInt(6),
                            random.nextInt(6),
                            new String[]{},
                            "politician",
                            culture,
                            random.nextInt());
                    issueTakeOverEvent(g);
                }
                turns_without_leader = 0;

                if (ruling_dynasty == null && leader != null){
                    if (random.nextBoolean()){
                        ruling_dynasty = new Dynasty(leader.getSurname());
                    }
                }
            }

            else if (turns_without_leader >= 10 &&
                    (governmentSystem == GovernmentSystem.SOCIALISM ||
                            governmentSystem == GovernmentSystem.AUTOCRACY ||
                            governmentSystem == GovernmentSystem.PUPPET || governmentSystem == GovernmentSystem.COLONIAL_GOVERNMENT)){
                if (random.nextBoolean()){

                    leader = new Person(g.getPersonNameGen().genName(),
                            g.getPersonNameGen().genSurname(),
                            27+random.nextInt(23),
                            random.nextInt(6),
                            random.nextInt(6),
                            random.nextInt(6),
                            new String[]{},
                            "politician",
                            culture,
                            random.nextInt());
                    issueTakeOverEvent(g);
                }
                turns_without_leader = 0;
            }
        }

        diplomacyManager.update(g);

        for (Puppet puppet: puppets){
            if (puppet.getCountry().gold > 0){
                gold += puppet.getCountry().gold*0.05f;
                puppet.getCountry().gold-=puppet.getCountry().gold*0.05f;
            }
        }

        turn++;

        politicalPower += 1;
        if (leader != null){
            politicalPower*= 10;
            politicalPower+=leader.getAdminSkill();
            politicalPower/=10;
        }
        if (politicalPower > 200){
            politicalPower = 200;
        }

        if (turn < 120){
            /*if (random.nextInt(1, 1000) > 997){
                String nomads = nameGen.genCultureName(nameGen.genRegionName());

                for (Province pr: provinces){
                    for (Province _pr : pr.getNeighbours()){
                        _pr.getUrbanPoints().add(new UrbanPoint(
                                random.nextInt(200, 400),
                                nameGen.genRegionName(),
                                UrbanPointType.VILLAGE_COMMUNITY,
                                nomads
                        ));
                    }
                    pr.getUrbanPoints().add(new UrbanPoint(
                            random.nextInt(200, 400),
                            nameGen.genRegionName(),
                            UrbanPointType.VILLAGE_COMMUNITY,
                            nomads
                    ));
                }

                g.getCultureColors().getColors().put(nomads, new Vector4f(random.nextFloat(), random.nextFloat(), random.nextFloat(), 1));

                if (g.getPlayer(this) != null)
                g.getPlayer(this).getEventManager().issueEvent(new PopupEvent() {
                    private final Person prvLeader = leader;

                    @Override
                    public ArrayList<EventOption> getOptions() {
                        return new ArrayList<>(List.of(new EventOption[]{new EventOption("OK") {
                            @Override
                            public int aiWeight(IPlayer player) {
                                return 100;
                            }

                            @Override
                            public void executeOption(IPlayer player) {

                            }
                        }}));
                    }

                    @Override
                    public String getText() {
                        return String.format("Nomads of culture %s arrived at our country", nomads);
                    }
                });

            }*/
        }

        ArrayList<TradeNode> tradeNodes = g.getCountryTradeNodes(this);
        tradeNodes.sort((o1, o2) -> {
            if (o1.getTotalNodeTradePower() > o2.getTotalNodeTradePower()) {
                return -1;
            } else if (o1.getTotalNodeTradePower() == o2.getTotalNodeTradePower())
                return 0;
            return 1;
        });

        for (Province pr: provinces){
            HashMap<Resources.Resource, Float> needs = new HashMap<>();
            NeedsGenerator.generateNeeds(pr, needs);

            for (Map.Entry<Resources.Resource, Float> need: needs.entrySet()){
                if (stockpile.getResource(need.getKey()) >= need.getValue()){
                    gold+=need.getValue()*0.01f*need.getKey().getPrice();
                    stockpile.addResource(need.getKey(), -need.getValue());
                    pr.setProsperity(pr.getProsperity()+need.getValue()/100f);
                }
                else {
                    gold += stockpile.getResource(need.getKey()) * 0.01f * need.getKey().getPrice();
                    stockpile.setResource(need.getKey(), 0);
                }
            }

            if (!tradeNodes.isEmpty()){
                TradeNode tradeNode = tradeNodes.get(0);

                for (var need: needs.entrySet()){
                    if (need.getValue() > 0)
                        tradeNode.publishUnpaidBuyOrder(need.getKey(), need.getValue(), this);
                }
            }
        }

        if (!tradeNodes.isEmpty()){
            TradeNode tradeNode = tradeNodes.get(0);
            for (Resources.Resource res: tradeNode.getWantedResources()){
                if (stockpile.getResource(res) > 0) {
                    tradeNode.publishSellOrder(res, stockpile.getResource(res), this, res.getPrice()
                            * (isCivilized ? 1.2f : 0.5f));
                    stockpile.setResource(res, 0);
                }
            }
        }
    }

    private void issueTakeOverEvent(Game g) {
        if (governmentSystem != GovernmentSystem.SUBSTATE)
            g.getPlayer(this).getEventManager().issueEvent(new PopupEvent() {
                @Override
                public ArrayList<EventOption> getOptions() {
                    return new ArrayList<>(List.of(new EventOption[]{new EventOption("OK") {
                        @Override
                        public int aiWeight(IPlayer player) {
                            return 100;
                        }

                        @Override
                        public void executeOption(IPlayer player) {

                        }
                    }}));
                }

                @Override
                public String getText() {
                    return String.format("%s %s takes over the country", leader.getName(), leader.getSurname());
                }
            });
    }

    public GovernmentSystem getGovernmentSystem() {
        return governmentSystem;
    }

    public Culture getCulture() {
        return culture;
    }

    public DiplomacyManager getDiplomacyManager() {
        return diplomacyManager;
    }

    private final ArrayList<Puppet> puppets = new ArrayList<>();

    // puppet related
    public void addPuppet(Country country, Game g){
        puppets.add(new Puppet(country, Puppet.PuppetRank.SATELITE));
        country.governmentSystem = GovernmentSystem.PUPPET;
        country.setCountryColor(new Vector4f(countryColor).mul(0.8f));
        country.overlord = this;

        for (Province province: country.getProvinces()){
            g.getProvinceColors().writeColor(province.getId(), country.countryColor);
            if (g.getPlayer(this) != null){
                g.getPlayer(this).discoverProvince(province);
            }
        }
    }

    public boolean isOurPuppet(Country c){
        for (Puppet puppet : puppets){
            if (puppet.getCountry() == c){
                return true;
            }
        }
        return false;
    }

    private Country overlord = null;

    public Country getOverlord() {
        return overlord;
    }

    public void setOverlord(Country overlord) {
        this.overlord = overlord;
    }

    public BufferedImage getFlag() {
        return flag;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setLeader(Person leader) {
        this.leader = leader;
    }

    public DecisionManager getDecisionManager() {
        return decisionManager;
    }

    public static final float colonizationCost = 25*3;

    /***
     * Doesn't check validity of anything
     * @param pr
     */
    public void colonize(Province pr, Game g){
        pr.setOwner(this);
        g.getProvinceColors().writeColor(pr.getId(), this.getCountryColor());

        Person p = null;
        for (Province other: provinces){
            if (!other.getPeople().isEmpty())
            {
                p = other.getPeople().get(0);
                other.removePerson(p);
            }
        }

        pr.setGovernor(p);
        pr.setProvinceRank(Province.ProvinceRank.COLONIAL_PROVINCE);
        this.getProvinces().add(pr);

        pr.getUrbanPoints().add(
                new UrbanPoint(
                        random.nextInt(200, 1000),
                        nameGen.genRegionName(),
                        UrbanPointType.VILLAGE_COMMUNITY,
                        this.getCulture()
                )
        );
    }

    public boolean colonizeWithChecks(Province pr, Country country, Game g){
        if (!country.isCivilized){
            return false;
        }

        if (g.getPlayer(country).hasEndedTurn()){
            return false;
        }

        boolean isNeighbour = false;

        for (Province _pr: country.getProvinces()){
            if (_pr.getNeighbours().contains(pr)){
                isNeighbour = true;
                break;
            }
        }

        if (country.getGold() >= colonizationCost &&
                 pr.getOwner() == null && isNeighbour){
            colonize(pr, g);
            country.setGold(country.getGold()-colonizationCost);
            return true;
        }
        else{
            return false;
        }
    }

    public void annexProvince(Province province, Game g){
        g.getProvinceColors().writeColor(province.getId(), this.getCountryColor());
        province.setOwner(this);
        Person governor = province.getGovernor();

        if (governor != null){
            province.setGovernor(null);
            province.addPerson(governor);
        }

        provinces.add(province);
    }

    public void annexCountry(Country c, Game g){
        // move leader
        Person leader = c.leader;
        if (leader != null) {
            c.setLeader(null);
            provinces.get(0).addPerson(leader);
        }

        // move manpower to annexed provinces
        int perPr = c.availableManpower/c.provinces.size();
        for (Province pr: c.provinces){
            pr.getUrbanPoints().get(0).setPopulation(pr.getUrbanPoints().get(0).getPopulation()+perPr);
            c.availableManpower -= perPr;
        }

        if (c.availableManpower > 0){
            UrbanPoint ub = c.provinces.get(0).getUrbanPoints().get(0);
            ub.setPopulation(ub.getPopulation()+c.availableManpower);
        }

        // move finances
        this.gold+=c.gold;
        c.gold = 0;

        // prs
        for (Province pr: c.provinces){
            annexProvince(pr, g);
        }
    }

    public Province getCapital() {
        return capital;
    }

    public void setCapital(Province capital) {
        this.capital = capital;
    }

    public Stockpile getStockpile() {
        return stockpile;
    }
}
