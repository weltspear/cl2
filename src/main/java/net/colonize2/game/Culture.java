package net.colonize2.game;

import org.joml.Vector4f;

public class Culture {

    private final String cultureName;
    private boolean isTribal;
    private Vector4f color;

    public Culture(String cultureName, boolean isTribal){
        this.cultureName = cultureName;
        this.isTribal = isTribal;
    }

    public boolean isTribal() {
        return isTribal;
    }

    public void setTribal(boolean tribal) {
        isTribal = tribal;
    }

    public String getCultureName() {
        return cultureName;
    }

    public Vector4f getColor() {
        return color;
    }

    public void setColor(Vector4f color) {
        this.color = color;
    }
}
