package net.colonize2.game;

import net.colonize2.buffers.IProvinceColorManager;
import net.colonize2.game.companies.ICompany;
import net.colonize2.game.trade.TradeNode;
import net.colonize2.gen.PersonNameGen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Game {

    private final Random random = new Random();
    private final ArrayList<Country> countries;
    private final ArrayList<Province> provinces;
    private final ArrayList<SeaProvince> seaProvinces;
    private final HashMap<Integer, Province> reprColorsProvince;
    private final HashMap<Integer, SeaProvince> reprColorsProvinceSea;
    private final ArrayList<ICompany> companies = new ArrayList<>();
    private final ArrayList<IPlayer> players;
    private int current_player_doing_turn = 0;

    private ArrayList<TradeNode> tradeNodes = new ArrayList<>();

    private final PersonNameGen personNameGen = new PersonNameGen();

    private final HashMap<Country, IPlayer> country2player = new HashMap<>();

    private IProvinceColorManager provinceColors;

    private int currentTurn = 0;

    public Game(ArrayList<Country> countries, ArrayList<Province> provinces,
                ArrayList<SeaProvince> seaProvinces,
                HashMap<Integer, Province> reprColorsProvince,
                HashMap<Integer, SeaProvince> reprColorsProvinceSea,
                ArrayList<IPlayer> players){
        this.countries = countries;
        this.provinces = provinces;
        this.seaProvinces = seaProvinces;
        this.reprColorsProvince = reprColorsProvince;
        this.reprColorsProvinceSea = reprColorsProvinceSea;
        this.players = players;
        for (IPlayer player: players){
            country2player.put(player.getCountry(), player);
        }
    }

    public void setProvinceColors(IProvinceColorManager provinceColors) {
        this.provinceColors = provinceColors;
    }

    public IProvinceColorManager getProvinceColors() {
        return provinceColors;
    }

    public ArrayList<Province> getProvinces() {
        return provinces;
    }

    public Province getProvince(Integer repr_color){
        return reprColorsProvince.get(repr_color);
    }

    public SeaProvince getProvinceSea(Integer repr_color){
        return reprColorsProvinceSea.get(repr_color);
    }

    public ArrayList<Country> getCountries() {
        return countries;
    }

    public IPlayer getPlayer(Country c){
        return country2player.get(c);
    }

    public void update(){
        countries.addAll(scheduleAdd);
        scheduleAdd.clear();

        if (!playersToBeRemoved.isEmpty()){
            // fix idx
            for (IPlayer rm: playersToBeRemoved){
                int idx = players.indexOf(rm);
                if (idx == -1){
                    continue;
                }
                if (idx < current_player_doing_turn){
                    current_player_doing_turn--;
                }
                players.remove(idx);
            }

            playersToBeRemoved.clear();
        }

        if (current_player_doing_turn == players.size()){
            currentTurn++;
            for (Province province: provinces){
                province.update(this);
            }

            for (Country c: countries){
                c.update(this);
            }

            current_player_doing_turn = 0;
            players.get(0).startTurn(this);

            for (TradeNode tradeNode: tradeNodes){
                tradeNode.searchAndAddCountriesToTheNode(this);
                tradeNode.turnUpdate();
            }

            ArrayList<ICompany> toBeRemovedCompanies = new ArrayList<>();

            for (ICompany company: companies){
                if (company.isDissolved())
                    toBeRemovedCompanies.add(company);

                company.turnUpdate(this);
            }

            companies.removeAll(toBeRemovedCompanies);
        }

        else{
            players.get(current_player_doing_turn).update();
        }

        while (true) {
            if (players.get(current_player_doing_turn).hasEndedTurn()) {
                current_player_doing_turn++;
                if (current_player_doing_turn < players.size())
                    players.get(current_player_doing_turn).startTurn(this);
            }
            else break;

            if (current_player_doing_turn == players.size()){
                break;
            }
        }
    }

    public PersonNameGen getPersonNameGen() {
        return personNameGen;
    }

    public void addPlayer(IPlayer player){
        players.add(player);
        country2player.put(player.getCountry(),player);
    }

    private final ArrayList<Country> scheduleAdd = new ArrayList<>();

    public void addCountry(Country country) {
        scheduleAdd.add(country);
    }


    ArrayList<IPlayer> playersToBeRemoved = new ArrayList<>();

    public void removePlayer(IPlayer player){
        playersToBeRemoved.add(player);

        Country c = null;
        for (Map.Entry<Country, IPlayer> playerEntry: country2player.entrySet()){
            if (playerEntry.getValue() == player){
                c = playerEntry.getKey();
                break;
            }
        }
        country2player.remove(c);
    }

    public Country getCountry(int id){
        for (Country c: countries)
            if (c.getId() == id){
                return c;
            }
        return null;
    }

    private int tribalStateAmount = 120;

    public boolean shouldSpawnTribalState(){
        if (random.nextInt(100) == 1 && tribalStateAmount > 0){
            tribalStateAmount--;
            return true;
        }
        return false;
    }

    public int getCurrentTurn() {
        return currentTurn;
    }

    public void addTradeNode(TradeNode tradeNode){
        tradeNodes.add(tradeNode);
    }

    public ArrayList<TradeNode> getTradeNodes() {
        return tradeNodes;
    }

    public ArrayList<TradeNode> getCountryTradeNodes(Country c){
        ArrayList<TradeNode> nodes = new ArrayList<>();
        for (TradeNode node: tradeNodes){
            if (node.getCountriesInNode().contains(c)){
                nodes.add(node);
            }
        }
        return nodes;
    }

    public void addCompany(ICompany company){
        companies.add(company);
    }
}
