package net.colonize2.game.decision;

import net.colonize2.game.Country;
import net.colonize2.game.Game;

public abstract class Decision {

    private final DecisionCategory category;
    private final Country country;
    private final String title;

    public Decision(DecisionCategory category, Country country, String title){

        this.category = category;
        this.country = country;
        this.title = title;
    }

    public String getDecisionTitle(){
        return title;
    };

    public abstract void doDecision(Game g);

    // Some rendering properties
    public boolean isDisabled(){
        return false;
    }
    public float getProgress(){
        return 0;
    }
    public String getProgressDisplayedData(){
        return null;
    }

}
