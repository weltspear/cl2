package net.colonize2.game.decision;

import java.util.ArrayList;

public class DecisionCategory {

    private final String title;
    private ArrayList<Decision> decisions = new ArrayList<>();

    public DecisionCategory(String title){
        this.title = title;
    }

    public void addDecision(Decision decision){
        decisions.add(decision);
    }

    public void removeDecision(Decision decision){
        decisions.remove(decision);
    }

    public ArrayList<Decision> getDecisions() {
        return decisions;
    }

    public String getTitle() {
        return title;
    }
}
