package net.colonize2.game.decision;

import java.util.ArrayList;

public class DecisionManager {
    private ArrayList<DecisionCategory> categories = new ArrayList<>();

    public DecisionManager(){
        categories.add(new DecisionCategory("General"));
    }

    public void addCategory(DecisionCategory decisionCategory){
        categories.add(decisionCategory);
    }

    public void removeCategory(DecisionCategory decisionCategory){
        categories.remove(decisionCategory);
    }

    public ArrayList<DecisionCategory> getCategories() {
        return categories;
    }
}
