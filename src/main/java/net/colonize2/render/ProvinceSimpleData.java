package net.colonize2.render;

public class ProvinceSimpleData {
    private final int rgb;
    private final int province_id;
    private final String name;
    private final boolean isSea;

    public ProvinceSimpleData(int rgb, int province_num, String name, boolean isSea){
        this.name = name;
        this.province_id = province_num;
        this.rgb = rgb;
        this.isSea = isSea;
    }

    public int getRgb() {
        return rgb;
    }

    public int getProvinceID() {
        return province_id;
    }

    public String getName() {
        return name;
    }

    public boolean isSea() {
        return isSea;
    }
}

