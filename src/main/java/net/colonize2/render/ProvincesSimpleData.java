package net.colonize2.render;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import net.colonize2.load.ExtFileLoader;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProvincesSimpleData {

    private final ArrayList<ProvinceSimpleData> provinces;
    private final BufferedImage provinces_b;

    public ProvincesSimpleData(){
        provinces = new ArrayList<>();

        CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build();
        try (CSVReader reader = new CSVReaderBuilder(
                new FileReader("world/definitions.csv"))
                .withCSVParser(csvParser)
                .build()) {
            List<String[]> r = reader.readAll();
            for (String[] pr: r){
                ProvinceSimpleData p = new ProvinceSimpleData(
                        new Color(Integer.parseInt(pr[1]), Integer.parseInt(pr[2]), Integer.parseInt(pr[3])).getRGB(),
                        Integer.parseInt(pr[0]), pr[4], Boolean.valueOf(pr[5]));

                provinces.add(p);
            }
        } catch (IOException | CsvException e) {
            e.printStackTrace();
        }

        provinces_b = ExtFileLoader.loadExternalImage("world/provinces.bmp");
    }

    public ArrayList<ProvinceSimpleData> getProvinces() {
        return provinces;
    }

    public BufferedImage getProvincesImage() {
        return provinces_b;
    }
}

