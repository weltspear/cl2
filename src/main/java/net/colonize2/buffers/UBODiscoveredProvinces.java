package net.colonize2.buffers;

import org.lwjgl.opengl.GL32;

import java.util.Arrays;

import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferSubData;

public class UBODiscoveredProvinces implements IDiscoveredProvinceManager{

    private final int provinceDataUBO3;
    private final int[] prDiscovered;

    public UBODiscoveredProvinces(int province_data_ubo3, int[] pr_discovered){
        provinceDataUBO3 = province_data_ubo3;
        prDiscovered = pr_discovered;
    }

    public void makeDiscovered(int id){
        prDiscovered[id-1] = 1;
        glBindBuffer(GL32.GL_UNIFORM_BUFFER, provinceDataUBO3);
        glBufferSubData(GL32.GL_UNIFORM_BUFFER, (id-1)* 4L,new int[]{1});
    }

    public void makeNotDiscovered(int id){
        prDiscovered[id-1] = 0;
        glBindBuffer(GL32.GL_UNIFORM_BUFFER, provinceDataUBO3);
        glBufferSubData(GL32.GL_UNIFORM_BUFFER, (id-1)* 4L,new int[]{0});
    }

    public int isDiscovered(int id){
        return prDiscovered[id-1];
    }

    @Override
    public void writeInts(int[] buffer) {
        glBindBuffer(GL32.GL_UNIFORM_BUFFER, provinceDataUBO3);
        glBufferSubData(GL32.GL_UNIFORM_BUFFER, 0, buffer);
    }

    public void discoverAll(){
        Arrays.fill(prDiscovered, 1);
        glBindBuffer(GL32.GL_UNIFORM_BUFFER, provinceDataUBO3);
        glBufferSubData(GL32.GL_UNIFORM_BUFFER, 0, prDiscovered);
    }
}
