package net.colonize2.buffers;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.opengl.ARBShaderStorageBufferObject;

import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferSubData;

/***
 * Note: validity of data is not checked
 */
public class BufferProvinceColors implements IProvinceColorManager{

    private final int provinceDataSSBO;

    public BufferProvinceColors(int province_data_ssbo){

        provinceDataSSBO = province_data_ssbo;
    }

    public void writeColor(int provinceId, Vector3f color){
        glBindBuffer(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, provinceDataSSBO);
        int idx = (provinceId-1)*3*4;
        glBufferSubData(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, idx, new float[]{color.x, color.y, color.z});
    }

    public void writeColor(int provinceId, Vector4f color){
        writeColor(provinceId, new Vector3f(color.x, color.y, color.z));
    }

    @Override
    public void writeFloats(float[] buffer) {
        glBindBuffer(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, provinceDataSSBO);
        glBufferSubData(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, 0, buffer);
    }
}
