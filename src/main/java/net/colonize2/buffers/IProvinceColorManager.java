package net.colonize2.buffers;

import org.joml.Vector3f;
import org.joml.Vector4f;

public interface IProvinceColorManager {
    void writeColor(int provinceId, Vector3f color);
    void writeColor(int provinceId, Vector4f color);

    void writeFloats(float[] buffer);
}
