package net.colonize2.buffers;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.opengl.GL32;

import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferSubData;

public class UBOProvinceColors implements IProvinceColorManager{

    private final int provinceDataUBO;

    public UBOProvinceColors(int province_data_ubo){

        provinceDataUBO = province_data_ubo;
    }

    public void writeColor(int provinceId, Vector3f color){
        glBindBuffer(GL32.GL_UNIFORM_BUFFER, provinceDataUBO);
        int idx = (provinceId-1)*3*4;
        glBufferSubData(GL32.GL_UNIFORM_BUFFER, idx, new float[]{color.x, color.y, color.z});
    }

    public void writeColor(int provinceId, Vector4f color){
        writeColor(provinceId, new Vector3f(color.x, color.y, color.z));
    }

    @Override
    public void writeFloats(float[] buffer) {
        glBindBuffer(GL32.GL_UNIFORM_BUFFER, provinceDataUBO);
        glBufferSubData(GL32.GL_UNIFORM_BUFFER, 0, buffer);
    }
}
