package net.colonize2.buffers;

public interface IDiscoveredProvinceManager {
    void makeDiscovered(int id);
    void makeNotDiscovered(int id);
    int isDiscovered(int id);

    void writeInts(int[] buffer);

}
