package net.colonize2.buffers;

import org.lwjgl.opengl.ARBShaderStorageBufferObject;

import java.util.Arrays;

import static org.lwjgl.opengl.GL32.*;

/***
 * Note: validity of data is not checked
 */
public class BufferDiscoveredProvinces implements IDiscoveredProvinceManager{

    private final int provinceDataSSBO3;
    private final int[] prDiscovered;

    public BufferDiscoveredProvinces(int province_data_ssbo3, int[] pr_discovered){
        provinceDataSSBO3 = province_data_ssbo3;
        prDiscovered = pr_discovered;
    }

    public void makeDiscovered(int id){
        prDiscovered[id-1] = 1;
        glBindBuffer(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, provinceDataSSBO3);
        glBufferSubData(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, (id-1)* 4L,new int[]{1});
    }

    public void makeNotDiscovered(int id){
        prDiscovered[id-1] = 0;
        glBindBuffer(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, provinceDataSSBO3);
        glBufferSubData(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, (id-1)* 4L,new int[]{0});
    }

    public int isDiscovered(int id){
        return prDiscovered[id-1];
    }

    @Override
    public void writeInts(int[] buffer) {
        glBindBuffer(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, provinceDataSSBO3);
        glBufferSubData(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, 0, buffer);
    }

    public void discoverAll(){
        Arrays.fill(prDiscovered, 1);
        glBindBuffer(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, provinceDataSSBO3);
        glBufferSubData(ARBShaderStorageBufferObject.GL_SHADER_STORAGE_BUFFER, 0, prDiscovered);
    }
}
