//#version 150 core
//fixme optimize
#extension GL_ARB_shader_storage_buffer_object : require

#ifndef PR_AMOUNT
#define PR_AMOUNT 1
#endif

#ifndef TEX_WIDTH
#define TEX_WIDTH 1
#endif

#ifndef TEX_HEIGHT
#define TEX_HEIGHT 1
#endif

in vec2 texposf;
out vec4 outColor;

uniform sampler2D tex;
uniform sampler2D tex2;
uniform sampler2D terrain;
uniform sampler2D normals;
uniform sampler2D heightmap;

// lighting

uniform vec3 lightPos = vec3(-70, 25, 8);
vec3 lightColor = vec3(1,1,1)*13;

//
uniform int noTerrain = 0;

// 2 buffers because i hate std430

#ifndef LEGACY

layout (packed, binding = 0)buffer province_data_buffer{
    // id -> color
    float[] pr_colors;
};

layout (packed, binding = 2) buffer province_data_buffer3{
    // "2d" array of pr ids
    bool[] discovered;
};

#endif
// legacy support
#ifdef LEGACY

#ifndef PRDATA_UBO_SIZE
#define PRDATA_UBO_SIZE 0
#endif

#ifndef PRDATA_UBO_SIZE_3
#define PRDATA_UBO_SIZE_3 0
#endif

// 2 buffers because i hate std430

layout (packed) uniform province_data_buffer{
    // id -> color
    float pr_colors[PRDATA_UBO_SIZE];
};

layout (packed) uniform province_data_buffer3{
    // "2d" array of pr ids
    bool discovered[PRDATA_UBO_SIZE_3];
};

#endif

uniform vec4 border_pressed;
uniform vec4 hovered;

vec4 test = vec4(204/255.0, 74/255.0, 79/255.0, 1);

vec4 sampleTerrain(){
    if (noTerrain == 0)
    return texture(terrain, texposf);
    else return vec4(0.7,0.7,0.7,1);
}


bool iscolorclose(vec4 color, vec4[8] check_array){
    for (int i = 0; i < check_array.length(); i++){
        if (check_array[i] == color){
            return true;
        }
    }
    return false;
}

float hoverLighten(){
    if (texture(tex, texposf) == hovered){
        return 1.5;
    }
    return 1;
}

highp float decode32(highp vec4 rgba) {
    highp float Sign = 1.0 - step(128.0,rgba[0])*2.0;
    highp float Exponent = 2.0 * mod(rgba[0],128.0) + step(128.0,rgba[1]) - 127.0;
    highp float Mantissa = mod(rgba[1],128.0)*65536.0 + rgba[2]*256.0 +rgba[3] + float(0x800000);
    highp float Result =  Sign * exp2(Exponent) * (Mantissa * exp2(-23.0 ));
    return Result;
}

int idfromtexel(){
    vec4 temp = texture(tex2, texposf);
    float r = (temp.r*255);
    float g = (temp.g*255);
    float b = (temp.b*255);
    float a = (temp.a*255);
    ivec4 bytes = ivec4(r,g,b,a);

    return((bytes.r) | (bytes.g << 8) | (bytes.b << 16)| (bytes.a<<24)); // ((bytes.r << 24) | (bytes.g << 16) | (bytes.b << 8)| (bytes.a));
}


vec4 prColor(){
    if ((0 <= texposf.x && texposf.x <= 1) && (0 <= texposf.y && texposf.y <= 1)) {
        ivec2 texposi = ivec2(int(texposf.x * TEX_WIDTH), int(texposf.y * TEX_HEIGHT));

        float r = pr_colors[((idfromtexel()-1)*3)];
        float g = pr_colors[((idfromtexel()-1)*3)+1];
        float b = pr_colors[((idfromtexel()-1)*3)+2];

        return vec4(r, g, b, 1);
    }
    else{
        return vec4(1,1,1,1);
    }
    return vec4(1,1,1,1);
}

/*puts all neighbour colors into array*/
vec4[8] produce_check_array(float _offset_constant){

    vec4[8] check_array;

    check_array[0] = texture(tex, texposf+vec2(_offset_constant, 0));
    check_array[1] = texture(tex, texposf+vec2(-_offset_constant, 0));
    check_array[2] = texture(tex, texposf+vec2(0, _offset_constant));
    check_array[3] = texture(tex, texposf+vec2(0, -_offset_constant));

    check_array[4] = texture(tex, texposf+vec2(_offset_constant, _offset_constant));
    check_array[5] = texture(tex, texposf+vec2(-_offset_constant, -_offset_constant));
    check_array[6] = texture(tex, texposf+vec2(-_offset_constant, _offset_constant));
    check_array[7] = texture(tex, texposf+vec2(_offset_constant, -_offset_constant));

    return check_array;
}

/*performs a check whether this fragment should be border*/
bool border_check(vec4 color, vec4[8] check_array){
    bool n1 = check_array[0] == color ? true: false;
    bool n2 = check_array[1] == color ? true: false;
    bool n3 = check_array[2] == color ? true: false;
    bool n4 = check_array[3] == color ? true: false;

    bool n5 = check_array[4] == color ? true: false;
    bool n6 = check_array[5] == color ? true: false;
    bool n7 = check_array[6] == color ? true: false;
    bool n8 = check_array[7] == color ? true: false;

    return !(n1 && n2 && n3 && n4 && n5 && n6 && n7 && n8);
}

vec4 border(vec2 uv){
    vec4 o = texture(tex, texposf);

    float _offset_constant = 0.000002;

    vec4[8] check_array = produce_check_array(_offset_constant);

    // select province?
    if (border_check(border_pressed, check_array)) {
        if (border_pressed == o || iscolorclose(border_pressed, check_array)) {
            return vec4(1,1,0,1);
        }
    }

    // is this sample/pixel/whatever border?
    if (border_check(o, check_array)){
        if (iscolorclose(o, check_array)) {
            return sampleTerrain()*0.6;
        }
    }

    return vec4(1,1,1,1);
}

vec4 darken(){
    ivec2 texposi = ivec2(int(texposf.x * TEX_WIDTH), int(texposf.y * TEX_HEIGHT));
    if (!discovered[idfromtexel()-1]){
        return vec4(0.4, 0.4, 0.4, 1);
    }
    return vec4(1,1,1,1);
}

void main() {
    vec4 _border = border(texposf);
    vec4 _darken = darken();
    vec4 _prcolor = prColor();

    outColor = vec4(1,1,1,1);

    if (texture(tex, texposf) == border_pressed){
        outColor*=vec4(1.5, 1.5, 1.5, 1);
    }

    if (_border == vec4(0,0,0,0))
    {
        outColor *= 0.45*sampleTerrain();
    }
    else if (_border != vec4(1,1,1,1)){
        outColor *= _border*_darken;
    }
    else if (_darken==vec4(1,1,1,1)&&_prcolor != vec4(1,1,1,1)){
        outColor *= ((_prcolor-(1-sampleTerrain())/4));
    }
    else
        outColor *= sampleTerrain()*_darken;

    outColor = hoverLighten()*outColor;

    // lighting
    vec3 fragPos = vec3(texposf*50, ((texture(heightmap, texposf).r)*2.0-1.0)*5);
    vec3 normal = normalize(texture(normals, texposf).xyz*2.0-1.0);
    vec3 lightDir = normalize(lightPos-fragPos);

    float diff = max(dot(normal, lightDir), 0);
    vec3 diffuse = diff * lightColor;

    outColor *= vec4(diffuse, 1);

}