# Colonize 2

Prototype Grand Strategy game. 

# Libraries and stuff used

This thing uses monogram font (https://datagoblin.itch.io/monogram), OpenSimplex2 noise (https://github.com/KdotJPG/OpenSimplex2), 
STGL (https://gitlab.com/weltspear/stgl), LWJGL (https://www.lwjgl.org/), Jackson, OpenCSV, JOML, 
JetBrains Annotations, also this dataset is used for "person generation" (https://github.com/rossgoodwin/american-names)